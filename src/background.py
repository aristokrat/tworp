import pygame, utils, random
from pygame.locals import *

SCREENWIDTH = 1024
SCREENHEIGHT = 768
AREAWIDTH = 1000
AREAHEIGHT = 760
TILESIZE = 40
HORITILES = AREAWIDTH/TILESIZE
VERTTILES = AREAHEIGHT/TILESIZE

class Background:
    
    def __init__(self):
        self.bg = utils.load_image('bg1.png')
        wallpiece = utils.load_image('wall1.png')
        self.wall = self.createWall(wallpiece)
        self.bgx1 = 400
        self.bgx2 = 400+AREAWIDTH
        self.wallY = -40
        self.counter = 0
        self.counterMax = 0
        self.rate = 0
        self.direction = -1
        
    def update(self, screen):
        screen.blit(self.bg, (self.bgx1-AREAWIDTH, 0))
        screen.blit(self.bg, (self.bgx2-AREAWIDTH, 0))
        screen.blit(self.wall, (0, self.wallY))
        self.shiftBG()
        self.shiftWall()
        
        
    def shiftBG(self):
        if self.counter == self.counterMax:
            self.counter = 0
            self.counterMax = random.randint(30, 60)
            self.rate = random.randint(1,3)
            if random.randint(0,1) == 1:
                self.direction = 1
            else:
                self.direction = -1
        
        move = 0
        
        if self.counter < self.rate:
            move = self.counter
        elif (self.counterMax - self.counter) < self.rate:
            move = self.counterMax - self.counter
        else: move = self.rate
            
        
        self.bgx1 = (self.bgx1 + (move * self.direction)) %(AREAWIDTH*2)
        self.bgx2 = (self.bgx2 + (move * self.direction)) %(AREAWIDTH*2)
        
        self.counter += 1
        
    def createWall(self, wallpiece):
        wall = pygame.Surface((AREAWIDTH, AREAHEIGHT + TILESIZE))
        for i in range (HORITILES):
            for j in range (VERTTILES + 1):
                wall.blit(wallpiece, (i*40, j*40))
        colorkey = wall.get_at((0,0))
        wall.set_colorkey(colorkey, RLEACCEL)
        return wall
    
    def shiftWall(self):
        self.wallY += 1
        if self.wallY == 0:
            self.wallY = -40
    

class Water(Background):
    
    def __init__(self):
        self.bg = utils.load_trans_image('water.png')
        self.centerX = AREAWIDTH/2
        self.centerY = AREAHEIGHT/2
        self.Xcounter = 0
        self.XcounterMax = 0
        self.Xrate = 0
        self.Xdirection = -1
        self.Ycounter = 0
        self.YcounterMax = 0
        self.Yrate = 0
        self.Ydirection = -1
        self.Xmove = 0
        self.Ymove = 0
        
    def getMove(self):
        return self.Xmove * self.Xdirection, self.Ymove * self.Ydirection
    
    def update(self, screen):
        screen.blit(self.bg, (self.centerX, self.centerY))
        screen.blit(self.bg, (self.centerX-AREAWIDTH, self.centerY))
        screen.blit(self.bg, (self.centerX, self.centerY-AREAHEIGHT))
        screen.blit(self.bg, (self.centerX-AREAWIDTH, self.centerY-AREAHEIGHT))
        self.shiftBG()
        
    def shiftBG(self):
        if self.Xcounter >= self.XcounterMax:
            self.Xcounter = 0
            self.XcounterMax = random.randint(70, 100)
            self.Xrate = random.randint(0,10)
            if random.randint(0,1) == 1:
                self.Xdirection = 1
            else:
                self.Xdirection = -1
        
        if self.Xcounter < self.Xrate:
            self.Xmove = self.Xcounter
        elif (self.XcounterMax - self.Xcounter) < self.Xrate:
            self.Xmove = self.XcounterMax - self.Xcounter
        else: self.Xmove = self.Xrate
            
        
        self.centerX = (self.centerX + (self.Xmove * self.Xdirection)) % AREAWIDTH
        
        self.Xcounter += 1
        
        if self.Ycounter >= self.YcounterMax:
            self.Ycounter = 0
            self.YcounterMax = random.randint(60, 90)
            self.Yrate = random.randint(0,10)
            if random.randint(0,1) == 1:
                self.Ydirection = 1
            else:
                self.Ydirection = -1
        
        if self.Ycounter < self.Yrate:
            self.Ymove = self.Ycounter
        elif (self.YcounterMax - self.Ycounter) < self.Yrate:
            self.Ymove = self.YcounterMax - self.Ycounter
        else: self.Ymove = self.Yrate
            
        
        self.centerY = (self.centerY + (self.Ymove * self.Ydirection)) % AREAHEIGHT
        
        self.Ycounter += 1

class BossWater(Water):
    
    def __init__(self):
        self.bg = utils.load_trans_image('water.png')
        self.centerX = AREAWIDTH/2
        self.centerY = AREAHEIGHT/2
        self.Xcounter = 0
        self.XcounterMax = 0
        self.Xrate = 0
        self.Xdirection = -1
        self.Ycounter = 0
        self.YcounterMax = 0
        self.Yrate = 0
        self.Ydirection = -1
        self.Xmove = 0
        self.Ymove = 0
        self.controlled = False
    
    def setCurrent(self, Xmove, Xdirection, Ymove, Ydirection):
        self.Xmove = Xmove
        self.Xdirection = Xdirection
        self.Ymove = Ymove
        self.Ydirection = Ydirection
    
    def update(self, screen):
        screen.blit(self.bg, (self.centerX, self.centerY))
        screen.blit(self.bg, (self.centerX-AREAWIDTH, self.centerY))
        screen.blit(self.bg, (self.centerX, self.centerY-AREAHEIGHT))
        screen.blit(self.bg, (self.centerX-AREAWIDTH, self.centerY-AREAHEIGHT))
        if not self.controlled:
            self.shiftBG()
    
    def shiftBG(self):
        if self.Xcounter >= self.XcounterMax:
            self.Xcounter = 0
            self.XcounterMax = random.randint(70, 100)
            self.Xrate = random.randint(0,5)
            if random.randint(0,1) == 1:
                self.Xdirection = 1
            else:
                self.Xdirection = -1
        
        if self.Xcounter < self.Xrate:
            self.Xmove = self.Xcounter
        elif (self.XcounterMax - self.Xcounter) < self.Xrate:
            self.Xmove = self.XcounterMax - self.Xcounter
        else: self.Xmove = self.Xrate
            
        
        self.centerX = (self.centerX + (self.Xmove * self.Xdirection)) % AREAWIDTH
        
        self.Xcounter += 1
        
        if self.Ycounter >= self.YcounterMax:
            self.Ycounter = 0
            self.YcounterMax = random.randint(60, 90)
            self.Yrate = random.randint(0,5)
            if random.randint(0,1) == 1:
                self.Ydirection = 1
            else:
                self.Ydirection = -1
        
        if self.Ycounter < self.Yrate:
            self.Ymove = self.Ycounter
        elif (self.YcounterMax - self.Ycounter) < self.Yrate:
            self.Ymove = self.YcounterMax - self.Ycounter
        else: self.Ymove = self.Yrate
            
        
        self.centerY = (self.centerY + (self.Ymove * self.Ydirection)) % AREAHEIGHT
        
        self.Ycounter += 1
            
    

                
def main():
    pygame.init()
    screen = pygame.display.set_mode((AREAWIDTH, AREAHEIGHT))
    bg = Background()
    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                utils.terminate()
        bg.update(screen)
        pygame.display.update()
    
if __name__ == '__main__':main()
    
        
        
        
        