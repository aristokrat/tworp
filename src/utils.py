import os, pygame, sys
from pygame.locals import *

class assetLoader:
    def __init__(self):
        self.assets= {}
        self.assets["fireEffect"] = self.load_fireEffect()
        self.assets["player"] = self.load_player()
        self.assets["fireBall"] = self.load_fireBall()
        self.assets["rock"] = self.load_rock()
        self.assets["blood"] = self.load_blood()
        self.assets["dummy"] = self.load_Dummy()
        self.assets["shieldy"] = self.load_shieldy()
        
    def imageList(self, type):
        return self.assets[type]
    
    def load_shieldy(self):
        images = {}
        images["down"] = load_trans_image('shieldyDownLeft.png')
        #images["downRight"] = load_trans_image('shieldyDownRight.png')
        #images["upLeft"] = load_trans_image('shieldyUpLeft.png')
        images["up"] = load_trans_image('shieldyUpRight.png')
        images["right"] = load_trans_image('shieldyRight.png')
        images["left"] = load_trans_image('shieldyLeft.png')
        return images
    
    def load_Dummy(self):
        images = {}
        images["count"] = 6
        images["left"] = load_trans_image('dummyLeft.png')
        images["right"] = load_trans_image('dummyLeft.png')
        images["sleep"] = {}
        images["sleep"][1] = load_trans_image('dummySleep1.png')
        images["sleep"][2] = load_trans_image('dummySleep2.png')
        images["sleep"][3] = load_trans_image('dummySleep3.png')
        images["sleep"][4] = load_trans_image('dummySleep4.png')
        images["sleep"][5] = load_trans_image('dummySleep5.png')
        images["sleep"][6] = load_trans_image('dummySleep6.png')
        images["sleep"][7] = load_trans_image('dummySleep7.png')
        images["sleep"][8] = load_trans_image('dummySleep8.png')
        images["sleep"][9] = load_trans_image('dummySleep9.png')
        return images
    
    def load_blood(self):
        images = {}
        images["count"] = 6
        images[6] = load_trans_image('blood0001.png')
        images[5] = load_trans_image('blood0002.png')
        images[4] = load_trans_image('blood0003.png')
        images[3] = load_trans_image('blood0004.png')
        images[2] = load_trans_image('blood0005.png')
        images[1] = load_trans_image('blood0006.png')
        return images
    
    def load_rock(self):
        images = {}
        images["count"] = 1
        images[1] = load_image('rock.png', -1)
        """images[1] = load_trans_image('rock0001.png')
        images[2] = load_trans_image('rock0002.png')
        images[3] = load_trans_image('rock0003.png')
        images[4] = load_trans_image('rock0004.png')
        images[5] = load_trans_image('rock0005.png')
        images[6] = load_trans_image('rock0006.png')
        images[7] = load_trans_image('rock0007.png')
        images[8] = load_trans_image('rock0008.png')
        images[9] = load_trans_image('rock0009.png')
        images[10] = load_trans_image('rock0010.png')
        images[11] = load_trans_image('rock0011.png')
        images[12] = load_trans_image('rock0012.png')
        images[13] = load_trans_image('rock0013.png')
        images[14] = load_trans_image('rock0014.png')
        images[15] = load_trans_image('rock0015.png')"""
        return images
        
    def load_fireEffect(self):
        images = {}
        images["ice"] = load_trans_image('ice.png')
        images["lightIce"] = load_trans_image('lightIce.png')
        images["fire"] = {}
        images["fire"][1] = load_trans_image('fireEffect0001.png')
        images["fire"][2] = load_trans_image('fireEffect0002.png')
        images["fire"][3] = load_trans_image('fireEffect0003.png')
        images["fire"][4] = load_trans_image('fireEffect0004.png')
        images["fire"][5] = load_trans_image('fireEffect0005.png')
        images["fire"][6] = load_trans_image('fireEffect0006.png')  
        images["smoke"] = {}
        images["smoke"][1] = load_trans_image('smoke0001.png')
        images["smoke"][2] = load_trans_image('smoke0002.png')
        images["smoke"][3] = load_trans_image('smoke0003.png')
        images["smoke"][4] = load_trans_image('smoke0004.png')
        images["smoke"][5] = load_trans_image('smoke0005.png')
        images["smoke"][6] = load_trans_image('smoke0006.png')
        images["blood"] = self.load_blood()  
        return images
    
    def load_player(self):
        images = {}
        ###############
        #upward images#
        ###############
        images['upHead'] = load_image('tworpUpHead.png', -1)
        images['upHeadHidden'] = load_trans_image('tworpUpHeadHidden.png')
        images['upBody'] = load_image('tworpUpBody.png', -1)
        images['upLegs'] = load_image('tworpUpLegs.png', -1)
        images['upWalk1'] = load_image('tworpUpWalk1.png', -1)
        images['upWalk2'] = load_image('tworpUpWalk2.png', -1)
        images['upWalk3'] = load_image('tworpUpWalk3.png', -1)
        images['upWalk4'] = load_image('tworpUpWalk4.png', -1)
        images['upSwing1'] = load_image('tworpUpSwing1.png', -1)
        images['upSwing2'] = load_image('tworpUpSwing2.png', -1)
        images['upSwing3'] = load_image('tworpUpSwing3.png', -1)
        images['upSwing4'] = load_image('tworpUpSwing4.png', -1)
        images['upSwing5'] = load_image('tworpUpSwing5.png', -1)
        images['upPoke1'] = load_image('tworpUpPoke1.png', -1)
        images['upPoke2'] = load_image('tworpUpPoke2.png', -1)
        images['upPoke3'] = load_image('tworpUpPoke3.png', -1)
        images['upPoke4'] = load_image('tworpUpPoke4.png', -1)
        images['upThrow1'] = load_image('tworpUpThrow1.png', -1)
        images['upThrow2'] = load_image('tworpUpThrow2.png', -1)

        #################
        #downward images#
        #################
        images['downHead'] = load_image('tworpDownHead.png', -1)
        images['downHeadHidden'] = load_trans_image('tworpDownHeadHidden.png')
        images['downBody'] = load_image('tworpDownBody.png', -1)
        images['downLegs'] = load_image('tworpDownLegs.png', -1)
        images['downWalk1'] = load_image('tworpDownWalk1.png', -1)
        images['downWalk2'] = load_image('tworpDownWalk2.png', -1)
        images['downWalk3'] = load_image('tworpDownWalk3.png', -1)
        images['downWalk4'] = load_image('tworpDownWalk4.png', -1)
        images['downSwing1'] = load_image('tworpDownSwing1.png', -1)
        images['downSwing2'] = load_image('tworpDownSwing2.png', -1)
        images['downSwing3'] = load_image('tworpDownSwing3.png', -1)
        images['downSwing4'] = load_image('tworpDownSwing4.png', -1)
        images['downSwing5'] = load_image('tworpDownSwing5.png', -1)
        images['downPoke1'] = load_image('tworpDownPoke1.png', -1)
        images['downPoke2'] = load_image('tworpDownPoke2.png', -1)
        images['downPoke3'] = load_image('tworpDownPoke3.png', -1)
        images['downPoke4'] = load_image('tworpDownPoke4.png', -1)
        images['downThrow1'] = load_image('tworpDownThrow1.png', -1)
        images['downThrow2'] = load_image('tworpDownThrow2.png', -1)
        
        ##################
        #rightward images#
        ##################
        images['rightHead'] = load_image('tworpRightHead.png', -1)
        images['rightHeadHidden'] = load_trans_image('tworpRightHeadHidden.png')
        images['rightBody'] = load_image('tworpRightBody.png', -1)
        images['rightLegs'] = load_image('tworpRightLegs.png', -1)
        images['rightWalk1'] = load_image('tworpRightWalk1.png', -1)
        images['rightWalk2'] = load_image('tworpRightWalk2.png', -1)
        images['rightWalk3'] = load_image('tworpRightWalk3.png', -1)
        images['rightWalk4'] = load_image('tworpRightWalk4.png', -1)
        images['rightSwing1'] = load_image('tworpRightSwing1.png', -1)
        images['rightSwing2'] = load_image('tworpRightSwing2.png', -1)
        images['rightSwing3'] = load_image('tworpRightSwing3.png', -1)
        images['rightSwing4'] = load_image('tworpRightSwing4.png', -1)
        images['rightSwing5'] = load_image('tworpRightSwing5.png', -1)
        images['rightPoke1'] = load_image('tworpRightPoke1.png', -1)
        images['rightPoke2'] = load_image('tworpRightPoke2.png', -1)
        images['rightPoke3'] = load_image('tworpRightPoke3.png', -1)
        images['rightPoke4'] = load_image('tworpRightPoke4.png', -1)
        images['rightThrow1'] = load_image('tworpRightThrow1.png', -1)
        images['rightThrow3'] = load_image('tworpRightThrow3.png', -1)
        
        #################
        #leftward images#
        #################
        images['leftHead'] = load_image('tworpLeftHead.png', -1)
        images['leftHeadHidden'] = load_trans_image('tworpLeftHeadHidden.png')
        images['leftBody'] = load_image('tworpLeftBody.png', -1)
        images['leftLegs'] = load_image('tworpLeftLegs.png', -1)
        images['leftWalk1'] = load_image('tworpLeftWalk1.png', -1)
        images['leftWalk2'] = load_image('tworpLeftWalk2.png', -1)
        images['leftWalk3'] = load_image('tworpLeftWalk3.png', -1)
        images['leftWalk4'] = load_image('tworpLeftWalk4.png', -1)
        images['leftSwing1'] = load_image('tworpLeftSwing1.png', -1)
        images['leftSwing2'] = load_image('tworpLeftSwing2.png', -1)
        images['leftSwing3'] = load_image('tworpLeftSwing3.png', -1)
        images['leftSwing4'] = load_image('tworpLeftSwing4.png', -1)
        images['leftSwing5'] = load_image('tworpLeftSwing5.png', -1)
        images['leftPoke'] = load_image('tworpLeftPoke.png', -1)
        images['leftThrow1'] = load_image('tworpLeftThrow1.png', -1)
        images['leftThrow2'] = load_image('tworpLeftThrow2.png', -1)
        images['leftThrow3'] = load_image('tworpLeftThrow3.png', -1)
        return images
    def load_fireBall(self):
        ##################
        #Load Projectiles#
        ##################
        images = {}
        images[1] = load_trans_image('fireball0001.png')
        images[2] = load_trans_image('fireball0002.png')
        images[3] = load_trans_image('fireball0003.png')
        images[4] = load_trans_image('fireball0004.png')
        images[5] = load_trans_image('fireball0005.png')
        images[6] = load_trans_image('fireball0006.png')
        images[7] = load_trans_image('fireball0007.png')
        images[8] = load_trans_image('fireball0008.png')
        images[9] = load_trans_image('fireball0009.png')
        images[10] = load_trans_image('fireball0010.png')
        images[11] = load_trans_image('fireball0011.png')
        images[12] = load_trans_image('fireball0012.png')
        images[13] = load_trans_image('fireball0013.png')
        images[14] = load_trans_image('fireball0014.png')
        images[15] = load_trans_image('fireball0014.png')
        images[16] = load_trans_image('fireball0016.png')
        images[17] = load_trans_image('fireball0017.png')
        images[18] = load_trans_image('fireball0018.png')
        images[19] = load_trans_image('fireball0019.png')
        images[20] = load_trans_image('fireball0020.png')
        images[21] = load_trans_image('fireball0021.png')
        images[22] = load_trans_image('fireball0022.png')
        images[23] = load_trans_image('fireball0023.png')
        images[24] = load_trans_image('fireball0024.png')
        images[25] = load_trans_image('fireball0025.png')
        images[26] = load_trans_image('fireball0026.png')
        images[27] = load_trans_image('fireball0027.png')
        images[28] = load_trans_image('fireball0028.png')
        images[29] = load_trans_image('fireball0029.png')
        images[30] = load_trans_image('fireball0030.png')
        return images
    


#platform-agnostic image loading
def load_image(name, colorkey=None):
    fullname = os.path.join('data', name)
    try:
        image = pygame.image.load(fullname).convert()
    except pygame.error, message:
        print 'Cannot load image:', fullname
        raise SystemExit, message
    if colorkey is not None:
        if colorkey is -1:
            colorkey = image.get_at((0,0))
            image.set_colorkey(colorkey, RLEACCEL) # accelerate 
    return image

def load_image_noconv(name, colorkey=None):
    fullname = os.path.join('data', name)
    try:
        image = pygame.image.load(fullname)
    except pygame.error, message:
        print 'Cannot load image:', fullname
        raise SystemExit, message
    if colorkey is not None:
        if colorkey is -1:
            colorkey = image.get_at((0,0))
            image.set_colorkey(colorkey, RLEACCEL) # accelerate 
    return image

def load_trans_image(name, notUsed = 0):
    fullname = os.path.join('data', name)
    try:
        image = pygame.image.load(fullname).convert_alpha()
    except pygame.error, message:
        print 'Cannot load image:', fullname
        raise SystemExit, message
    return image

#function for awaiting player keyboard input, allows quitting
def waitForPlayerToPressKey():
    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                terminate()
            elif event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    terminate()
                elif event.key == K_CAPSLOCK or event.key == K_NUMLOCK:
                    True
                else:
                    return
            elif event.type == JOYBUTTONDOWN:
                if event.button == 16: #PS button
                    terminate()
                return

#invert colors of image            
def inverted(image):
    
    inv = pygame.Surface(image.get_rect().size, pygame.SRCALPHA)
    inv.fill((255,255,255,255))
    inv.blit(image, (0,0), None, BLEND_RGB_SUB)
    return inv
            
#function for easy exiting of application
def terminate():
    pygame.quit()
    sys.exit()
