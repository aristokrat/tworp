import pygame, utils
from pygame.locals import *

class Fire1():

    def __init__(self):
        self.name = 'fire1'
        self.mapImage = utils.load_trans_image('bookFire1.png')
        
class Fire2():
    
    def __init__(self):
        self.name = 'fire2'
        self.mapImage = utils.load_trans_image('bookFire2.png')
        
class Ice1():

    def __init__(self):
        self.name = 'ice1'
        self.mapImage = utils.load_trans_image('bookIce1.png')
        
class Ice2():
    
    def __init__(self):
        self.name = 'ice2'
        self.mapImage = utils.load_trans_image('bookIce2.png')
        
class Hide1():

    def __init__(self):
        self.name = 'hide1'
        self.mapImage = utils.load_trans_image('bookHide1.png')
        
class Hide2():
    
    def __init__(self):
        self.name = 'hide2'
        self.mapImage = utils.load_trans_image('bookHide2.png')