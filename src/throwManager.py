import pygame, projectile
from pygame.locals import *

def newProjectile(name, vector, playerRect, start):
    if name == 'coin':
        return projectile.Coin(vector, playerRect, start)
    elif name == 'acorn':
        return projectile.Acorn(vector, playerRect, start)
    elif name == 'egg':
        return projectile.Egg(vector, playerRect, start)
    elif name == 'eye':
        return projectile.Eye(vector, playerRect, start)
    elif name == 'cloud':
        return projectile.Cloud(vector, playerRect, start)
    elif name == 'puffer':
        return projectile.Puffer(vector, playerRect, start)
    
def getMapImage(name):
    if name == 'coin':
        temp = projectile.Coin(None, None, None)
        return temp.image
    elif name == 'acorn':
        temp = projectile.Acorn(None, None, None)
        return temp.image
    elif name == 'egg':
        temp = projectile.Egg(None, None, None)
        return temp.image
    elif name == 'eye':
        temp = projectile.Eye(None, None, None)
        return temp.image
    elif name == 'cloud':
        temp = projectile.Cloud(None, None, None)
        return temp.image
    elif name == 'Puffer':
        temp = projectile.Puffer(None, None, None)
        return temp.image