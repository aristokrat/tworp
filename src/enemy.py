import pygame, utils, math, random, time, projectile
from pygame.locals import *

AREAWIDTH = 1000
AREAHEIGHT = 760

BURN_DAMAGE_RATE = 10
#################################
#Change these for easy balancing#
#################################
VALUES = {
          "Base": {       "damage": 2,  "life": 200,  "moveRate":  5, "expYield":  0, "manaYield":  0, "freezeAble": True, "burnAble": True},
          "Dummy": {      "damage": 1,  "life": 200,  "moveRate":  5, "expYield": 10, "manaYield":  0, "freezeAble": True, "burnAble": True},   
          "Snail": {      "damage": 7, "life":  40,  "moveRate":  2, "expYield": 20, "manaYield": 10, "freezeAble": True, "burnAble": False},
          "Shieldy": {    "damage": 5, "life":  15,  "moveRate":  6, "expYield": 25, "manaYield": 10, "freezeAble": True, "burnAble": True},
          "Shooter": {    "damage": 5, "life":  30,  "moveRate":  0, "expYield": 10, "manaYield":  0, "freezeAble": True, "burnAble": True, "peaMoveRate":10, "peaTimeDelay":.5,},
          "Bat": {        "damage": 2, "life":  20,  "moveRate": 15, "expYield":  5, "manaYield":  0, "freezeAble": True, "burnAble": True},
          "Ivy": {        "damage": 10, "life":  10,  "moveRate":  0, "expYield":  5, "manaYield": 10, "freezeAble": True, "burnAble": True}, 
          "Boss":{        "damage": 10, "life":  300, "moveRate":  7, "expYield": 50, "manaYield": 50, "freezeAble": False,"burnAble": False,  "bombTimeDelay": 2},
          "Bomb":{        "damage": 20, "life":    5, "moveRate":  0, "expYield":  1, "manaYield":  0, "freezeAble": True, "burnAble": True},
          "GooBlob":{     "damage": 10, "life":   50, "moveRate":  2, "expYield": 30, "manaYield": 30, "freezeAble": True, "burnAble": True},
          "Goo":{         "damage":  0, "life":   50, "moveRate":  2, "expYield":  0, "manaYield":  0, "freezeAble": True, "burnAble": True},
          "runAway":{     "damage": 10, "life":   30, "moveRate":  10,"expYield": 15, "manaYield":  5, "freezeAble": True, "burnAble": True , "standTime" : 13},
          "teleport":{    "damage": 15, "life":   30, "moveRate":  10,"expYield": 25, "manaYield": 20, "freezeAble": False,"burnAble": False , "standTime" : 13, "castTime" : 8},
          "levelMultiplyer":{"damage": 0}#life level increase not yet implemented
          }

class Base:
    def __init__(self,imageAssets, type = "Base"):
        self.life = VALUES[type]["life"]
        self.image = utils.load_trans_image('dummyLeft.png')
        self.rect = self.image.get_rect()
        self.baseMoveRate = VALUES[type]["moveRate"]
        self.baseDamage = VALUES[type]["damage"]
        self.expPayload = VALUES[type]["expYield"]
        self.manaPayload = VALUES[type]["manaYield"]
        self.freezeAble = VALUES[type]["freezeAble"]
        self.burnAble = VALUES[type]["burnAble"]
        self.moveRate = self.baseMoveRate
        self.damageMultiplyer = 1
        self.imageAssets = imageAssets
        self.effects = self.loadEffects()
        
        self.type = 'enemy' #new type tracking variable
        
        ############################
        #new level effect variables#
        ############################
        self.inDark = False
        self.phased = False

                
        #######################
        #new ability variables#
        #######################
        self.frozen = False
        self.iced = False
        self.freezeCounter = 0
        self.aFlame = False
        self.onFife = 0
        self.bleeding = 0
        
    def loadEffects(self):
        return self.imageAssets.imageList("fireEffect")
    
    def drawEffects(self, screen):
        if self.frozen:
            image = self.effects['ice']
            rect = image.get_rect()
            rect.center = self.rect.center
            screen.blit(image,rect.topleft)
        if self.iced:
            image = self.effects['lightIce']
            rect = image.get_rect()
            rect.center = self.rect.center
            screen.blit(image,rect.topleft)
        if self.onFife >= 1:
            if self.burnAble:
                burnType = "fire"
            else:
                burnType = "smoke"
            image = self.effects[burnType][self.onFife]
            rect = image.get_rect()
            rect.center = self.rect.center
            screen.blit(image,rect.topleft)
            self.onFife -= 1
        if self.bleeding > 0:
            image = self.effects["blood"][self.bleeding]
            rect = image.get_rect()
            rect.center = self.rect.center
            screen.blit(image,rect.topleft)
            self.bleeding -= 1
            
    def draw(self, screen):
        screen.blit(self.image, self.rect.topleft)
        self.drawEffects(screen)
    
    def update(self, screen, player, environs, room, magicColor):
        self.damageMultiplyer = (1+VALUES["levelMultiplyer"]["damage"]*(player.gameLevel-1))
         #new ability method
        if not self.phased:
            if self.aFlame:
                self.life -= BURN_DAMAGE_RATE
                if self.life <= 0:
                    self.life = 1
                    self.aFlame = False #burn can't kill enemies
            #new ability method
            if self.frozen:
                self.freezeCounter -= 1
                if self.freezeCounter == 0:
                    self.frozen = False
                    self.moveRate = self.baseMoveRate #added line
            if self.iced: #added line
                self.moveRate = self.baseMoveRate/2 #added line
            else:
                self.moveRate = self.baseMoveRate
        
        if not self.phased:
            self.draw(screen)
            
                
    def takeThrowDamage(self, damage):
        if not self.phased:
            if self.frozen:
                damage = damage/2
            else:
                self.bleeding = self.effects["blood"]["count"]
            self.life -= damage
        if self.life <= 0:
            return 'dead'
        
    def phase(self):
        if self.phased == True:
            self.phased = False
        elif self.phased == False:
            self.phased = True
        
    #####################
    #new exp/mana method#
    #####################
    def giveUpTheGoods(self):
        return (self.expPayload, self.manaPayload)
    
    ###################
    #new freeze method#
    ###################
    def freeze(self, time, icy = False):
        if self.freezeAble and not self.phased:
            self.frozen = True
            self.freezeCounter = time
            if icy:
                self.iced = True
                self.aFlame = False
        
    ###########################
    #new takeFireDamage method#
    ###########################
    def takeFireDamage(self, damage, burn = False):
        if not self.phased:
            if self.onFife == 0:
                    self.onFife = 6
            if self.burnAble:
                if not self.iced or not self.frozen:
                    self.life -= damage
                if burn:
                    self.aFlame = True
                    self.iced = False
                    self.frozen = False
                if self.life <= 0:
                    return 'dead'
            else:
                self.iced = False
                self.frozen = False
        
        
    def takeMeleeDamage(self, damage, knockback):
        if not self.phased:
            if self.frozen:
                damage = damage/2
            else:
                self.bleeding = self.effects["blood"]["count"]
                #print self.effects["blood"]
            self.life -= damage
            self.rect.move_ip(knockback)
        if self.life <= 0:
            return 'dead'
        
    def pickPoint(self):#picks a random point on the screen
        return (random.randint(200,AREAWIDTH -200),random.randint(100,AREAHEIGHT -100) )
    
    
    
    
    ###############
    ####  NEW  ####
    ###############
    
    
    
    def waterMove(self, x, y, room, magicColor):
        if x > 0:
            self.moveRight(room, magicColor, x)
        elif x < 0:
            self.moveLeft(room, magicColor, 0-x)
        if y > 0:
            self.moveDown(room, magicColor, y)
        elif y < 0:
            self.moveUp(room, magicColor, 0-y)
    
    #move left, scanning left edge for collisions
    def moveLeft(self, room, magicColor, distance):
        if self.rect.left - distance > 0:
            maxMove = distance
            x = self.rect.left
            y = self.rect.top
            #scan edge
            while maxMove > 0 and y < self.rect.bottom:
                move = 1
                while move <= maxMove:
                    if room.get_at((x-move,y)) == magicColor:
                        if maxMove > move - 1:
                            maxMove = move - 1
                            break
                    else:
                        move += 1
                y += 1
            
            self.rect.move_ip(-1*maxMove, 0)
        else: self.rect.left = 0
    
    #move right, scanning right edge for collisions    
    def moveRight(self, room, magicColor, distance):
        if self.rect.right + distance < AREAWIDTH:
            maxMove = distance
            x = self.rect.right
            y = self.rect.top
            #scan edge
            while maxMove > 0 and y < self.rect.bottom:
                move = 1
                while move <= maxMove:
                    if room.get_at((x-1+move,y)) == magicColor:
                        if maxMove > move - 1:
                            maxMove = move - 1
                        break
                    else:
                        move += 1
                y += 1

            self.rect.move_ip(maxMove, 0)
        else: self.rect.right = AREAWIDTH
        
    #move up, scanning top edge for collisions
    def moveUp(self, room, magicColor, distance):
        if self.rect.top - distance > 0:
            maxMove = distance
            y = self.rect.top
            x = self.rect.left
            #scan edge
            while maxMove > 0 and x < self.rect.right:
                move = 1
                while move <= maxMove:
                    if room.get_at((x,y-move)) == magicColor:
                        if maxMove > move - 1:
                            maxMove = move - 1
                        break
                    else:
                        move += 1
                x += 1
                
            self.rect.move_ip(0, -1*maxMove)
        else: self.rect.top = 0
        
    #move down, scanning bottom edge for collisions
    def moveDown(self, room, magicColor, distance):
        if self.rect.bottom + distance < AREAHEIGHT:
            maxMove = distance
            y = self.rect.bottom
            x = self.rect.left
            #scan edge
            while maxMove > 0 and x < self.rect.right:
                move = 1
                while move <= maxMove:
                    if room.get_at((x,y-1+move)) == magicColor:
                        if maxMove > move - 1:
                            maxMove = move - 1
                        break
                    else:
                        move += 1
                x += 1
                    
            self.rect.move_ip(0, maxMove)
        else: self.rect.bottom = AREAHEIGHT


class Dummy(Base):
    
    def __init__(self,imageAssets):
        Base.__init__(self, imageAssets, "Dummy")
        self.imageAssets = imageAssets
        self.image = utils.load_trans_image('dummySleep1.png')
        self.rect = self.image.get_rect()
        self.images = self.loadImages()
        self.direction = "left"
        self.awake = False
        self.sleepImage = 1
        self.sleepTime = 1
        
    def loadImages(self):
        images = self.imageAssets.imageList("dummy")
        return images
    
        
    #########################################
    #new update ability with freeze and hide#
    #########################################
    #changed player.rect to player.lastRect
    def update(self, screen, player, environs, room, magicColor):
        Base.update(self, screen, player, environs, room, magicColor)
        ###############
        move = True
        if player.gameLevel == 1 and self.inDark:
            move = False
        elif player.gameLevel == 1 and self.phased:
            move = False
        ################

        if self.life != VALUES["Dummy"]["life"]:
            self.awake = True #consider rolling this into move variable?

        if move and not self.frozen and self.awake:
            xP = player.lastRect.centerx #changed line
            yP = player.lastRect.centery #changed line
            xE = self.rect.centerx
            yE = self.rect.centery
            vectorLength = math.sqrt((xP-xE)**2 + (yP-yE)**2)
            if vectorLength == 0.0:
                vectorLength = 0.0001
            #move basically moveRate number of units toward player, based on ratio calculation
            deltaX = (self.moveRate*(xP-xE))/vectorLength #changed line
            deltaY = (self.moveRate*(yP-yE))/vectorLength #changed line
            
            if deltaX > 0:
                self.moveRight(room, magicColor, deltaX)
            elif deltaX < 0:
                self.moveLeft(room, magicColor, 0-deltaX)
            if deltaY > 0:
                self.moveDown(room, magicColor, deltaY)
            elif deltaY < 0:
                self.moveUp(room, magicColor, 0-deltaY)
            
            if not player.hidden:#new line
                if self.rect.colliderect(player.rect) and not self.phased:
                    player.takeDamage(self.baseDamage * self.damageMultiplyer)
            
            if deltaX > 0:
                self.direction = "right"
            else:
                self.direction = "left"
            self.image = self.images[self.direction]
        if not self.awake:
            self.image = self.images["sleep"][self.sleepImage]
            self.sleepTime += 1
            if self.sleepTime > 7:
                self.sleepImage +=1
                self.sleepTime = 0
            if self.sleepImage == 10:
                self.sleepImage = 1
         
class Teleport(Base):
    def __init__(self, imageAssets):
        Base.__init__(self, imageAssets, "teleport")
        self.images = self.loadImages()
        self.image = self.images["stand"]
        self.rect = self.images["rect"]
        self.moveTo = self.pickPoint()  
        self.MAXSTANDTIME = VALUES["teleport"]["standTime"]
        self.MAXCASTTIME = VALUES["teleport"]["castTime"]
        self.standTime = 0
        self.castTime = 0
        self.casting = True
        self.peas = []
        self.peaMoveRate = VALUES["Shooter"]["peaMoveRate"]
        
    def update(self, screen, player, environs, room, magicColor):
        Base.update(self, screen, player, environs, room, magicColor)
        if not self.frozen and not self.phased:
            if self.casting:
                self.image = self.images["cast"]
                if self.castTime == 0 or self.castTime == self.MAXCASTTIME/2 or self.castTime == self.MAXCASTTIME/3:
                    a = random.uniform(0.0, 1.0)
                    b = math.sqrt(1 - a**2)
                    v1 = a * self.peaMoveRate
                    v2 = b * self.peaMoveRate
                    self.peas.append(projectile.Pea((v1, v2), self.rect))
                    self.peas.append(projectile.Pea((-1*v1, -1*v2), self.rect))
                    self.peas.append(projectile.Pea((-1* v2, v1), self.rect))
                    self.peas.append(projectile.Pea((v2, -1 * v1), self.rect))
                    self.lastPeaTime = time.time()
                if self.castTime == self.MAXCASTTIME:
                    self.rect.center = self.pickPoint()
                    self.casting = False
                    self.standTime = 0
                else:
                    self.castTime += 1
            else:
                self.image = self.images["stand"]
                if self.standTime == self.MAXSTANDTIME:
                    self.castTime = 0
                    self.casting = True
                self.standTime +=1
                    

        for pea in self.peas:
            pea.update(screen)
        tempPeas = self.peas
        for pea in tempPeas:
            if pea.rect.left < 0 or pea.rect.right > AREAWIDTH or pea.rect.top < 0 or pea.rect.bottom > AREAHEIGHT:
                self.peas.remove(pea)
            if pea.rect.colliderect(player.rect):
                player.takeDamage(self.baseDamage * self.damageMultiplyer)
                self.peas.remove(pea)
        if self.rect.colliderect(player.rect) and not self.phased:
                player.takeDamage(self.baseDamage * self.damageMultiplyer)  
        
    def loadImages(self):
        imageStand = utils.load_trans_image('teleStand.png')
        imageCast = utils.load_trans_image('teleCast.png')
        rect = imageStand.get_rect()
        images = {"cast" :imageCast, "rect": rect, "stand" : imageStand}
        return images
   
class RunAway(Base):
    def __init__(self, imageAssets):
        Base.__init__(self, imageAssets, "runAway")
        self.images = self.loadImages()
        self.image = self.images["stand"]
        self.rect = self.images["rect"]
        self.moveTo = self.pickPoint()  
        self.MAXSTANDTIME = VALUES["runAway"]["standTime"]
        self.standTime = 0
        self.running = True
        self.peas = []
        self.peaMoveRate = VALUES["Shooter"]["peaMoveRate"]
        
    def update(self, screen, player, environs, room, magicColor):
        Base.update(self, screen, player, environs, room, magicColor)
        if not self.frozen and not self.phased:
            if self.running:
                xP = self.moveTo[0]
                yP = self.moveTo[1]
                xE = self.rect.centerx
                yE = self.rect.centery
                vectorLength = math.sqrt((xP-xE)**2 + (yP-yE)**2)
                if vectorLength == 0.0:
                    vectorLength = 0.0001
                #move basically moveRate number of units toward player, based on ratio calculation
                deltaX = (self.moveRate*(xP-xE))/vectorLength
                deltaY = (self.moveRate*(yP-yE))/vectorLength
                
                self.rect.move_ip(deltaX, deltaY)
                
                if deltaX > 0:
                    self.direction = "right"
                else:
                    self.direction = "left"
                self.image = self.images[self.direction]
                
                if self.rect.collidepoint(self.moveTo):
                    self.standTime = self.MAXSTANDTIME
                    self.running = False
                    self.moveTo = self.pickPoint()
            else:
                if self.standTime == self.MAXSTANDTIME or self.standTime == (self.MAXSTANDTIME/2):
                    a = random.uniform(0.0, 1.0)
                    b = math.sqrt(1 - a**2)
                    v1 = a * self.peaMoveRate
                    v2 = b * self.peaMoveRate
                    self.peas.append(projectile.Pea((v1, v2), self.rect))
                    self.peas.append(projectile.Pea((-1*v1, -1*v2), self.rect))
                    self.peas.append(projectile.Pea((-1* v2, v1), self.rect))
                    self.peas.append(projectile.Pea((v2, -1 * v1), self.rect))
                    self.lastPeaTime = time.time()
                if self.standTime > 0:
                    self.standTime -= 1
                    self.image = self.images["stand"]
                else:
                    self.running = True
        for pea in self.peas:
            pea.update(screen)
        tempPeas = self.peas
        for pea in tempPeas:
            if pea.rect.left < 0 or pea.rect.right > AREAWIDTH or pea.rect.top < 0 or pea.rect.bottom > AREAHEIGHT:
                self.peas.remove(pea)
            if pea.rect.colliderect(player.rect):
                player.takeDamage(self.baseDamage * self.damageMultiplyer)
                self.peas.remove(pea)
        if self.rect.colliderect(player.rect) and not self.phased:
                player.takeDamage(self.baseDamage * self.damageMultiplyer)  
        
    def loadImages(self):
        imageStand = utils.load_trans_image('runAwayStand.png')
        imageLeft = utils.load_trans_image('runAwayLeft.png')
        imageRight = utils.load_trans_image('runAwayRight.png')
        rect = imageStand.get_rect()
        images = {"left" :imageLeft, "right" : imageRight, "rect": rect, "stand" : imageStand}
        return images
        
class GooBlob(Dummy):
    def __init__(self, imageAssets):
        Base.__init__(self, imageAssets, "GooBlob")
        self.images = self.loadImages()
        self.image = self.images["left"]
        self.rect = self.images["rect"]
        self.gooList = []
        self.moveTo = self.pickPoint()
        
    def update(self, screen, player, environs, room, magicColor):
        Base.update(self, screen, player, environs, room, magicColor)
        if not self.frozen and not self.phased:
            xP = self.moveTo[0]
            yP = self.moveTo[1]
            xE = self.rect.centerx
            yE = self.rect.centery
            vectorLength = math.sqrt((xP-xE)**2 + (yP-yE)**2)
            if vectorLength == 0.0:
                vectorLength = 0.0001
            #move basically moveRate number of units toward player, based on ratio calculation
            deltaX = (self.moveRate*(xP-xE))/vectorLength
            deltaY = (self.moveRate*(yP-yE))/vectorLength
            
            self.rect.move_ip(deltaX, deltaY)
            
            if deltaX < deltaY:
                self.direction = "left"
            else:
                self.direction = "right"
            self.image = self.images[self.direction]
            
            if self.rect.collidepoint(self.moveTo):
                self.moveTo = self.pickPoint()
            
                
        makeGoo = True
        for goo in self.gooList:
            goo.update(screen, player, environs, room, magicColor)
            if self.rect.colliderect(goo.innerRect):
                makeGoo = False
            if goo.life <= 0:
                self.gooList.remove(goo)
                    
        if makeGoo:
            self.gooList.append(Goo(self.imageAssets, self))
                
        if self.rect.colliderect(player.rect) and not self.phased:
                player.takeDamage(self.baseDamage * self.damageMultiplyer)
        
    def loadImages(self):
        imageLeft = utils.load_trans_image('gooLeft.png')
        imageRight = utils.load_trans_image('gooRight.png')
        rect = imageLeft.get_rect()
        images = {"left" :imageLeft, "right" : imageRight, "rect": rect}
        return images
        
class Goo(Base):
    
    def __init__(self, imageAssets, boss):
        Base.__init__(self, imageAssets, "Goo")
        self.image = utils.load_trans_image('Goo.png')
        self.rect = self.image.get_rect()
        self.rect.center = boss.rect.center
        self.innerRect = pygame.Rect(self.rect.left, self.rect.top, self.rect.width - 40, self.rect.height -40)
        self.boss = boss
        
    def update(self, screen, player, environs, room, magicColor):
        Base.update(self, screen, player, environs, room, magicColor)
        self.innerRect.center = self.rect.center
        if self.rect.colliderect(player.rect) and not self.phased:
            player.takeDamage(self.baseDamage * self.damageMultiplyer)
            player.stunned = True
            
class Snail(Dummy):
    def __init__(self, imageAssets):
        Base.__init__(self, imageAssets, "Snail")
        self.images = self.loadImages()
        self.image = self.images['right']
        self.rect = self.images['snailRect']
        self.direction = "right"
        self.moveing = True
        
    def update(self, screen, player, environs, room, magicColor):
        Base.update(self, screen, player, environs, room, magicColor)
        if not self.frozen and not self.phased:
            if random.randint(0,150) == 1 and not self.moveing:
                tempLoc = self.rect.topleft
                self.rect = self.images["snailRect"]
                self.rect.topleft = tempLoc 
                self.moveing = True
            xP = player.rect.centerx
            yP = player.rect.centery
            xE = self.rect.centerx
            yE = self.rect.centery
            vectorLength = math.sqrt((xP-xE)**2 + (yP-yE)**2)
            if vectorLength == 0.0:
                vectorLength = 0.0001
            #move basically moveRate number of units toward player, based on ratio calculation
            deltaX = (self.moveRate*(xP-xE))/vectorLength
            deltaY = (self.moveRate*(yP-yE))/vectorLength
            
            
            if self.moveing:
                if deltaX > 0:
                    self.direction = "right"
                else:
                    self.direction = "left"
            elif not (self.direction == "shellLeft" or self.direction == "shellLeft"):
                tempLoc = self.rect.topleft
                self.rect = self.images["shellRect"]
                self.rect.topleft = tempLoc
                if self.direction == "left":
                    if self.direction  != "shellLeft":#accounts for diffrence in image center
                        self.rect.move_ip(14,0)
                    self.direction = "shellLeft"
                    #print "eeeeeeeeeeeeee"
                else:
                    self.direction = "shellRight"
            self.image = self.images[self.direction]
            
            if self.moveing:
                self.rect.move_ip(deltaX, deltaY)
            
        if self.rect.colliderect(player.rect) and not self.phased:
            player.takeDamage(self.baseDamage * self.damageMultiplyer)
        
    def takeFireDamage(self, damage, burn = False):
        if self.moveing:
            self.moveing = False
        return Base.takeFireDamage(self, damage, burn)

    def takeMeleeDamage(self, damage, knockback):
        if self.moveing:
            return Base.takeMeleeDamage(self, damage, knockback)
        else:
            return Base.takeMeleeDamage(self, 0, knockback)
        
    def takeThrowDamage(self, damage):
        if self.moveing:
            self.moveing = False
            return Base.takeThrowDamage(self, damage/2)
        
    def loadImages(self):

        imageLeft = utils.load_trans_image('snailLeft.png')
        imageRight = utils.load_trans_image('snailRight.png')
        imageShellRight = utils.load_trans_image('shellRight.png')
        imageShellLeft = utils.load_trans_image('shellLeft.png')
        rectSnail = imageRight.get_rect()
        rectShell = imageShellRight.get_rect()
        images = {'left' : imageLeft ,'right' : imageRight,'snailRect' :rectSnail, "shellRect" :rectShell,'shellLeft' : imageShellLeft ,'shellRight' : imageShellRight ,}

        return images
        
class Shieldy(Dummy):
    
    def __init__(self,imageAssets):
        Base.__init__(self, imageAssets, "Shieldy")
        self.assets= imageAssets
        self.images = self.loadImages()
        self.image = self.images['left']
        self.rect = self.images['rect']
        self.moveTo = self.pickPoint()
        self.direction = "left"
        self.player = None
        
    def update(self, screen, player, environs, room, magicColor):
        Base.update(self, screen, player, environs, room, magicColor)
        if not self.frozen and not self.phased:
            self.player = player
            xP = player.rect.centerx
            yP = player.rect.centery
            xM = self.moveTo[0]
            yM = self.moveTo[1]
            xE = self.rect.centerx
            yE = self.rect.centery
            vectorLength = math.sqrt((xM-xE)**2 + (yM-yE)**2)
            if vectorLength == 0.0:
                vectorLength = 0.0001
            #move basically moveRate number of units toward player, based on ratio calculation
            deltaX = (self.moveRate*(xM-xE))/vectorLength
            deltaY = (self.moveRate*(yM-yE))/vectorLength
            
            self.rect.move_ip(deltaX, deltaY)
            
            if self.rect.collidepoint(self.moveTo):
                self.moveTo = (player.rect.centerx, player.rect.centery)
                if abs(xP - xE) >abs(yP - yE) :
                    if xP > xE:
                        self.direction = 'right'
                    else:
                        self.direction = 'left'
                else:
                    if yP > yE:
                        self.direction = 'down'
                    else:
                        self.direction = 'up'
                self.image = self.images[self.direction]
                    
            
        if self.rect.colliderect(player.rect) and not self.phased:
            player.takeDamage(self.baseDamage * self.damageMultiplyer)
    
    def takeMeleeDamage(self, damage, knockback):
        if self.player is not None:
            xP = self.player.rect.centerx
            yP = self.player.rect.centery
            xE = self.rect.centerx
            yE = self.rect.centery
            if self.direction == 'right':
                if xP > xE:
                    self.rect.move_ip(knockback[0]*5,knockback[1]*5)
                else:
                    return Base.takeMeleeDamage(self, damage, knockback)
            elif self.direction == 'left':
                if xP < xE:
                    self.rect.move_ip(knockback[0]*5,knockback[1]*5)
                else:
                    return Base.takeMeleeDamage(self, damage, knockback)
            elif self.direction == 'up':
                if yP < yE:
                    self.rect.move_ip(knockback[0]*5,knockback[1]*5)
                else:
                    return Base.takeMeleeDamage(self, damage, knockback)
            elif self.direction == 'down':
                if yP > yE:
                    self.rect.move_ip(knockback[0]*5,knockback[1]*5)
                else:
                    return Base.takeMeleeDamage(self, damage, knockback)
                
    def takeThrowDamage(self, damage):
        if self.player is not None:
            xP = self.player.rect.centerx
            yP = self.player.rect.centery
            xE = self.rect.centerx
            yE = self.rect.centery
            if self.direction == 'right':
                if xP > xE:
                    pass
                else:
                    return Base.takeThrowDamage(self, damage)
            elif self.direction == 'left':
                if xP < xE:
                    pass
                else:
                    return Base.takeThrowDamage(self, damage)
            elif self.direction == 'up':
                if yP < yE:
                    pass
                else:
                    return Base.takeThrowDamage(self, damage)
            elif self.direction == 'down':
                if yP > yE:
                    pass
                else:
                    return Base.takeThrowDamage(self, damage)
    
    def loadImages(self):
        images = self.assets.imageList("shieldy")
        images["rect"] = images["left"].get_rect()
        return images

class Shooter(Dummy):
    
    def __init__(self,imageAssets):
        Base.__init__(self, imageAssets, "Shooter")
        self.image = utils.load_trans_image('shooter.png')
        self.rect = self.image.get_rect()
        self.peas = []
        self.lastPeaTime = 0
        self.peaTimeDelay = VALUES["Shooter"]["peaTimeDelay"]
        self.peaMoveRate = VALUES["Shooter"]["peaMoveRate"]
        
    def update(self, screen, player, environs, room, magicColor):
        Base.update(self, screen, player, environs, room, magicColor)
        if not self.frozen and not self.phased:
            if time.time() - self.lastPeaTime > self.peaTimeDelay:
                a = random.uniform(0.0, 1.0)
                b = math.sqrt(1 - a**2)
                v1 = a * self.peaMoveRate
                v2 = b * self.peaMoveRate
                self.peas.append(projectile.Pea((v1, v2), self.rect))
                self.peas.append(projectile.Pea((-1*v1, -1*v2), self.rect))
                self.peas.append(projectile.Pea((-1* v2, v1), self.rect))
                self.peas.append(projectile.Pea((v2, -1 * v1), self.rect))
                self.lastPeaTime = time.time()
        for pea in self.peas:
            pea.update(screen)
        tempPeas = self.peas
        for pea in tempPeas:
            if pea.rect.left < 0 or pea.rect.right > AREAWIDTH or pea.rect.top < 0 or pea.rect.bottom > AREAHEIGHT:
                self.peas.remove(pea)
            if pea.rect.colliderect(player.rect):
                player.takeDamage(self.baseDamage * self.damageMultiplyer)
                self.peas.remove(pea)
                
    def takeMeleeDamage(self, damage, knockback):
        print 'took', damage, 'damage'
        self.life -= damage
        if self.life <= 0:
            return 'dead'
        
class BossShooter(Shooter):

    def __init__(self, imageAssets):
        Shooter.__init__(self, imageAssets)
        self.life = 30
        self.image = self.image = utils.load_trans_image('bossShooter.png')
        self.peaImage = utils.load_trans_image('redPea.png')
    
    def update(self, screen, player, environs, room, magicColor):
        for pea in self.peas:
            pea.image = self.peaImage
        Shooter.update(self, screen, player, environs, room, magicColor)
        
    def takeThrowDamage(self, damage):
        self.life -= 1 #basically immune to throw damage
        if self.life <= 0:
            return 'dead'


class Bat(Dummy):
    
    def __init__(self, imageAssets):
        Base.__init__(self, imageAssets, "Bat")
        self.image = utils.load_trans_image('bat.png')
        self.rect = self.image.get_rect()
        
    def update(self, screen, player, environs, room, magicColor):
        Base.update(self, screen, player, environs, room, magicColor)
        if not self.frozen and not self.phased:
            while True:
                tempRect = pygame.Rect(self.rect)
                s1 = random.choice ((-1,1))
                s2 = random.choice ((-1,1))
                a = random.uniform(0.0, 1.0)
                b = math.sqrt(1 - a**2)
                v1 = a * self.moveRate
                v2 = b * self.moveRate
                tempRect.move_ip(v1*s1, v2*s1)
                if tempRect.left > 0 and tempRect.right < AREAWIDTH and tempRect.top > 0 and tempRect.bottom < AREAHEIGHT:
                    self.rect.topleft = tempRect.topleft
                    break
        if self.rect.colliderect(player.rect) and not self.phased:
            player.takeDamage(self.baseDamage * self.damageMultiplyer)
              
class BossBat(Bat):
    def __init__(self, imageAssets):
         Bat.__init__(self, imageAssets)
         self.image = utils.load_trans_image('bossBat.png')
         
    def update(self, screen, player, environs, room, magicColor):
        Base.update(self, screen, player, environs, room, magicColor)
        if not self.frozen and not self.phased:
            
            xP = player.lastRect.centerx
            yP = player.lastRect.centery
            xE = self.rect.centerx
            yE = self.rect.centery
            vectorLength = math.sqrt((xP-xE)**2 + (yP-yE)**2)
            if vectorLength == 0.0:
                vectorLength = 0.0001
            #move basically moveRate number of units toward player, based on ratio calculation
            deltaX = (self.moveRate*(xP-xE))/vectorLength #changed line
            deltaY = (self.moveRate*(yP-yE))/vectorLength
            
            jiggle = random.randint(0,2)
            
            s1, s2 = 1, 1
            if random.randint(0,1) == 1:
                s1 = -1
            if random.randint(0,1) == 1:
                s2 = -1
            v1 = deltaX + (s1*jiggle)
            v2 = deltaY + (s2*jiggle)
            self.rect.move_ip(v1, v2)
        if self.rect.colliderect(player.rect) and not self.phased:
            player.takeDamage(self.baseDamage * self.damageMultiplyer)

class Ivy(Dummy):
    
    def __init__(self,imageAssets):
        Base.__init__(self, imageAssets, "Ivy")
        self.image = utils.load_trans_image('ivy.png')
        self.rect = self.image.get_rect()
        self.baseDamage = VALUES["Ivy"]["damage"]
        
    def update(self, screen, player, environs, room, magicColor):
        Base.update(self, screen, player, environs, room, magicColor)
        if self.rect.colliderect(player.rect) and not self.phased:
            player.takeDamage(self.baseDamage * self.damageMultiplyer)
            
    def takeMeleeDamage(self, damage, knockback):
        print 'took', damage, 'damage'
        self.life -= damage
        if self.life <= 0:
            return 'dead'
             
class Boss(Base):
    
    def __init__(self,imageAssets, topleft = (474, 321)):
        Base.__init__(self, imageAssets, "Boss")
        self.image = utils.load_trans_image('boss.png')
        self.rect = self.image.get_rect()
        self.direction = 'left'
        self.images = self.loadImages()
        self.rect.topleft = (474, 321)
        self.bombs = []
        self.lastBombTime = 0
        self.bombTimeDelay = VALUES["Boss"]["bombTimeDelay"]
        
        
    def takeThrowDamage(self, damage):
        self.life -= damage
        if self.life <= 0:
            return 'dead'
         
    def takeMeleeDamage(self, damage, knockback):
        self.life -= damage
        self.rect.move_ip(knockback)
        if self.life <= 0:
            return 'dead'
        
    def removeBomb(self, bomb):
        self.bombs.remove(bomb)
        
    def update(self, screen, player, environs, room, magicColor):
        Base.update(self, screen, player, environs, room, magicColor)
        if not self.frozen and not self.phased:
            if time.time() - self.lastBombTime > self.bombTimeDelay:
                self.bombs.append(Bomb(self.imageAssets, self))
                self.lastBombTime = time.time()
            xP = player.rect.centerx
            yP = player.rect.centery
            xE = self.rect.centerx
            yE = self.rect.centery
            vectorLength = math.sqrt((xP-xE)**2 + (yP-yE)**2)
            #move basically moveRate number of units toward player, based on ratio calculation
            deltaX = (self.moveRate*(xP-xE))/vectorLength
            deltaY = (self.moveRate*(yP-yE))/vectorLength
            
            self.rect.move_ip(deltaX, deltaY)
            
            if deltaX > 0:
                self.direction = "right"
            else:
                self.direction = "left"
            self.image = self.images[self.direction]
        
        if self.rect.colliderect(player.rect) and not self.phased:
            player.takeDamage(self.baseDamage * self.damageMultiplyer)
            
        
        for bomb in self.bombs:
            bomb.update(screen, player, environs, room, magicColor)
                    
    def loadImages(self):
        imageLeft = utils.load_trans_image('bossLeft.png')
        imageRight = utils.load_trans_image('bossRight.png')
        images = {'left' : imageLeft ,'right' : imageRight }
        return images
        
class Bomb(Ivy):
    
    def __init__(self,imageAssets, x, y, boss):
        Base.__init__(self, imageAssets, "Bomb")
        self.image = utils.load_trans_image('bomb.png')
        self.rect = self.image.get_rect()
        self.rect.center = (x, y)
        self.boss = boss
        self.lifeCounter = random.randint(120,200)
        self.explodeDamage = 40
        self.explodeRadius = 80
        
    def update(self, screen, player, environs, room, magicColor):
        Base.update(self, screen, player, environs, room, magicColor)
        self.lifeCounter -= 1
        if self.lifeCounter == 0:
            self.explode(player)
        if self.rect.colliderect(player.rect) and not self.phased:
            player.takeDamage(self.baseDamage * self.damageMultiplyer)
            self.boss.removeBomb(self)
                            
    def explode(self, player):
        xDistance = self.rect.centerx - player.rect.centerx
        yDistance = self.rect.centery - player.rect.centery
        distance = int(round(math.sqrt(xDistance**2 + yDistance**2),0))
        if distance < self.explodeRadius:
            player.takeDamage(self.explodeDamage)
        self.boss.removeBomb(self)
        
        
        
            
        
