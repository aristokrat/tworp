import pygame, utils, time
from pygame.locals import *

SCREENWIDTH = 1024
SCREENHEIGHT = 768

def titleScreen(screen):
    #load visual assets
    title = utils.load_image('titleScreen.png')
    filler = pygame.Surface((SCREENWIDTH,SCREENHEIGHT))
    filler.fill((255,255,255))
    
    #fade in title screen
    counter = 1
    while counter < 11:
        filler.set_alpha(255-25*counter)
        screen.blit(title, (0,0))
        screen.blit(filler, (0,0))
        pygame.display.update()
        counter += 1
        
    
    #display title screen and await player input
    screen.blit(title, (0,0))
    pygame.display.update()
    utils.waitForPlayerToPressKey()
    
    #reset counter and filler alpha
    filler.set_alpha(255)
    counter = 1
    
    #move title image up in preparation for main menu
    while counter < 11:
        screen.blit(filler, (0,0))
        screen.blit(title, (0, 0-counter*15))
        pygame.display.update()
        counter += 1
    return
    