import pygame, utils, menuinput, interface
from pygame.locals import *

def Up(screen, area, areaRect, player, controller):
    
    expCombat = utils.load_trans_image('expCombatOn.png')
    expMagic = utils.load_trans_image('expMagicOn.png')
    expHealth = utils.load_trans_image('expHealthOn.png')
    corner = (212, 150)
    menuState = 1
    HUD = interface.UI()
    
    while True:
        
        for event in pygame.event.get():
            action = menuinput.manage(event, controller)
            
            if action == 'moveRight':
                menuState = (menuState + 1) % 3
            elif action == 'moveLeft':
                menuState = (menuState - 1) % 3
            elif action == 'select':
                if menuState == 0:
                    player.levelCombat()
                elif menuState == 1:
                    player.levelMagic()
                elif menuState == 2:
                    player.levelHealth()
                player.levelUp = False
                return
            elif action == 'terminate':
                utils.terminate()
            
        screen.blit(area, areaRect)
        HUD.update(screen, player)
        
        if menuState == 0:
            screen.blit(expCombat, corner)
            
        elif menuState == 1:
            screen.blit(expMagic, corner)
            
        elif menuState == 2:
            screen.blit(expHealth, corner)
            
        pygame.display.update()
    
    