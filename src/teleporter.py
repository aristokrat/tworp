import pygame, utils
from pygame.locals import *

AREAWIDTH = 1000
AREAHEIGHT = 760

class Teleporter():
    
    def __init__(self):
        self.image = utils.load_image('teleport.png', -1)
        self.rect = self.image.get_rect()
        self.rect.topleft = (AREAWIDTH/2 - self.rect.width/2, AREAHEIGHT/2 -self.rect.height/2)
        self.appearanceCounter = 0
        
    def update(self, screen):
        if self.appearanceCounter > 10:
            screen.blit(self.image, self.rect.topleft)
        self.appearanceCounter += 1
        