import pygame, utils, melee, projectile, random, magic
from pygame.locals import *

AREAWIDTH = 1000
AREAHEIGHT = 760

class weaponItem():
    
    def __init__(self, name):
        self.item = None
        self.type = 'weapon'
        if name == 'stick':
            self.item = melee.Stick()
        elif name == 'hammer1':
            self.item = melee.Hammer1()
        elif name == 'hammer2':
            self.item = melee.Hammer2()
        elif name == 'hammer3':
            self.item = melee.Hammer3()
        elif name == 'hammer4':
            self.item = melee.Hammer4()
        elif name == 'hammer5':
            self.item = melee.Hammer5()
        elif name == 'hammer6':
            self.item = melee.Hammer6()
        elif name == 'sword1':
            self.item = melee.Sword1()
        elif name == 'sword2':
            self.item = melee.Sword2()
        elif name == 'sword3':
            self.item = melee.Sword3()
        elif name == 'sword4':
            self.item = melee.Sword4()
        elif name == 'sword5':
            self.item = melee.Sword5()
        elif name == 'sword6':
            self.item = melee.Sword6()
        elif name == 'blade1':
            self.item = melee.Blade1()
        elif name == 'blade2':
            self.item = melee.Blade2()
        elif name == 'blade3':
            self.item = melee.Blade3()
        elif name == 'blade4':
            self.item = melee.Blade4()
        elif name == 'blade5':
            self.item = melee.Blade5()
        elif name == 'blade6':
            self.item = melee.Blade6()
        self.image = self.item.mapImage
        self.rect = self.image.get_rect()
        self.rect.center = (AREAWIDTH/2, AREAHEIGHT/2)
        
        
    def update(self, screen):
        screen.blit(self.image, self.rect)
        
class projectileItem(weaponItem):
    
    def __init__(self, name, center=(AREAWIDTH/2, AREAHEIGHT/2)):
        self.item = None
        self.type = 'projectile'
        self.name = name
        if name == 'coin':
            self.item = projectile.Coin(None, None, None)
        elif name == 'acorn':
            self.item = projectile.Acorn(None, None, None)
        elif name == 'egg':
            self.item = projectile.Egg(None, None, None)
        elif name == 'eye':
            self.item = projectile.Eye(None, None, None)
        elif name == 'cloud':
            self.item = projectile.Cloud(None, None, None)
        elif name == 'puffer':
            self.item = projectile.Puffer(None, None, None)
        self.image = self.item.image
        self.rect = self.image.get_rect()
        self.rect.center = center
        
class magicItem(weaponItem):
    
    def __init__(self, name):
        self.item = None
        self.type = 'magic'
        self.name = name
        if name == 'fire1':
            self.item =  magic.Fire1()
        elif name == 'fire2':
            self.item = magic.Fire2()
        elif name == 'ice1':
            self.item = magic.Ice1()
        elif name == 'ice2':
            self.item = magic.Ice2()
        elif name == 'hide1':
            self.item = magic.Hide1()
        elif name == 'hide2':
            self.item = magic.Hide2()
        self.image = self.item.mapImage
        self.rect = self.image.get_rect()
        self.rect.center = (AREAWIDTH/2, AREAHEIGHT/2)
        
class healthItem():
    
    def __init__(self):
        self.type = 'health'
        self.image = utils.load_image('spacon.png', -1)
        self.rect = self.image.get_rect()
        self.rect.center = (AREAWIDTH/2, AREAHEIGHT/2)
        
    def update(self, screen):
        screen.blit(self.image, self.rect)


def getNextWeapon(player):
    if player.weapon == None: return 'stick'
    elif player.weapon.name == 'stick':
        return random.choice(('blade1', 'sword1', 'hammer1'))
    
    
    elif player.weapon.name == 'blade1': 
        return random.choice(( 'sword1', 'hammer1', 'blade2', 'blade2', 'blade2'))
    elif player.weapon.name == 'sword1':
        return random.choice(( 'blade1', 'hammer1', 'sword2', 'sword2', 'sword2'))
    elif player.weapon.name == 'hammer1':
        return random.choice(( 'blade1', 'sword1', 'hammer2', 'hammer2', 'hammer2'))
    
    elif player.weapon.name == 'blade2': 
        return random.choice(( 'sword2', 'hammer2', 'blade3', 'blade3', 'blade3'))
    elif player.weapon.name == 'sword2':
        return random.choice(( 'blade2', 'hammer2', 'sword3', 'sword3', 'sword3'))
    elif player.weapon.name == 'hammer2':
        return random.choice(( 'blade2', 'sword2', 'hammer3', 'hammer3', 'hammer3'))
    
    elif player.weapon.name == 'blade3': 
        return random.choice(( 'sword3', 'hammer3', 'blade4', 'blade4', 'blade4'))
    elif player.weapon.name == 'sword3':
        return random.choice(( 'blade3', 'hammer3', 'sword4', 'sword4', 'sword4'))
    elif player.weapon.name == 'hammer3':
        return random.choice(( 'blade3', 'sword3', 'hammer4', 'hammer4', 'hammer4'))
    
    elif player.weapon.name == 'blade4': 
        return random.choice(( 'sword4', 'hammer4', 'blade5', 'blade5', 'blade5'))
    elif player.weapon.name == 'sword4':
        return random.choice(( 'blade4', 'hammer4', 'sword5', 'sword5', 'sword5'))
    elif player.weapon.name == 'hammer4':
        return random.choice(( 'blade4', 'sword4', 'hammer5', 'hammer5', 'hammer5'))
    
    elif player.weapon.name == 'blade5': 
        return random.choice(( 'sword5', 'hammer5', 'blade6', 'blade6', 'blade6'))
    elif player.weapon.name == 'sword5':
        return random.choice(( 'blade5', 'hammer5', 'sword6', 'sword6', 'sword6'))
    elif player.weapon.name == 'hammer5':
        return random.choice(( 'blade5', 'sword5', 'hammer6', 'hammer6', 'hammer6'))
    
    elif player.weapon.name == 'blade6': 
        return random.choice(( 'sword6', 'hammer6', 'out', 'out'))
    elif player.weapon.name == 'sword6':
        return random.choice(( 'blade6', 'hammer6', 'out', 'out'))
    elif player.weapon.name == 'hammer6':
        return random.choice(( 'blade6', 'sword6', 'out', 'out'))


def getNextProjectile(player):
    if player.projectile == None: return 'coin'
    elif player.projectile == 'coin':
        return 'acorn'
    elif player.projectile == 'acorn':
        return 'egg'
    elif player.projectile == 'egg':
        return 'eye'
    elif player.projectile == 'eye':
        return 'cloud'
    elif player.projectile == 'cloud':
        return 'puffer'
    elif player.projectile == 'puffer':
        return 'out'
    
def getNextMagic(player):
    
    choices = ['ice1', 'fire1', 'hide1', 'ice1', 'fire1', 'hide1', 'ice1', 'fire1', 'hide1', 'ice2', 'fire2', 'hide2']
    
    if player.fireLevel == 0:
        choices.remove('fire2')
    else:
        while 'fire1' in choices:
            choices.remove('fire1')
        if player.fireLevel == 2:
            choices.remove('fire2')
        
    if player.iceLevel == 0:
        choices.remove('ice2')
    else:
        while 'ice1' in choices:
            choices.remove('ice1')
        if player.iceLevel == 2:
            choices.remove('ice2')
    
    if player.hideLevel == 0:
        choices.remove('hide2')
    else:
        while 'hide1' in choices:
            choices.remove('hide1')
        if player.hideLevel == 2:
            choices.remove('hide2')
    
    if len(choices) == 0:
        return 'out'
    else:
        return random.choice(choices)
        
        
        
        
        