import pygame, levelgen, roomgen, utils, background, enemy, teleporter, item, os
from pygame.locals import *
import caveinput, random, interface, environ, leveler, math, npc, words

FRAMERATE = 30

SCREENWIDTH = 1024
SCREENHEIGHT = 768
AREAWIDTH = 1000
AREAHEIGHT = 760


def startIntro(screen, player, controller, globVars, imageAssets):
    
    Node0 = levelgen.Node(0,0)
    Node0.item = item.projectileItem('acorn', (430, 320))
    Node1 = levelgen.Node(0,0)
    Node1.item = item.projectileItem('coin', (435, 590))
    currentNode = levelgen.Node(0,0)
    
    mainClock = pygame.time.Clock()
    pygame.key.set_repeat(50)
    if globVars.sound:
        pygame.mixer.music.load(os.path.join('data', 'tworpLevel.wav'))
        pygame.mixer.music.play(-1)
    
    HUD = interface.UI()
    area = pygame.Surface((AREAWIDTH, AREAHEIGHT))
    areaRect = area.get_rect()
    areaRect.topleft = (12, 0)
    
    moveRight = moveLeft = moveUp = moveDown = False
    throwRight = throwLeft = throwUp = throwDown = False
    enemies = []
    environs = []
    
    intro0 = utils.load_image('intro0.png')
    intro1 = utils.load_image('intro1.png')
    intro2 = utils.load_image('intro2.png')
    
    mask0 = utils.load_image('intro0mask.png')
    mask1 = utils.load_image('intro1mask.png')
    mask2 = utils.load_image('intro2mask.png')
    
    squirrel = utils.load_trans_image('poisonSquirrel.png')
    squirrelRect = squirrel.get_rect()
    
    dad = npc.Dad()
    judge = npc.Judge()
    
    

    room = 0
    seqCounter = 0
    firstSeq = True
    secondSeq = False
    thirdSeq = False
    fourthSeq = False
    
    room1 = False
    room2 = False
    
    
    text1 = False
    text2 = False
    text3 = False
    text4 = False
    text5 = False
    text6 = False
    text7 = False
    text8 = False
    text9 = False
    
    
    mask = mask0
    magicColor = mask.get_at((20,20))
    
    player.rect.topleft = (140, 300)
    
    ##################
    #start event loop#
    ##################
    while True:
        
        if text1:
            words.drawText(screen, 0, 'dad1')
            text1 = False
        elif text2:
            words.drawText(screen, 0, 'dad2')
            text2 = False
        elif text3:
            words.drawText(screen, 0, 'judge1')
            text3 = False
        elif text4:
            words.drawText(screen, 0, 'tworp1')
            text4 = False
        elif text5:
            words.drawText(screen, 0, 'dad3')
            text5 = False
        elif text6:
            words.drawText(screen, 0, 'judge2')
            text6 = False
        elif text7:
            words.drawText(screen, 0, 'judge3')
            text7 = False
        elif text8:
            words.drawText(screen, 0, 'judge4')
            text8 = False
        elif text9:
            words.drawText(screen, 0, 'tworp2')
            text9 = False
        
        if not firstSeq and not secondSeq and not thirdSeq and not fourthSeq and not room2:
        
            ################
            #process inputs#
            ################   
            for event in pygame.event.get():
                action = caveinput.manage(event, controller)
                
                #########################
                #digital movement inputs#
                #########################
                if action == 'moveRight':
                    moveRight = True
                    moveLeft = False
                elif action == 'moveLeft':
                    moveLeft = True
                    moveRight = False
                elif action == 'moveUp':
                    moveUp = True
                    moveDown = False
                elif action == 'moveDown':
                    moveDown = True
                    moveUp = False
                    
                elif action == 'use':
                    player.use(currentNode)
                
                #######################    
                #stop digital movement#
                #######################
                elif action == 'XmoveRight':
                    moveRight = False
                elif action == 'XmoveLeft':
                    moveLeft = False
                elif action == 'XmoveUp':
                    moveUp = False
                elif action == 'XmoveDown':
                    moveDown = False
                    
                ##########################
                #digital throwing attacks#
                ##########################    
                elif action == 'throwRight':
                    throwRight = True
                    throwLeft = False
                elif action == 'throwLeft':
                    throwLeft = True
                    throwRight = False
                elif action == 'throwUp':
                    throwUp = True
                    throwDown = False
                elif action == 'throwDown':
                    throwDown = True
                    throwUp = False
                
                ######################    
                #stop diital throwing#
                ######################
                elif action == 'XthrowRight':
                    throwRight = False
                elif action == 'XthrowLeft':
                    throwLeft = False
                elif action == 'XthrowUp':
                    throwUp = False
                elif action == 'XthrowDown':
                    throwDown = False 
                    
                ###########################
                #process quick termination#
                ###########################
                elif action == 'skip':
                    player.gameLevel += 1
                    return
                elif action == 'terminate':
                    utils.terminate()
                        
            ############################
            #update background and room#
            ############################
            if room == 0:
                mask = mask0
                magicColor = mask.get_at((20,20))
                area.blit(intro0, (0,0))
            elif room == 1:
                mask = mask1
                area.blit(intro1, (0,0))
            elif room == 2:
                mask = mask2
                area.blit(intro2, (0,0))
            magicColor = mask.get_at((20,20))
            
            for environ in environs:
                environ.update(area)
                
                
            #############################
            #draw collectibles into room#  
            #############################
            if currentNode.item != None:
                currentNode.item.update(area)
            
            ################
            #update enemies#
            ################
            for enemy in enemies:
                enemy.update(area, player, environs, area, room.magicColor)
            
            
            
            ##########################
            #process projectile input#
            ##########################
            if throwRight or throwLeft or throwUp or throwDown:
                player.digitalThrow(throwLeft, throwRight, throwUp, throwDown)
            elif controller != None and (abs(controller.get_axis(2)) > 0.3 or abs(controller.get_axis(3)) > 0.3):
                player.analogThrow(controller.get_axis(2), controller.get_axis(3), area)
           
           
            #########################
            #process player movement#
            #########################
            if moveRight or moveLeft or moveUp or moveDown:
                player.digitalMove(moveLeft, moveRight, moveUp, moveDown, mask, magicColor)
            elif controller != None and (abs(controller.get_axis(0)) > 0.3 or abs(controller.get_axis(1)) > 0.3):
                player.analogMove(controller.get_axis(0), controller.get_axis(1), mask, magicColor)
            else:
                player.moving = False   
            
            
            ################################################
            #process character movement in room constraints#
            ################################################
            if room == 0:
            
                if player.rect.right == AREAWIDTH:
                    player.rect.topleft = (30, 500)
                    room = 1
                    currentNode = Node1
                    if not room1:
                        secondSeq = True
                        room1 = True
                        
                    moveRight = moveLeft = moveUp = moveDown = False
                    throwRight = throwLeft = throwUp = throwDown = False
                    
            elif room == 1:
                
                if player.rect.right >= 810:
                    player.rect.topleft = (300, 650)
                    room = 2
                    thirdSeq = True
                
                elif player.rect.left == 0:
                    player.rect.topright = (970, 400)
                    currentNode = Node0
                    room = 0
                    
                    moveRight = moveLeft = moveUp = moveDown = False
                    throwRight = throwLeft = throwUp = throwDown = False
                
        elif room2:
            
            ################
            #process inputs#
            ################   
            for event in pygame.event.get():
                action = caveinput.manage(event, controller)
                
                ##########################
                #digital throwing attacks#
                ##########################    
                if action == 'throwRight':
                    throwRight = True
                    throwLeft = False
                elif action == 'throwLeft':
                    throwLeft = True
                    throwRight = False
                elif action == 'throwUp':
                    throwUp = True
                    throwDown = False
                elif action == 'throwDown':
                    throwDown = True
                    throwUp = False
                
                ######################    
                #stop diital throwing#
                ######################
                elif action == 'XthrowRight':
                    throwRight = False
                elif action == 'XthrowLeft':
                    throwLeft = False
                elif action == 'XthrowUp':
                    throwUp = False
                elif action == 'XthrowDown':
                    throwDown = False 
                    
                ###########################
                #process quick termination#
                ###########################
                elif action == 'skip':
                    player.gameLevel += 1
                    return
                elif action == 'terminate':
                    utils.terminate()
                        
            ############################
            #update background and room#
            ############################
            area.blit(intro2, (0,0))


            
            judge.update(area)
            dad.update(area)
            
            
            
            ##########################
            #process projectile input#
            ##########################
            if throwRight or throwLeft or throwUp or throwDown:
                player.digitalThrow(throwLeft, throwRight, throwUp, throwDown)
            elif controller != None and (abs(controller.get_axis(2)) > 0.3 or abs(controller.get_axis(3)) > 0.3):
                player.analogThrow(controller.get_axis(2), controller.get_axis(3), area)
                
            hit = player.updateIntro(area, enemies)
            if hit:
                player.projectiles = []
                room2 = False
                fourthSeq = True
        
        
        elif firstSeq:
            if seqCounter == 0:
                dad.rect.center = (800,500)
                area.blit(intro0, (0,0))
                dad.update(area)
                text1 = True
                seqCounter += 1
            elif seqCounter < 41:
                dad.direction = 'right'
                dad.walking = True
                dad.rect.move_ip(5,0)
                area.blit(intro0, (0,0))
                dad.update(area)
                seqCounter += 1
            elif seqCounter >= 41:
                seqCounter = 0
                firstSeq = False
                
        elif secondSeq:
            if seqCounter == 0:
                dad.rect.center = (700,540)
                dad.direction = 'left'
                area.blit(intro1, (0,0))
                dad.update(area)
                text2 = True
                seqCounter += 1
            elif seqCounter < 30:
                dad.direction = 'right'
                dad.walking = True
                dad.rect.move_ip(5,-2)
                area.blit(intro1, (0,0))
                dad.update(area)
                seqCounter += 1
            elif seqCounter >= 30:
                seqCounter = 0
                secondSeq = False
                
        elif thirdSeq:
            if seqCounter == 0:
                player.direction = 'up'
                dad.rect.topleft = (650,650)
                dad.direction = 'left'
                dad.walking = False
                area.blit(intro2, (0,0))
                dad.update(area)
                judge.rect.center = (590, 390)
                judge.update(area)
                text3 = True
                seqCounter += 1
            elif seqCounter < 5:
                area.blit(intro2, (0,0))
                dad.update(area)
                judge.update(area)
                seqCounter += 1
            elif seqCounter < 6:
                area.blit(intro2, (0,0))
                player.direction = 'right'
                dad.update(area)
                judge.update(area)
                seqCounter += 1
                text4 = True
            elif seqCounter < 10:
                area.blit(intro2, (0,0))
                dad.update(area)
                judge.update(area)
                seqCounter += 1
            elif seqCounter < 11:
                area.blit(intro2, (0,0))
                dad.update(area)
                judge.update(area)
                seqCounter += 1
                text5 = True
            elif seqCounter >= 11:
                player.direction = 'up'
                seqCounter = 0
                thirdSeq = False
                room2 = True
                enemies.append(judge)
        
        elif fourthSeq:
            if seqCounter == 0:
                area.blit(intro2, (0,0))
                player.direction = 'up'
                dad.update(area)
                judge.update(area)
                seqCounter += 1
                text6 = True
            elif seqCounter < 5:
                area.blit(intro2, (0,0))
                dad.update(area)
                judge.update(area)
                seqCounter += 1
            elif seqCounter < 6:
                area.blit(intro2, (0,0))
                dad.update(area)
                judge.update(area)
                seqCounter += 1
                text7 = True
                squirrelRect.center = judge.rect.midright
                judge.throwing = True
            elif seqCounter < 30:
                area.blit(intro2, (0,0))
                dad.update(area)
                judge.update(area)
                seqCounter += 1
                area.blit(squirrel, squirrelRect)
                squirrelRect.move_ip(-11,11)
            elif seqCounter < 32:
                judge.throwing = False
                area.blit(intro2, (0,0))
                dad.update(area)
                judge.update(area)
                seqCounter += 1
                text8 = True
            elif seqCounter < 35:
                area.blit(intro2, (0,0))
                dad.update(area)
                judge.update(area)
                seqCounter += 1
            elif seqCounter < 36:
                judge.throwing = False
                area.blit(intro2, (0,0))
                dad.update(area)
                judge.update(area)
                seqCounter += 1
                text9 = True
            elif seqCounter >= 36:
                player.gameLevel += 1
                return
            
                
            
                
            
                
                
            
            
                
                
               
        
        #######################
        #draw player to screen#
        #######################
        if not room2:
            player.update(area, enemies)
        
        screen.blit(area, (12,0))
        
        HUD.update(screen, player)
        
        pygame.display.update()
        mainClock.tick(FRAMERATE)
        #print mainClock.get_fps()