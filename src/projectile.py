import pygame, utils, random, math
from pygame.locals import *

#DAMAGES
COIN = 5
ACORN = 20
EGG = 35
EYE = 50
CLOUD = 65
PUFFER = 80

class Coin():
    
    def __init__(self, vector, playerRect, start):
        self.damage = COIN
        self.image = utils.load_trans_image('coin.png')
        self.rect = self.image.get_rect()
        self.vector = vector
        self.setStart(start, playerRect)
        self.counter = 0
        
    def setStart(self, start, playerRect):
        if start == 'topLeft':
            self.rect.bottomright = playerRect.topleft
        elif start == 'bottomLeft':
            self.rect.topright = playerRect.bottomleft
        elif start == 'midLeft':
            self.rect.midright = playerRect.midleft
        elif start == 'topRight':
            self.rect.bottomleft = playerRect.topright
        elif start == 'bottomRight':
            self.rect.topleft = playerRect.bottomright
        elif start == 'midRight':
            self.rect.midleft = playerRect.midright
        elif start == 'midTop':
            self.rect.midbottom = playerRect.midtop
        elif start == 'midBottom':
            self.rect.midtop = playerRect.midbottom
        elif start == 'center':
            self.rect.center = playerRect.center
            
    def update(self, screen):
        image = pygame.transform.rotate(self.image, self.counter*20)
        self.counter = (self.counter + 1) % 18
        screen.blit(image, self.rect)
        self.rect.move_ip(self.vector[0], self.vector[1])
        
class Acorn(Coin):
    
    def __init__(self, vector, playerRect, start):
        self.damage = ACORN
        self.image = utils.load_trans_image('acorn.png')
        self.image = pygame.transform.rotate(self.image, random.randint(0,359))
        self.rect = self.image.get_rect()
        self.vector = vector
        self.setStart(start, playerRect)
        self.counter = 0

class Egg(Coin):
    
    def __init__(self, vector, playerRect, start):
        self.damage = EGG
        self.image = utils.load_trans_image('egg1.png')
        self.image = pygame.transform.rotate(self.image, random.randint(0,359))
        self.rect = self.image.get_rect()
        self.vector = vector
        self.setStart(start, playerRect)
        self.counter = 0
        
class Eye(Coin):
    
    def __init__(self, vector, playerRect, start):
        self.damage = EYE
        self.image1 = self.image = utils.load_trans_image('eye1.png')
        self.image2 = utils.load_trans_image('eye2.png')
        self.rect = self.image1.get_rect()
        self.vector = vector
        self.setStart(start, playerRect)
        self.counter = 0
        self.counterMax = 3
        self.mode = 0
        
    def update(self, screen):
        if self.mode == 0:
            screen.blit(self.image1, self.rect)
            self.counter += 1
            if self.counter == self.counterMax:
                self.counter = 0
                self.counterMax = 2
                self.mode = 1
        elif self.mode == 1:
            screen.blit(self.image2, self.rect)
            self.counter += 1
            if self.counter == self.counterMax:
                self.counter = 0
                self.counterMax = random.randint(6,20)
                self.mode = 0    
        self.rect.move_ip(self.vector[0], self.vector[1])
        
    
class Cloud(Eye):
    
    def __init__(self, vector, playerRect, start):
        self.damage = CLOUD
        self.image1 = utils.load_trans_image('cloud1.png')
        self.image = utils.load_trans_image('cloud2.png')
        if vector != None:
            if vector[0] == 0:
                x = 0.0000001
            else:
                x = vector[0]
            rotation = -1*math.degrees(math.atan(vector[1]/x)) - 90
            self.image1 = pygame.transform.rotate(self.image1, rotation)
            self.image2 = pygame.transform.rotate(self.image, rotation)
            self.rect = self.image1.get_rect()
        self.vector = vector
        self.setStart(start, playerRect)
        self.counter = 0
        self.counterMax = 3
        self.mode = 0
        
    
class Puffer(Eye):
    
    def __init__(self, vector, playerRect, start):
        self.image = utils.load_trans_image('pufferR2.png')
        self.damage = PUFFER
        if vector != None:
            if vector[0] == 0:
                x = 0.0000001
            else:
                x = vector[0]
            if vector[0] < 0:
                rotation = -1*math.degrees(math.atan(vector[1]/x))
                self.image2 = utils.load_trans_image('pufferL1.png')
                self.image2 = pygame.transform.rotate(self.image2, rotation)
                self.image1 = utils.load_trans_image('pufferL2.png')
                self.image1 = pygame.transform.rotate(self.image1, rotation)
            else:
                rotation = -1*math.degrees(math.atan(vector[1]/x))
                self.image2 = utils.load_trans_image('pufferR1.png')
                self.image2 = pygame.transform.rotate(self.image2, rotation)
                self.image1 = utils.load_trans_image('pufferR2.png')
                self.image1 = pygame.transform.rotate(self.image1, rotation)
            self.rect = self.image2.get_rect()
        self.vector = vector
        self.setStart(start, playerRect)
        self.counter = 0
        self.counterMax = 8
        self.mode = 1


class Pea(Coin):
    
    def __init__(self, vector, shooterRect):
        self.image = utils.load_image('pea.png', -1)
        self.rect = self.image.get_rect()
        self.vector = vector
        self.rect.center = shooterRect.center
        self.counter = 0
        
class MagicFireBall(Coin):
    
    def __init__(self, vector, playerRect, damage, imageAssets):
        self.damage = damage
        self.imageAssets = imageAssets
        self.images = self.imageAssets.imageList("fireBall")
        self.imageCount = 1
        self.image = self.images[self.imageCount]
        #self.image = pygame.transform.smoothscale(tempImage, (45, 45))
        self.rect = self.image.get_rect()
        self.rect.center = playerRect.center
        self.vector = vector
        
    def update(self, screen):
        self.imageCount += 1
        if self.imageCount == 31:
            self.imageCount = 1
        self.image = self.images[self.imageCount]
        screen.blit(self.image, self.rect)
        self.rect.move_ip(self.vector[0], self.vector[1])

class BossRock(Coin):
    
    def __init__(self, vector, shooterRect):
        self.image = utils.load_trans_image('bossrock.png')
        self.image = pygame.transform.rotate(self.image, random.randint(0,359))
        self.rect = self.image.get_rect()
        self.vector = vector
        self.rect.center = shooterRect.center
        self.counter = 0
        
class BossGoo(Coin):
    
    def __init__(self, vector, shooterRect):
        self.image = utils.load_trans_image('bossrock.png')
        self.image = pygame.transform.rotate(self.image, random.randint(0,359))
        self.rect = self.image.get_rect()
        self.vector = vector
        self.rect.center = shooterRect.center
        self.counter = 0
