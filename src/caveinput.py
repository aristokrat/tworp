import pygame
from pygame.locals import *


def manage(event, controller):
            
    if event.type == JOYBUTTONDOWN:
        
        #PS3 DualShock3 controls
        if controller.get_name() == 'PLAYSTATION(R)3 Controller':
                 
            #melee attacks and abilities
            if event.button == 10: #L1 button
                return 'lightMelee'
            elif event.button == 11: #R1 button
                return 'heavyMelee'    
            elif event.button == 15: #square button
                return 'ability1'
            elif event.button == 12: #triangle button
                return 'ability2'
            elif event.button == 13: #circle button
                return 'ability3'
            elif event.button == 14: #x button
                return 'use'
            #movement (rarer on dpad)
            elif event.button == 5: #dpad right
                return 'moveRight'
            elif event.button == 7: #dpad left
                return 'moveLeft'
            elif event.button == 4: #dpad up
                return 'moveUp'
            elif event.button == 6: #dpad down
                return 'moveDown'
            
        
        
        #Xbox 360 controller controls 
        elif controller.get_name() == 'Controller':
             
            #melee attacks and abilities    
            if event.button == 8: #LB button
                return 'lightMelee'
            elif event.button == 9: #RB button
                return 'heavyMelee'
            elif event.button == 13: #X button
                return 'ability1'
            elif event.button == 14: #Y button
                return 'ability2'
            elif event.button == 12: #B button
                return 'ability3'
            elif event.button == 11: #A button
                return 'use'
            
            #movement (rarer from dpad)
            elif event.button == 3: #dpad right
                return 'moveRight'
            elif event.button == 2: #dpad left
                return 'moveLeft'
            elif event.button == 0: #dpad up
                return 'moveUp'
            elif event.button == 1: #dpad down
                return 'moveDown' 
            
            
            
    elif event.type == JOYBUTTONUP:
        

        #PS3 DualShock3 controls
        if controller.get_name() == 'PLAYSTATION(R)3 Controller':
            #stop movement
            if event.button == 5: #dpad right
                return 'XmoveRight'
            elif event.button == 7: #dpad left
                return 'XmoveLeft'
            elif event.button == 4: #dpad up
                return 'XmoveUp'
            elif event.button == 6: #dpad down
                return 'XmoveDown'
            
            elif event.button == 16: #PS button
                return 'terminate'
        
        #Xbox 360 controller controls
        elif controller.get_name() == 'Controller':
            #stop movement
            if event.button == 3: #dpad right
                return 'XmoveRight'
            elif event.button == 2: #dpad left
                return 'XmoveLeft'
            elif event.button == 0: #dpad up
                return 'XmoveUp'
            elif event.button == 1: #dpad down
                return 'XmoveDown'
            
            elif event.button == 10: #Guide button
                return 'terminate'
        
        
    elif event.type == KEYDOWN:
        
        #player one
        
        #keyboard movement
        if event.key == K_d:
            return 'moveRight'
        elif event.key == K_a:
            return 'moveLeft'
        elif event.key == K_w:
            return 'moveUp'
        elif event.key == K_s:
            return 'moveDown'
        
        #keyboard throwing attacks    
        elif event.key == K_l or event.key == K_KP6:
            return 'throwRight'
        elif event.key == K_j or event.key == K_KP4:
            return 'throwLeft'
        elif event.key == K_i or event.key == K_KP8:
            return 'throwUp'
        elif event.key == K_k or event.key == K_KP5:
            return 'throwDown'
        
        #melee attacks and abilites
        elif event.key == K_u or event.key == K_KP7:
            return 'lightMelee'
        elif event.key == K_o or event.key == K_KP9:
            return 'heavyMelee'
        elif event.key == K_n or event.key == K_KP1:
            return 'ability1'
        elif event.key == K_m or event.key == K_KP2:
            return 'ability2'
        elif event.key == K_COMMA or event.key == K_KP3:
            return 'ability3'
        elif event.key == K_SPACE or event.key == K_KP0:
            return 'use'
            
        
            
            
    elif event.type == KEYUP:
        
        #stop movement
        if event.key == K_d:
            return 'XmoveRight'
        elif event.key == K_a:
            return 'XmoveLeft'
        elif event.key == K_w:
            return 'XmoveUp'
        elif event.key == K_s:
            return 'XmoveDown'
            
        #attacks
        elif event.key == K_l or event.key == K_KP6:
            return 'XthrowRight'
        elif event.key == K_j or event.key == K_KP4:
            return 'XthrowLeft'
        elif event.key == K_i or event.key == K_KP8:
            return 'XthrowUp'
        elif event.key == K_k or event.key == K_KP5:
            return 'XthrowDown'
        
        elif event.key == K_t:
            return 'skip'
        elif event.key == K_g:
            return 'god'
            
        
        elif event.key == K_ESCAPE:
            return 'terminate'
            
    
    elif event.type == QUIT:
        return 'terminate'