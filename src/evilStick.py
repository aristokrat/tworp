import pygame, utils
from pygame.locals import *

class Stick():

    def __init__(self):
        self.heavyDamage = 100
        self.lightDamage = 40
        self.mapImage = utils.load_image('evilStickNE.png', -1)
        self.images = {}
        self.images['N'] = utils.load_image('evilStickN.png', -1)
        self.images['NNE'] = utils.load_image('evilStickNNE.png', -1)
        self.images['NE'] = utils.load_image('evilStickNE.png', -1)
        self.images['ENE'] = utils.load_image('evilStickENE.png', -1)
        
        self.images['E'] = utils.load_image('evilStickE.png', -1)
        self.images['ESE'] = utils.load_image('evilStickESE.png', -1)
        self.images['SE'] = utils.load_image('evilStickSE.png', -1)
        self.images['SSE'] = utils.load_image('evilStickSSE.png', -1)
        
        self.images['S'] = utils.load_image('evilStickS.png', -1)
        self.images['SSW'] = utils.load_image('evilStickSSW.png', -1)
        self.images['SW'] = utils.load_image('evilStickSW.png', -1)
        self.images['WSW'] = utils.load_image('evilStickWSW.png', -1)
        
        self.images['W'] = utils.load_image('evilStickW.png', -1)
        self.images['WNW'] = utils.load_image('evilStickWNW.png', -1)
        self.images['NW'] = utils.load_image('evilStickNW.png', -1)
        self.images['NNW'] = utils.load_image('evilStickNNW.png', -1)
        self.rect = None
        
    def pokeUp(self):
        return self.images['N']
    
    def pokeDown(self):
        return self.images['S']
    
    def pokeRight(self):
        return self.images['E']
    
    def pokeLeft(self):
        return self.images['W']
    
    def sweepUp(self):
        return [self.images['NW'], self.images['NNW'], self.images['N'], self.images['NNE'], self.images['NE']]
    
    def sweepRight(self):
        return [self.images['NE'], self.images['ENE'], self.images['E'], self.images['ESE'], self.images['SE']]
    
    def sweepDown(self):
        return [self.images['SE'], self.images['SSE'], self.images['S'], self.images['SSW'], self.images['SW']]
    
    def sweepLeft(self):
        return [self.images['NW'], self.images['WNW'], self.images['W'], self.images['WSW'], self.images['SW']]