#TO DO:
#erase old save file if new game is selected

import pygame, utils, title, menus, levelmanager, character, os, globals
from pygame.locals import *

SCREENWIDTH = 1024
SCREENHEIGHT = 768

def play():
    pygame.init()
    globVars = globals.Vars()
    if globVars.sound:
        pygame.mixer.music.load(os.path.join('data', 'tworpMain.wav'))
        pygame.mixer.music.play(-1)
    icon = utils.load_image_noconv('tworpIcon.png', -1)
    pygame.display.set_icon(icon)
    pygame.display.set_caption('Tworp', 'Tworp')
    screen = pygame.display.set_mode((SCREENWIDTH, SCREENHEIGHT), FULLSCREEN)
    pygame.display.set_icon(icon)
    pygame.mouse.set_visible(False)
    if pygame.joystick.get_count() == 1:
        controller = pygame.joystick.Joystick(0)
        controller.init()
        print 'initialized controller'
    else:
        controller = None
        
    
    title.titleScreen(screen)
    
    imageAssets = utils.assetLoader()
    
    while True:
        
        #display main menu (new game, load game, options, exit)
        userSelection = menus.mainMenu(screen, controller)
        
        player = None
        
        if userSelection == 'new':
            #pass through default starting values
            #perhaps create a class that stores all player variables with default constructor
            player = character.Character(imageAssets)
        elif userSelection == 'resume':
            #unpickle saved file and pass through specific values
            x = 1
        
        levelmanager.startgame(screen, player, controller, globVars, imageAssets)