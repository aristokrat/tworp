#TO DO:
#create third menu option that allows for exiting of game
#write method for determining if a saved state is present

SCREENWIDTH = 1024
SCREENHEIGHT = 768

import pygame, utils, menuinput
from pygame.locals import *


def mainMenu(screen, controller):

    if savedGameAvail():
        return drawResumeMenu(screen, controller)
    else:
        return drawNonResumeMenu(screen, controller)
    
def drawResumeMenu(screen, controller):
    
    #load static background image
    title = utils.load_image('titleScreen.png')
    filler = pygame.Surface((SCREENWIDTH,SCREENHEIGHT))
    filler.fill((255,255,255))
    background = pygame.Surface((SCREENWIDTH,SCREENHEIGHT))
    background.blit(filler, (0,0))
    background.blit(title, (0,-1*150))
    
    #load images for menu interaction
    #off means not currently selected
    #on means currently selected
    resumeGameOff = utils.load_image('menuResumeOff.png', -1)
    resumeGameOn = utils.load_image('menuResumeOn.png', -1)
    resumeGameFaded = utils.load_image('menuResumeFaded.png', -1)#when there isn't a new game available
    resumeGamePos = ((SCREENWIDTH/2)-145, 400)
    
    newGameOff = utils.load_image('menuNewOff.png', -1)
    newGameOn = utils.load_image('menuNewOn.png', -1)
    newGamePos = ((SCREENWIDTH/2)-145, 450)
    
    helpOff = utils.load_image('menuHelpOff.png', -1)
    helpOn = utils.load_image('menuHelpOn.png', -1)
    helpPos = ((SCREENWIDTH/2)-145, 500)
    
    exitOff = utils.load_image('menuExitOff.png', -1)
    exitOn = utils.load_image('menuExitOn.png', -1)
    exitPos = ((SCREENWIDTH/2)-145, 550)
    
    choices = 4
    menuState = 3    
    
    #start interaction loop
    while True:
        
        #process menu interactions
        for event in pygame.event.get():
            action = menuinput.manage(event, controller)
            
            if action == 'moveDown':
                menuState = (menuState + 1) % choices
            elif action == 'moveUp':
                menuState = (menuState - 1) % choices
            elif action == 'select':
                return mainMenuChoice(menuState)
            elif action == 'terminate':
                utils.terminate()
                
            
        screen.blit(background, (0,0))
        #if new game is currently selected, highlight new game option
        if menuState == 0:
            screen.blit(resumeGameOff, resumeGamePos)
            screen.blit(newGameOn, newGamePos)
            screen.blit(helpOff, helpPos)
            screen.blit(exitOff, exitPos)
            
        #if help is currently selected, highlight help option
        elif menuState == 1:
            screen.blit(resumeGameOff, resumeGamePos)
            screen.blit(newGameOff, newGamePos)
            screen.blit(helpOn, helpPos)
            screen.blit(exitOff, exitPos)
        
        #if exit is currently selected, highlight exit option
        elif menuState == 2:
            screen.blit(resumeGameOff, resumeGamePos)
            screen.blit(newGameOff, newGamePos)
            screen.blit(helpOff, helpPos)
            screen.blit(exitOn, exitPos)
            
        #if resume game is currently selected, highlight resume game option
        elif menuState == 3:
            screen.blit(resumeGameOn, resumeGamePos)
            screen.blit(newGameOff, newGamePos)
            screen.blit(helpOff, helpPos)
            screen.blit(exitOff, exitPos)
        
        #update display
        pygame.display.update()

def drawNonResumeMenu(screen, controller):
    
    #load static background image
    title = utils.load_image('titleScreen.png')
    filler = pygame.Surface((SCREENWIDTH,SCREENHEIGHT))
    filler.fill((255,255,255))
    background = pygame.Surface((SCREENWIDTH,SCREENHEIGHT))
    background.blit(filler, (0,0))
    background.blit(title, (0,-1*150))
    
    #load images for menu interaction
    #off means not currently selected
    #on means currently selected
    resumeGameOff = utils.load_image('menuResumeOff.png', -1)
    resumeGameOn = utils.load_image('menuResumeOn.png', -1)
    resumeGameFaded = utils.load_image('menuResumeFaded.png', -1)#when there isn't a new game available
    resumeGamePos = ((SCREENWIDTH/2)-145, 400)
    
    newGameOff = utils.load_image('menuNewOff.png', -1)
    newGameOn = utils.load_image('menuNewOn.png', -1)
    newGamePos = ((SCREENWIDTH/2)-145, 450)
    
    helpOff = utils.load_image('menuHelpOff.png', -1)
    helpOn = utils.load_image('menuHelpOn.png', -1)
    helpPos = ((SCREENWIDTH/2)-145, 500)
    
    exitOff = utils.load_image('menuExitOff.png', -1)
    exitOn = utils.load_image('menuExitOn.png', -1)
    exitPos = ((SCREENWIDTH/2)-145, 550)
    
    choices = 3
    menuState = 0    
    
    #start interaction loop
    while True:
        
        #process menu interactions
        for event in pygame.event.get():
            action = menuinput.manage(event, controller)
            
            if action == 'moveDown':
                menuState = (menuState + 1) % choices
            elif action == 'moveUp':
                menuState = (menuState - 1) % choices
            elif action == 'select':
                return mainMenuChoice(menuState)
            elif action == 'terminate':
                utils.terminate()
                
            
        screen.blit(background, (0,0))
        
        #if new game is currently selected, highlight new game option
        if menuState == 0:
            screen.blit(resumeGameFaded, resumeGamePos)
            screen.blit(newGameOn, newGamePos)
            screen.blit(helpOff, helpPos)
            screen.blit(exitOff, exitPos)
            
        #if help is currently selected, highlight help option
        elif menuState == 1:
            screen.blit(resumeGameFaded, resumeGamePos)
            screen.blit(newGameOff, newGamePos)
            screen.blit(helpOn, helpPos)
            screen.blit(exitOff, exitPos)
        
        #if exit is currently selected, highlight exit option
        elif menuState == 2:
            screen.blit(resumeGameFaded, resumeGamePos)
            screen.blit(newGameOff, newGamePos)
            screen.blit(helpOff, helpPos)
            screen.blit(exitOn, exitPos)
        
        #update display
        pygame.display.update()

        
def savedGameAvail():
    #check for presence of saved game file?
    return False

def mainMenuChoice(menuState):
    if menuState == 0:
        return 'new'
    elif menuState == 1:
        return 'help' #this should launch the help menu
    elif menuState == 2:
        utils.terminate()
    elif menuState == 3:
        return 'load'

def main():
    pygame.init()
    screen = pygame.display.set_mode((SCREENWIDTH, SCREENHEIGHT))
    pygame.display.set_caption('Menu Test')
    this = mainMenu(screen)
    print this

#calls 'main' function for testing when this script is executed   
if __name__ == '__main__': main()