import pygame, utils
from pygame.locals import *

class Dad():
    
    def __init__(self):
        self.standLeft = utils.load_trans_image('fatherStandLeft.png')
        self.standRight = utils.load_trans_image('fatherStandRight.png')
        self.walkLeft1 = utils.load_trans_image('fatherWalkLeft1.png')
        self.walkLeft2 = utils.load_trans_image('fatherWalkLeft2.png')
        self.walkRight1 = utils.load_trans_image('fatherWalkRight1.png')
        self.walkRight2 = utils.load_trans_image('fatherWalkRight2.png')
        self.rect = self.standLeft.get_rect()
        self.walking = False
        self.walkCounter = 0
        self.walkingMod = 12
        self.direction = 'left'
        
    def update(self, screen):
        if self.direction == 'left':
            if self.walking:
                if self.walkCounter < 3:
                    screen.blit(self.walkLeft1, self.rect)
                elif self.walkCounter < 6:
                    screen.blit(self.standLeft, self.rect)
                elif self.walkCounter < 9:
                    screen.blit(self.walkLeft2, self.rect)
                elif self.walkCounter < 12:
                    screen.blit(self.standLeft, self.rect)
                self.walkCounter = (self.walkCounter + 1) % 12
            else:
                screen.blit(self.standLeft, self.rect)
                self.walkCounter = 0
        else:
            if self.walking:
                if self.walkCounter < 3:
                    screen.blit(self.walkRight1, self.rect)
                elif self.walkCounter < 6:
                    screen.blit(self.standRight, self.rect)
                elif self.walkCounter < 9:
                    screen.blit(self.walkRight2, self.rect)
                elif self.walkCounter < 12:
                    screen.blit(self.standRight, self.rect)
                self.walkCounter = (self.walkCounter + 1) % 12
            else:
                screen.blit(self.standRight, self.rect)
                self.walkCounter = 0
                
class Judge():
    
    def __init__(self):
        self.stand = utils.load_trans_image('judgeStand.png')
        self.throw = utils.load_trans_image('judgeThrow.png')
        self.rect = self.stand.get_rect()
        self.throwing = False
        
    def update(self, screen):
        if self.throwing:
            screen.blit(self.throw, self.rect)
        else:
            screen.blit(self.stand, self.rect)
            
    def takeThrowDamage(self, damage):
        True
        
        