import pygame, levelgen, roomgen, utils, background, teleporter, item, os, words
from pygame.locals import *
import caveinput, enemyPopulate, random, interface, environ, leveler, math, ladder

FRAMERATE = 30

SCREENWIDTH = 1024
SCREENHEIGHT = 768
AREAWIDTH = 1000
AREAHEIGHT = 760

#for preview, change desired effect value to 1
DARKNESSLEVEL = 1
LIGHT_RADIUS = 300

FLASHLEVEL = 2
WATERLEVEL = 3
PINWHEELLEVEL = 4


def startlevel(screen, player, controller, globVars, imageAssets):
    
    mainClock = pygame.time.Clock()
    pygame.key.set_repeat(50)
    if globVars.sound:
        pygame.mixer.music.load(os.path.join('data', 'tworpLevel.wav'))
        pygame.mixer.music.play(-1)
    
    HUD = interface.UI()
    area = pygame.Surface((AREAWIDTH, AREAHEIGHT))
    areaRect = area.get_rect()
    areaRect.topleft = (12, 0)

    levelMap = levelgen.Map(player.gameLevel)
            
    currentMapNode = levelMap.firstNode
    currentMapNode.visited = True
    roomEntryEdge = None
    roomEntryStart = None
    roomEntryWidth = None
    bg = background.Background()
    moveRight = moveLeft = moveUp = moveDown = False
    throwRight = throwLeft = throwUp = throwDown = False
    teleport = None
    enemies = []
    secretNode = None
    
    #level effect variables
    effect = False
    effectCounter = 0
    effectCounterMax = 40
    pinwheeltracker = 0
    effectChoice = 0
    
    #load level effects
    if player.gameLevel == DARKNESSLEVEL:
        darkness = utils.load_trans_image('darkness.png')
        darknessRect = darkness.get_rect()
    elif player.gameLevel == PINWHEELLEVEL:
        redE = utils.load_trans_image('redE.png')
        redNE = utils.load_trans_image('redNE.png')
        redN = utils.load_trans_image('redN.png')
        redNW = utils.load_trans_image('redNW.png')
        redW = utils.load_trans_image('redW.png')
        redSW = utils.load_trans_image('redSW.png')
        redS = utils.load_trans_image('redS.png')
        redSE = utils.load_trans_image('redSW.png')
        redRect = redE.get_rect()
        reds = [redE, redNE, redN, redNW, redW, redSW, redS, redSE]
    elif player.gameLevel == FLASHLEVEL:
        whiteOut = utils.load_trans_image('whiteOut.png')
    elif player.gameLevel == WATERLEVEL:
        water = background.Water()
    
    #determine if secret room is adjacent to this room
    if currentMapNode.left != None and currentMapNode.left.roomName == 'secret':
        secretNode = 'left'
    elif currentMapNode.right != None and currentMapNode.right.roomName == 'secret':
        secretNode = 'right'
    elif currentMapNode.up != None and currentMapNode.up.roomName == 'secret':
        secretNode = 'up'
    elif currentMapNode.down != None and currentMapNode.down.roomName == 'secret':
        secretNode = 'down'
    
    #get flattened image of room from roomgenerator
    room = roomgen.Room(roomEntryEdge, roomEntryStart, roomEntryWidth, currentMapNode, secretNode, None, player)
    
    player.rect.topleft = (AREAWIDTH/3, 400)  #NEEDS TO COME FROM FALLING ANIMATION!!!!!!!!!!!
    
    climbing = False
    falling = False
    introCounter = 0
    levelText = False
    
    #animate tworp falling into first level of dungeon, in darkness, and finding stick
    if player.gameLevel == 1:
        player.rect.center = (370, -80)
        currentMapNode.item = item.weaponItem('stick')
        enemies = enemyPopulate.first(imageAssets)
        player.climbing = True
        falling = True
        smoke1 = utils.load_trans_image('smoke0.png')
        smoke2 = utils.load_trans_image('smoke1.png')
        smoke3 = utils.load_trans_image('smoke2.png')
        smoke4 = utils.load_trans_image('smoke3.png')
        smoke5 = utils.load_trans_image('smoke4.png')
        smoke6 = utils.load_trans_image('smoke5.png')
        smoke7 = utils.load_trans_image('smoke6.png')
        smoke8 = utils.load_trans_image('smoke7.png')
    else:
        currentMapNode.item = ladder.LadderHole()
        player.rect.midbottom = currentMapNode.item.rect.midbottom
        player.climbing = True
        climbing = True
        
    
        
    

    ##################
    #start event loop#
    ##################
    while True:
        
        if falling:
            if introCounter == 14:
                words.drawText(screen, 1, 1)
            if introCounter == 29:
                words.drawText(screen, 1, 2)
            if introCounter == 59:
                words.drawText(screen, 1, 3)
                
        if levelText:
            words.drawText(screen, player.gameLevel, 1)
            levelText = False
            
        if (currentMapNode.roomName == 'branchEnd' or currentMapNode.roomName == 'secret') and not player.seenItem:
            words.drawText(screen, 'gen', 'item')
            player.seenItem = True
            moveRight = moveLeft = moveUp = moveDown = False
            throwRight = throwLeft = throwUp = throwDown = False
        elif currentMapNode.roomName == 'penultimate' and not player.seenBoss:
            words.drawText(screen, 'gen', 'boss')
            player.seenBoss = True
            moveRight = moveLeft = moveUp = moveDown = False
            throwRight = throwLeft = throwUp = throwDown = False
                
        
        
        if not falling and not climbing:
        
            ################
            #process inputs#
            ################   
            for event in pygame.event.get():
                action = caveinput.manage(event, controller)
                
                #########################
                #digital movement inputs#
                #########################
                if action == 'moveRight':
                    moveRight = True
                    moveLeft = False
                elif action == 'moveLeft':
                    moveLeft = True
                    moveRight = False
                elif action == 'moveUp':
                    moveUp = True
                    moveDown = False
                elif action == 'moveDown':
                    moveDown = True
                    moveUp = False
                    
                #############################
                #melee attacks and abilities#
                #############################
                elif action == 'heavyMelee':
                    player.heavyMelee()
                elif action == 'lightMelee':
                    player.lightMelee()
                elif action == 'use':
                    player.use(currentMapNode)
                elif action == 'ability1':
                    player.ability('freeze', enemies)
                elif action == 'ability2':
                    player.ability('fire', enemies)
                elif action == 'ability3':
                    player.ability('hide', enemies)
                
                #######################    
                #stop digital movement#
                #######################
                elif action == 'XmoveRight':
                    moveRight = False
                elif action == 'XmoveLeft':
                    moveLeft = False
                elif action == 'XmoveUp':
                    moveUp = False
                elif action == 'XmoveDown':
                    moveDown = False
                    
                ##########################
                #digital throwing attacks#
                ##########################    
                elif action == 'throwRight':
                    throwRight = True
                    throwLeft = False
                elif action == 'throwLeft':
                    throwLeft = True
                    throwRight = False
                elif action == 'throwUp':
                    throwUp = True
                    throwDown = False
                elif action == 'throwDown':
                    throwDown = True
                    throwUp = False
                
                ######################    
                #stop diital throwing#
                ######################
                elif action == 'XthrowRight':
                    throwRight = False
                elif action == 'XthrowLeft':
                    throwLeft = False
                elif action == 'XthrowUp':
                    throwUp = False
                elif action == 'XthrowDown':
                    throwDown = False 
                    
                ###########################
                #process quick termination#
                ###########################
                elif action == 'skip':
                    return None, None
                elif action == 'god':
                    player.godmode()
                elif action == 'terminate':
                    utils.terminate()
                        
            ######################################
            #process character moving out of room#
            ######################################
            if player.rect.right == AREAWIDTH:
                lastRoomName = currentMapNode.roomName
                currentMapNode = currentMapNode.right
                player.rect.left = 30
                teleport = None
                room, enemies = switchRooms('left', room.rightStart, room.rightWidth, currentMapNode, lastRoomName, player, enemies, globVars, imageAssets)
                if currentMapNode.roomName == 'last':
                    return room, currentMapNode
                    
            elif player.rect.left == 0:
                lastRoomName = currentMapNode.roomName
                currentMapNode = currentMapNode.left
                player.rect.right = AREAWIDTH-30
                teleport = None
                room, enemies = switchRooms('right', room.leftStart, room.leftWidth, currentMapNode, lastRoomName, player, enemies, globVars, imageAssets)
                if currentMapNode.roomName == 'last':
                    return room, currentMapNode
            
            elif player.rect.bottom == AREAHEIGHT:
                lastRoomName = currentMapNode.roomName
                currentMapNode = currentMapNode.down
                player.rect.top = 30
                teleport = None
                room, enemies = switchRooms('top', room.bottomStart, room.bottomWidth, currentMapNode, lastRoomName, player, enemies, globVars, imageAssets)
                if currentMapNode.roomName == 'last':
                    return room, currentMapNode
                
            elif player.rect.top == 0:
                lastRoomName = currentMapNode.roomName
                currentMapNode = currentMapNode.up
                player.rect.bottom = AREAHEIGHT-30
                teleport = None
                room, enemies = switchRooms('bottom', room.topStart, room.topWidth, currentMapNode, lastRoomName, player, enemies, globVars, imageAssets)
                if currentMapNode.roomName == 'last':
                    return room, currentMapNode
            
            
            ############################
            #update background and room#
            ############################
            bg.update(area)
            room.update(area, len(enemies))
            for environ in currentMapNode.environs:
                environ.update(area)
            
            ################
            #update enemies#
            ################
            for enemy in enemies:
                enemy.update(area, player, currentMapNode.environs, area, room.magicColor)
            #if player dies
            #should call a game over screen?
            if player.health <= 0:
                if globVars.sound:
                    pygame.mixer.music.load(os.path.join('data', 'tworpMain.wav'))
                    pygame.mixer.music.play(-1)
                return None, None
            
            
            #############################
            #draw collectibles into room#  
            #############################
            if currentMapNode.item != None:
                currentMapNode.item.update(area)
            
            
            ##########################
            #process projectile input#
            ##########################
            if throwRight or throwLeft or throwUp or throwDown:
                player.digitalThrow(throwLeft, throwRight, throwUp, throwDown)
            elif controller != None and (abs(controller.get_axis(2)) > 0.3 or abs(controller.get_axis(3)) > 0.3):
                player.analogThrow(controller.get_axis(2), controller.get_axis(3), area)
           
           
            #########################
            #process player movement#
            #########################
            if moveRight or moveLeft or moveUp or moveDown:
                player.digitalMove(moveLeft, moveRight, moveUp, moveDown, area, room.magicColor)
            elif controller != None and (abs(controller.get_axis(0)) > 0.3 or abs(controller.get_axis(1)) > 0.3):
                player.analogMove(controller.get_axis(0), controller.get_axis(1), area, room.magicColor)
            else:
                player.moving = False
                
        elif climbing:
            ############################
            #update background and room#
            ############################
            bg.update(area)
            room.update(area, len(enemies))
            for environ in currentMapNode.environs:
                environ.update(area)
            
            ################
            #update enemies#
            ################
            for enemy in enemies:
                enemy.update(area, player, currentMapNode.environs, area, room.magicColor)
            #if player dies
            #should call a game over screen?
            if player.health <= 0:
                if globVars.sound:
                    pygame.mixer.music.load(os.path.join('data', 'tworpMain.wav'))
                    pygame.mixer.music.play(-1)
                return None, None
            
            
            #############################
            #draw collectibles into room#  
            #############################
            if currentMapNode.item != None:
                currentMapNode.item.update(area)
                
            if player.rect.bottom > 320:
                player.rect.move_ip(0, -5)
                
            else:
                player.climbing = False
                climbing = False
                player.direction = 'up'
                levelText = True
                
        elif falling:
            ############################
            #update background and room#
            ############################
            bg.update(area)
            room.update(area, len(enemies))
            for environ in currentMapNode.environs:
                environ.update(area)
            
            ################
            #update enemies#
            ################
            for enemy in enemies:
                enemy.update(area, player, currentMapNode.environs, area, room.magicColor)
            #if player dies
            #should call a game over screen?
            if player.health <= 0:
                if globVars.sound:
                    pygame.mixer.music.load(os.path.join('data', 'tworpMain.wav'))
                    pygame.mixer.music.play(-1)
                return None, None
            
            
            #############################
            #draw collectibles into room#  
            #############################
            if currentMapNode.item != None:
                currentMapNode.item.update(area)
                
            if player.rect.centery < 380:
                player.rect.move_ip(0, 15)
                
            elif introCounter < 15:
                player.climbing = False
                climbing = False
                player.direction = 'up'
                introCounter += 1
                
            
            elif introCounter < 30:
                player.direction = 'down'
                introCounter += 1
                
            elif introCounter < 45:
                player.direction = 'left'
                introCounter += 1
                
            elif introCounter < 60:
                player.direction = 'right'
                introCounter += 1
            
            elif introCounter >= 60: 
                falling = False
                

        

        
        
       
        
        
        ######################
        #draw pinwheel effect#
        ######################
        if player.gameLevel == PINWHEELLEVEL:
            redRect.center = player.rect.center
            area.blit(reds[pinwheeltracker], redRect)
            pinwheeltracker = (pinwheeltracker + 1) % 8 #randomize spinning pattern on each glitch?'''
        
        ###############################
        #process water effect movement#
        ###############################
        if player.gameLevel == WATERLEVEL and not climbing:
            x, y = water.getMove()
            player.waterMove(x, y, area, room.magicColor)
            for enemy in enemies:
                enemy.waterMove(x, y, area, room.magicColor)
        
        #######################
        #draw player to screen#
        #######################
        player.update(area, enemies)
        
        if falling:
            smoke = pygame.Rect(0,0,257,160)
            smoke.center = player.rect.midbottom
            if introCounter == 1:
                area.blit(smoke1, smoke)
            elif introCounter == 2:
                area.blit(smoke2, smoke)
            elif introCounter == 3:
                area.blit(smoke3, smoke)
            elif introCounter == 4:
                area.blit(smoke4, smoke)
            elif introCounter == 5:
                area.blit(smoke5, smoke)
            elif introCounter == 6:
                area.blit(smoke6, smoke)
            elif introCounter == 7:
                area.blit(smoke7, smoke)
            elif introCounter == 8:
                area.blit(smoke8, smoke)
            
            
            
        ######################
        #draw darkness effect#
        ######################
        if player.gameLevel == DARKNESSLEVEL:
            darknessRect.center = player.rect.center
            area.blit(darkness, darknessRect)
            for enemy in enemies:
                xDistance = enemy.rect.centerx - player.rect.centerx
                yDistance = enemy.rect.centery - player.rect.centery
                distance = int(round(math.sqrt(xDistance**2 + yDistance**2),0))
                if distance < LIGHT_RADIUS:
                    enemy.inDark = False
                else:
                    enemy.inDark = True
            screen.blit(area, areaRect)
                    
        
        ###################
        #draw water effect#
        ###################
        if player.gameLevel == WATERLEVEL:
            water.update(area)
            screen.blit(area, areaRect)
        
        
        ##################
        #make level shake#
        ##################
        #may lose in final?
        if player.gameLevel == PINWHEELLEVEL:
            if effect:
                if effectChoice >= 0 and effectChoice <= 4:
                    effectedArea = pygame.transform.flip(area, False, True)
                    screen.blit(effectedArea, areaRect)
                elif effectChoice > 4 and effectChoice <= 9:
                    effectedArea = pygame.transform.flip(area, True, False)
                    screen.blit(effectedArea, areaRect)
                elif effectChoice > 9 and effectChoice <= 13:
                    effectedArea = pygame.transform.flip(area, True, True)
                    screen.blit(effectedArea, areaRect) 
                elif effectChoice == 14:
                    screen.fill((0,0,0))
                    effectedArea = pygame.transform.rotozoom(area, 90, 0.76)
                    screen.blit(effectedArea, (223,0))
                elif effectChoice == 15:
                    screen.fill((0,0,0))
                    effectedArea = pygame.transform.rotozoom(area, -90, 0.76)
                    screen.blit(effectedArea, (223,0))
                elif effectChoice == 16 or effectChoice == 17:
                    screen.fill((0,0,0))
                    shrink = (500,380)
                    effectedArea = pygame.Surface(shrink)
                    pygame.transform.smoothscale(area, shrink, effectedArea)
                    screen.blit(effectedArea, (262, 190))
                elif effectChoice == 18 or effectChoice == 19:
                    zoom = (2000,1520)
                    effectedArea = pygame.Surface(zoom)
                    pygame.transform.smoothscale(area, zoom, effectedArea)
                    screen.blit(effectedArea, (-488, -380))
                effectCounter += 1
                if effectCounter == effectCounterMax:
                    effect = False
                    effectCounter = 0
                    effectCounterMax = random.randint(40,80)
            else:
                screen.blit(area, areaRect)                
                effectCounter += 1
                if effectCounter == effectCounterMax:
                    effect = True
                    effectCounter = 0
                    effectCounterMax = 4#random.randint(3,10)
                    effectChoice = random.randint(0,19)
            
                            
            '''while True:
                tempRect = pygame.Rect(areaRect)
                s = 1
                if random.randint(0,1) == 1:
                    s = -1
                direction = random.choice('xy')
                if direction == 'x':
                    tempRect.move_ip(s, 0)
                else:
                    tempRect.move_ip(0, s)
                if tempRect.left > 0 and tempRect.right < SCREENWIDTH and tempRect.top > 0 and tempRect.bottom < SCREENHEIGHT:
                    areaRect.topleft = tempRect.topleft
                    break'''

        ######################
        #process flash effect#
        ######################
        if player.gameLevel == FLASHLEVEL:
            #pygame.draw.rect(area, (0,0,0), pygame.Rect(300,200,400,360), 1) #temp rectangle for showing safe area
            if effect:
                #invArea = utils.inverted(area)
                #screen.blit(invArea, (12,4))
                area.blit(whiteOut, (0,0))
                
                effectCounter += 1
                if effectCounter == effectCounterMax:
                    if player.rect.left > 300 and player.rect.right < 700 and player.rect.top > 200 and player.rect.bottom < 560:
                        secretNode = None
                        if currentMapNode.left != None and currentMapNode.left.roomName == 'secret':
                            secretNode = 'left'
                        elif currentMapNode.right != None and currentMapNode.right.roomName == 'secret':
                            secretNode = 'right'
                        elif currentMapNode.up != None and currentMapNode.up.roomName == 'secret':
                            secretNode = 'up'
                        elif currentMapNode.down != None and currentMapNode.down.roomName == 'secret':
                            secretNode = 'down'
                        room = roomgen.Room(None, None, None, currentMapNode, secretNode, None, player)
                    for enemy in enemies:
                        enemy.phase()
                    effect = False
                    effectCounter = 0
                    effectCounterMax = random.randint(40,80)
            else:
                effectCounter += 1
                if effectCounter == effectCounterMax:
                    effect = True
                    effectCounter = 0
                    effectCounterMax = random.randint(3,10)
            screen.blit(area, areaRect)
        
            
        HUD.update(screen, player)
        
        pygame.display.update()
        mainClock.tick(FRAMERATE)
        #print mainClock.get_fps()
        
        if player.levelUp == True:
            leveler.Up(screen, area, areaRect, player, controller)
            moveRight = moveLeft = moveUp = moveDown = False
            throwRight = throwLeft = throwUp = throwDown = False
            
        

def switchRooms(roomEntryEdge, roomEntryStart, roomEntryWidth, currentMapNode, lastRoomName, player, enemies, globVars, imageAssets):
    player.projectiles = []
    player.fireballs = []
    enemies = []
    if not currentMapNode.visited:
        '''if currentMapNode.roomName == 'last':
            enemies = [enemy.Boss(imageAssets)]
            if globVars.sound:
                pygame.mixer.music.load(os.path.join('data', 'tworpBoss.wav'))
                pygame.mixer.music.play(-1)'''
        if currentMapNode.roomName == 'branchEnd' or currentMapNode.roomName == 'secret':
            getItem(player, currentMapNode)      
        else:
            things = enemyPopulate.fill(imageAssets, player.gameLevel)
            for thing in things:
                if thing.type == 'enemy':
                    enemies.append(thing)
                else:
                    currentMapNode.environs.append(thing)
        currentMapNode.visited = True
                
    #determine if secret room is adjacent to this room
    secretNode = None
    if currentMapNode.left != None and currentMapNode.left.roomName == 'secret':
        secretNode = 'left'
    elif currentMapNode.right != None and currentMapNode.right.roomName == 'secret':
        secretNode = 'right'
    elif currentMapNode.up != None and currentMapNode.up.roomName == 'secret':
        secretNode = 'up'
    elif currentMapNode.down != None and currentMapNode.down.roomName == 'secret':
        secretNode = 'down'
            
    return roomgen.Room(roomEntryEdge, roomEntryStart, roomEntryWidth, currentMapNode, secretNode, lastRoomName, player), enemies


def getItem(player, currentMapNode):
    
    if currentMapNode.roomName == 'secret' or random.randint(0,2) == 0:
        thisItem = None
        choices = ['weapon', 'projectile', 'magic']
        while len(choices) != 0:
            choice = random.choice(choices)
            if choice == 'weapon':
                thisItem = item.getNextWeapon(player)
                if thisItem == 'out':
                    choices.remove('weapon')
                else:
                    currentMapNode.item = item.weaponItem(thisItem)
                    break
            elif choice == 'projectile':
                thisItem = item.getNextProjectile(player)
                if thisItem == 'out':
                    choices.remove('projectile')
                else:
                    currentMapNode.item = item.projectileItem(thisItem)
                    break
            elif choice == 'magic':
                thisItem = item.getNextMagic(player)
                if thisItem == 'out':
                    choices.remove('magic')
                else:
                    currentMapNode.item = item.magicItem(thisItem)
                    break
        if thisItem == 'out':
            currentMapNode.item = item.healthItem()    
    else:
        currentMapNode.item = item.healthItem()



