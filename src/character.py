import utils, pygame, time, math, throwManager, projectile, cavemanager
from pygame.locals import *

##################
#TUNING VARIABLES#
##################
HEALTH = 400
HEALTHPLUS = 100
MOVE = 11   #absolute max is 29 to avoid teleporting through walls
MOVEPLUS = 1

MANA = 300
MANAPLUS = 100
MANA_REGEN_RATE = 1


LIGHT_DAMAGE = 2
LIGHT_KNOCKBACK = 1
LIGHT_COOLDOWN = 0

HEAVY_DAMAGE = 5
HEAVY_KNOCKBACK = 5
HEAVY_ANIM_FRAMES = 15 #must be 7 or bigger
HEAVY_COOLDOWN = 0

THROW_DELAY = 0.5
THROW_DELAY_PLUS = 0.03
MAX_SIMUL_THROWS = 3
MAX_SIMUL_THROWS_PLUS = 1
THROW_SPEED = 10
THROW_SPEED_PLUS = 3
THROW_DAMAGE_PLUS = 2

ICE_TIME = 60
ICE_TIME_PLUS = 10
ICE_MANA_LOSS = 100

FIRE_DAMAGE = 1 #fireballs continue to sweep over enemy
FIRE_MANA_LOSS = 150

HIDE_HEALTH_REGEN_RATE = 30
HIDE_HEALTH_PLUS = 5
HIDE_MANA_LOSS_RATE = 10


ITEM_PICKUP_DISTANCE = 60

LEVEL_EXP = 300

#################E#####
#asset state variables#
#######################
AREAWIDTH = 1000
AREAHEIGHT = 760

HEADSIZE = (50, 47)
BODYSIZE = (50, 23)
LEGSIZE = (50, 10)


#################
#character class#
#################
class Character():
    
    ##############################
    #constructor and image loader#
    ##############################
    def __init__(self, assets):
        ############
        #attributes#
        ############
        self.gameLevel = 0
        self.playerLevel = 1
        self.levelUp = False
        self.god = False
        self.combatLevel = 0
        self.magicLevel = 0
        self.healthLevel = 0
        
        self.iceLevel = 0
        self.fireLevel = 0
        self.hideLevel = 0
        
        self.weaponLightDamage = 0
        self.weaponHeavyDamage = 0
        self.weaponLightKnockback = 0
        self.weaponHeavyKnockback = 0
        self.projectileDamage = 0
        
        #combat specs
        self.throwTimeDelay = THROW_DELAY - self.combatLevel * THROW_DELAY_PLUS
        self.maxSimulThrows = MAX_SIMUL_THROWS + self.combatLevel * MAX_SIMUL_THROWS_PLUS
        self.throwSpeed = THROW_SPEED + self.combatLevel * THROW_SPEED_PLUS
        self.throwDamageBonus = self.combatLevel * THROW_DAMAGE_PLUS
        
        self.heavyDamageBonus = HEAVY_DAMAGE * self.combatLevel
        self.heavyKnockbackBonus = HEAVY_KNOCKBACK * self.combatLevel
        self.lightDamageBonus = LIGHT_DAMAGE * self.combatLevel
        self.lightKnockbackBonus = LIGHT_KNOCKBACK * self.combatLevel
        
        #magic specs
        self.maxMana = MANA + self.magicLevel*MANAPLUS
        self.manaRegen = MANA_REGEN_RATE + self.magicLevel
        
        self.iceTimeBonus = ICE_TIME_PLUS * self.magicLevel
        self.fireDamageBonus = self.magicLevel
        self.hideHealthBonus = HIDE_HEALTH_PLUS * self.magicLevel
        
        
        
        #health specs
        self.maxHealth = HEALTH + self.healthLevel*HEALTHPLUS
        self.maxMoveRate = MOVE + self.healthLevel*MOVEPLUS
        
        
        
        #ability specs
        self.iceTime = ICE_TIME * self.iceLevel
        self.fireDamage = FIRE_DAMAGE * self.fireLevel
        self.hideHealthRegen = HIDE_HEALTH_REGEN_RATE * self.hideLevel
        
        
        #god mode containers
        self.oldthrowTimeDelay = 0
        self.oldmaxSimulThrows = 0
        self.oldthrowSpeed = 0
        self.oldthrowDamageBonus = 0
        self.oldheavyDamageBonus = 0
        self.oldlightDamageBonus = 0
        self.oldmanaRegen = 0
        self.oldiceTimeBonus = 0
        self.oldfireDamageBonus = 0
        self.oldhideHealthBonus = 0
        self.oldiceLevel = 0
        self.oldfireLevel = 0
        self.oldhideLevel = 0
        self.oldmaxHealth = 0
        
        
        
        
        #basic specs
        self.moveRate = self.maxMoveRate
        self.exp = 0
        self.expThreshold = LEVEL_EXP*self.playerLevel
        self.health = self.maxHealth
        self.mana = 0
        
        
        ##########################
        #state tracking variables#
        ##########################
        #moving
        self.direction = 'down'
        self.moving = False
        self.movingCounter = 0
        self.stunned = False
        #meleeing
        self.weapon = None
        self.meleeing = None
        self.meleeHits = []
        self.meleeCounter = 0
        #throwing
        self.projectile = None
        self.throwing = False
        self.throwingCounter = 0
        self.projectiles = []
        self.lastThrowTime = time.time()
        #abilities
        self.fireballs = []
        self.hidden = False
        self.hideCounter = 0
        
        ###################################
        #load images into image dictionary#
        ###################################
        self.assets = assets
        self.images = {}
        self.loadImages()
        
        self.climb1 = utils.load_trans_image("tworpClimb1.png")
        self.climb2 = utils.load_trans_image("tworpClimb2.png")
        self.climbing = False
        self.climbCounter = 0
        
        ########################
        #create character rects#
        ########################
        self.rect = pygame.Rect(0, 0, 40, 80)
        self.lastRect = pygame.Rect(0, 0, 40, 80)
        self.headRect = pygame.Rect((0, 0), HEADSIZE)
        self.bodyRect = pygame.Rect((0, 0), BODYSIZE)
        self.legRect = pygame.Rect((0, 0), LEGSIZE)
        
        
        #message tracking
        self.seenBoss = False
        self.seenItem = False
        self.usedLadder = False

    
    def godmode(self):
        if not self.god:
            self.god = True
            self.oldthrowTimeDelay = self.throwTimeDelay
            self.oldmaxSimulThrows = self.maxSimulThrows
            self.oldthrowSpeed = self.throwSpeed
            self.oldthrowDamageBonus = self.throwDamageBonus
            self.oldheavyDamageBonus = self.heavyDamageBonus
            self.oldlightDamageBonus = self.lightDamageBonus
            self.oldmanaRegen = self.manaRegen
            self.oldiceTimeBonus = self.iceTimeBonus
            self.oldfireDamageBonus = self.fireDamageBonus
            self.oldhideHealthBonus = self.hideHealthBonus
            self.oldiceLevel = self.iceLevel
            self.oldfireLevel = self.fireLevel
            self.oldhideLevel = self.hideLevel
            self.oldmaxHealth = self.maxHealth
            
            self.throwTimeDelay = 0.2
            self.maxSimulThrows = 10
            self.throwSpeed = 40
            self.throwDamageBonus = 80
            self.heavyDamageBonus = 300
            self.lightDamageBonus = 100
            self.manaRegen = 1000
            self.iceTimeBonus = 120
            self.fireDamageBonus = 2
            self.hideHealthBonus = 100
            self.iceLevel = 2
            self.fireLevel = 2
            self.hideLevel = 2
            self.maxHealth = 40000
            self.health = self.maxHealth
        
        elif self.god:
            self.god = False
            self.throwTimeDelay = self.oldthrowTimeDelay
            self.maxSimulThrows = self.oldmaxSimulThrows
            self.throwSpeed = self.oldthrowSpeed
            self.throwDamageBonus = self.oldthrowDamageBonus
            self.heavyDamageBonus = self.oldheavyDamageBonus
            self.lightDamageBonus = self.oldlightDamageBonus
            self.manaRegen = self.oldmanaRegen
            self.iceTimeBonus = self.oldiceTimeBonus
            self.fireDamageBonus = self.oldfireDamageBonus
            self.hideHealthBonus = self.oldhideHealthBonus
            self.iceLevel = self.oldiceLevel
            self.fireLevel = self.oldfireLevel
            self.hideLevel = self.oldhideLevel
            self.maxHealth = self.oldmaxHealth
            self.health = self.maxHealth
            
        
    
    ###############
    #refill health#
    ###############
    def refillHeatlh(self):
        self.health += self.maxHealth / 2
        if self.health > self.maxHealth:
            self.health = self.maxHealth
        
    #############
    #take damage#
    #############
    def takeDamage(self, damage):
        self.health -= damage
    
    ############################
    #interact with item in room#
    ############################
    def use(self, Node):
        item = Node.item
        if item != None:
            xDistance = self.rect.centerx - item.rect.centerx
            yDistance = self.rect.centery - item.rect.centery
            distance = int(round(math.sqrt(xDistance**2 + yDistance**2),0))
            
            if distance < ITEM_PICKUP_DISTANCE:
                if item.type == 'ladder':
                    return True
                else:
                    self.addItem(item)
                    Node.item = None
                
    ###################################
    #add collectible item to inventory#
    ###################################
    def addItem(self, item):
        if item.type == 'weapon':
            self.weapon = item.item
            self.weaponLightDamage = item.item.lightDamage
            self.weaponHeavyDamage = item.item.heavyDamage
            self.weaponLightKnockback = item.item.lightKnockback
            self.weaponHeavyKnockback = item.item.heavyKnockback
        elif item.type == 'projectile':
            self.projectile = item.name
            self.projectileDamage = item.item.damage
        elif item.type == 'health':
            self.refillHeatlh()
        elif item.type == 'magic':
            if item.name == 'fire1':
                self.fireLevel = 1
            elif item.name == 'fire2':
                self.fireLevel = 2
            elif item.name == 'ice1':
                self.iceLevel = 1
            elif item.name == 'ice2':
                self.iceLevel = 2
            elif item.name == 'hide1':
                self.hideLevel = 1
            elif item.name == 'hide2':
                self.hideLevel = 2
            self.iceTime = ICE_TIME * self.iceLevel
            self.fireDamage = FIRE_DAMAGE * self.fireLevel
            self.hideHealthRegen = HIDE_HEALTH_REGEN_RATE * self.hideLevel
            
            
    def levelCombat(self):
        self.combatLevel += 1
        self.throwTimeDelay = THROW_DELAY - self.combatLevel * THROW_DELAY_PLUS
        self.maxSimulThrows = MAX_SIMUL_THROWS + self.combatLevel * MAX_SIMUL_THROWS_PLUS
        self.throwSpeed = THROW_SPEED + self.combatLevel * THROW_SPEED_PLUS
        self.throwDamageBonus = self.combatLevel * THROW_DAMAGE_PLUS
        self.heavyDamageBonus = HEAVY_DAMAGE * self.combatLevel
        self.heavyKnockbackBonus = HEAVY_KNOCKBACK * self.combatLevel
        self.lightDamageBonus = LIGHT_DAMAGE * self.combatLevel
        self.lightKnockbackBonus = LIGHT_KNOCKBACK * self.combatLevel
        
    def levelMagic(self):
        self.magicLevel += 1
        self.maxMana = MANA + self.magicLevel*MANAPLUS
        self.manaRegen = MANA_REGEN_RATE + self.magicLevel
        self.iceTimeBonus = ICE_TIME_PLUS * self.magicLevel
        self.fireDamageBonus = self.magicLevel
        self.hideHealthBonus = HIDE_HEALTH_PLUS * self.magicLevel
        
    def levelHealth(self):
        self.healthLevel += 1
        factor = float(self.health)/float(self.maxHealth)
        self.maxHealth = HEALTH + self.healthLevel*HEALTHPLUS
        self.health = int(factor * self.maxHealth)
        self.maxMoveRate = MOVE + self.healthLevel*MOVEPLUS
    
    
    #######################
    #process ability input#
    #######################
    def ability(self, ability, enemies):
        if ability == 'freeze' and self.iceLevel > 0 and self.mana >= ICE_MANA_LOSS:
            self.mana -= ICE_MANA_LOSS
            for enemy in enemies:
                if self.iceLevel == 1:
                    enemy.freeze(self.iceTime + self.iceTimeBonus)
                elif self.iceLevel == 2:
                    enemy.freeze(self.iceTime + self.iceTimeBonus, True)
        elif ability == 'fire' and self.fireLevel > 0 and self.mana >= FIRE_MANA_LOSS:
            self.mana -= FIRE_MANA_LOSS
            self.flameOn()
        elif ability == 'hide' and self.hideLevel > 0:
            if self.hidden:
                self.hidden = False
            elif self.mana >= HIDE_MANA_LOSS_RATE:
                self.hidden = True
                self.meleeing = None
                self.throwing = False
                self.throwingCounter = 0
                self.meleeCounter = 0
                
    def waterMove(self, x, y, room, magicColor):
        if x > 0:
            self.moveRight(room, magicColor, x)
        elif x < 0:
            self.moveLeft(room, magicColor, 0-x)
        if y > 0:
            self.moveDown(room, magicColor, y)
        elif y < 0:
            self.moveUp(room, magicColor, 0-y)
    
    ######################################
    #draw character and weapons to screen#
    #-----processes damage to enemies----#
    ######################################
    def update(self, screen, enemies):
        
        if self.climbing:
            if self.climbCounter < 4:
                screen.blit(self.climb1, self.rect)
            else:
                screen.blit(self.climb2, self.rect)
            self.climbCounter = (self.climbCounter + 1) % 8
            
        
        else:
            ########################################
            #update locations of body section rects#
            ########################################
            self.headRect.centerx = self.rect.centerx
            self.bodyRect.centerx = self.rect.centerx
            self.legRect.centerx = self.rect.centerx
            self.headRect.top = self.rect.top
            self.bodyRect.top = self.headRect.bottom
            self.legRect.top = self.bodyRect.bottom
            
            #############################
            #process direction animation#
            #############################
            if self.direction == 'down':
                self.drawDown(screen)
            elif self.direction == 'up':
                self.drawUp(screen)
            elif self.direction == 'right':
                self.drawRight(screen)
            elif self.direction == 'left':
                self.drawLeft(screen)
                
            #####################
            #process projectiles#
            #####################
            if len(self.projectiles) != 0:
                for enemy in enemies:
                    for projectile in self.projectiles:
                        if projectile.rect.colliderect(enemy.rect):
                            status = enemy.takeThrowDamage(self.projectileDamage + self.throwDamageBonus)
                            if status == 'dead':
                                self.gainGoods(enemy.giveUpTheGoods())
                                enemies.remove(enemy)
                            self.projectiles.remove(projectile)
                for projectile in self.projectiles:
                    projectile.update(screen)
                for projectile in self.projectiles:
                    if projectile.rect.right < 0 or projectile.rect.left > AREAWIDTH or projectile.rect.bottom < 0 or projectile.rect.top > AREAHEIGHT:
                        self.projectiles.remove(projectile)
                    
            #########################
            #process magic fireballs#
            #########################
            if len(self.fireballs) != 0:
                for ball in self.fireballs:
                    for enemy in enemies:
                        if ball.rect.colliderect(enemy.rect):
                            status = None
                            if self.fireLevel == 1:
                                status = enemy.takeFireDamage(self.fireDamage + self.fireDamageBonus)
                            elif self.fireLevel == 2:
                                status = enemy.takeFireDamage(self.fireDamage + self.fireDamageBonus, True)
                            if status == 'dead':
                                self.gainGoods(enemy.giveUpTheGoods())
                                enemies.remove(enemy)
                for ball in self.fireballs:
                    ball.update(screen)
                for ball in self.fireballs:
                    if ball.rect.right < 0 or ball.rect.left > AREAWIDTH or ball.rect.bottom < 0 or ball.rect.top > AREAHEIGHT:
                        self.fireballs.remove(ball)
            
            ###############
            #process melee#
            ###############
            if self.meleeing != None:
                self.meleeUpdate(screen, enemies)
                
            #########################
            #process hide effects#
            #########################
            if self.hidden:
                if self.mana < HIDE_MANA_LOSS_RATE:
                    self.hidden = False
                else:
                    self.mana -= HIDE_MANA_LOSS_RATE
                if self.hideLevel == 2:
                    self.health += self.hideHealthRegen + self.hideHealthBonus
                    if self.health > self.maxHealth:
                        self.health = self.maxHealth
            
            
            self.mana += self.manaRegen
            if self.fireLevel == 0 and self.iceLevel == 0 and self.hideLevel == 0:
                self.mana = 0
            if self.mana > self.maxMana:
                self.mana = self.maxMana
            
            #######################
            #reset moving to false#
            #######################
            self.moving = False
            
            ##################
            #mannages stunned#
            ##################
            if self.stunned:
                self.moveRate = self.maxMoveRate / 3
            else:
                self.moveRate = self.maxMoveRate
            self.stunned = False
        
                
    ############################################
    #update method for processing melee attacks#
    ############################################
    def meleeUpdate(self, screen, enemies):
        
        #######################
        #process heavy attacks#
        #######################
        if self.meleeing == 'heavy':
            if self.meleeCounter < HEAVY_ANIM_FRAMES:
                #setup variables
                weaponRect = None
                knockback = (0,0)
        
                ################################
                #process heavy melee animations#
                ################################
                if self.direction == 'up':
                    weaponRect, knockback = self.heavyMeleeUp(screen)
                elif self.direction == 'down':
                    weaponRect, knockback = self.heavyMeleeDown(screen)
                elif self.direction == 'right':
                    weaponRect, knockback = self.heavyMeleeRight(screen)
                elif self.direction == 'left':
                    weaponRect, knockback = self.heavyMeleeLeft(screen)
                    
                ###########################
                #process damage to enemies#
                ###########################
                if self.meleeCounter < HEAVY_ANIM_FRAMES - 3: #no damage while retracting weapon
                    processEnemies = enemies
                    for enemy in processEnemies:
                        if enemy not in self.meleeHits:
                            if weaponRect.colliderect(enemy.rect):
                                status = enemy.takeMeleeDamage(self.weaponHeavyDamage + self.heavyDamageBonus, knockback)
                                if status == 'dead':
                                    self.gainGoods(enemy.giveUpTheGoods())
                                    enemies.remove(enemy)
                                else:
                                    self.meleeHits.append(enemy)
                
                ######################
                #update melee counter#
                ######################
                self.meleeCounter += 1
                if self.meleeCounter == HEAVY_ANIM_FRAMES + HEAVY_COOLDOWN:
                    self.meleeCounter = 0
                    self.meleeing = None
                    self.meleeHits = []

        #######################
        #process light attacks#        
        #######################
        elif self.meleeing == 'light':
            if self.meleeCounter < 5:
                #setup variables
                weaponRect = None
                knockback = (0,0)
                
                ################################
                #process light melee animations#
                ################################
                if self.direction == 'up':
                    weaponRect, knockback = self.lightMeleeUp(screen)
                elif self.direction == 'down':
                    weaponRect, knockback = self.lightMeleeDown(screen)
                elif self.direction == 'right':
                    weaponRect, knockback = self.lightMeleeRight(screen)
                elif self.direction == 'left':
                    weaponRect, knockback = self.lightMeleeLeft(screen)
                
                ###########################
                #process damage to enemies#
                ###########################
                for enemy in enemies:
                    if enemy not in self.meleeHits:
                        if weaponRect.colliderect(enemy.rect):
                            status = enemy.takeMeleeDamage(self.weaponLightDamage + self.lightDamageBonus, knockback)
                            if status == 'dead':
                                self.gainGoods(enemy.giveUpTheGoods())
                                enemies.remove(enemy)
                            else:
                                self.meleeHits.append(enemy)
                
                ######################
                #update melee counter#
                ######################
                self.meleeCounter += 1
                if self.meleeCounter == 5 + LIGHT_COOLDOWN:
                    self.meleeCounter = 0
                    self.meleeing = None
                    self.meleeHits = []
                
    ######################################
    #process exp/mana after killing enemy#
    ######################################
    def gainGoods(self, (expPlus, manaPlus)):
        self.exp += expPlus
        if self.exp >= self.expThreshold:
            self.exp -= self.expThreshold
            self.playerLevel += 1
            self.expThreshold = LEVEL_EXP*self.playerLevel
            self.levelUp = True
        self.mana += manaPlus
        if self.mana > self.maxMana:
            self.mana = self.maxMana
    
    ###########################
    #process light melee input#
    ###########################
    def lightMelee(self):
        if self.meleeing == None and self.weapon != None:
            self.meleeing = 'light'
    
    ###########################
    #process heavy melee input#
    ###########################
    def heavyMelee(self):
        if self.meleeing == None and self.weapon != None:
            self.meleeing = 'heavy'
    
    ###########################
    #process analog move input#
    ###########################
    def analogMove(self, xMag, yMag, screen, magicColor):
        self.moving = True
        xMove = int(abs(round(xMag * self.moveRate, 0)))
        yMove = int(abs(round(yMag * self.moveRate, 0)))
        
        screen.lock()
        if xMag < 0.0:
            self.moveLeft(screen, magicColor, xMove)
        elif xMag > 0.0:
            self.moveRight(screen, magicColor, xMove)
        if yMag < 0.0:
            self.moveUp(screen, magicColor, yMove)
        elif yMag > 0.0:
            self.moveDown(screen, magicColor, yMove)
        screen.unlock()
        
        if abs(xMag) > abs(yMag): #more horizontal movement than vertical
            if xMag < 0.0: #if left
                self.direction = 'left'
            elif xMag > 0.0: #if right
                self.direction = 'right'
        elif abs(xMag) < abs(yMag): #more vertical movement than horizontal
            if yMag < 0.0: #if up
                self.direction = 'up'
            elif yMag > 0.0: #if down
                self.direction = 'down'
                
    ############################
    #process digital move input#
    ############################
    def digitalMove(self, moveLeft, moveRight, moveUp, moveDown, screen, magicColor):
        self.moving = True
        screen.lock()
        
        if moveLeft:
            if moveUp:
                self.moveLeft(screen, magicColor, int(round(math.sqrt((self.moveRate**2)/2), 0)))
                self.moveUp(screen, magicColor, int(round(math.sqrt((self.moveRate**2)/2), 0)))
                self.direction = 'left'
            elif moveDown:
                self.moveLeft(screen, magicColor, int(round(math.sqrt((self.moveRate**2)/2), 0)))
                self.moveDown(screen, magicColor, int(round(math.sqrt((self.moveRate**2)/2), 0)))
                self.direction = 'down'
            else:
                self.moveLeft(screen, magicColor, self.moveRate)
                self.direction = 'left'
        
        elif moveRight:
            if moveUp:
                self.moveRight(screen, magicColor, int(round(math.sqrt((self.moveRate**2)/2), 0)))
                self.moveUp(screen, magicColor, int(round(math.sqrt((self.moveRate**2)/2), 0)))
                self.direction = 'right'
            elif moveDown:
                self.moveRight(screen, magicColor, int(round(math.sqrt((self.moveRate**2)/2), 0)))
                self.moveDown(screen, magicColor, int(round(math.sqrt((self.moveRate**2)/2), 0)))
                self.direction = 'down'
            else:
                self.moveRight(screen, magicColor, self.moveRate)
                self.direction = 'right'
        
        elif moveUp:
            self.moveUp(screen, magicColor, self.moveRate)
            self.direction = 'up'
        
        elif moveDown:
            self.moveDown(screen, magicColor, self.moveRate)
            self.direction = 'down'
            
        screen.unlock()
    
    #############################
    #process digital throw input#
    #############################
    def digitalThrow(self, throwLeft, throwRight, throwUp, throwDown):
        if self.projectile != None:
            if time.time() - self.lastThrowTime > self.throwTimeDelay and len(self.projectiles) < self.maxSimulThrows:
                self.throwing = True
                diagSpeedFrac = int(round(math.sqrt((self.throwSpeed**2)/2), 0))
                thisProjectile = None
                if throwLeft:
                    if throwUp:
                        thisProjectile = throwManager.newProjectile(self.projectile, (-1*diagSpeedFrac,-1*diagSpeedFrac), self.rect, 'topLeft')
                    elif throwDown:
                        thisProjectile = throwManager.newProjectile(self.projectile, (-1*diagSpeedFrac, diagSpeedFrac), self.rect, 'bottomLeft')
                    else:
                        thisProjectile = throwManager.newProjectile(self.projectile, (-1*self.throwSpeed, 0), self.rect, 'midLeft')
                
                elif throwRight:
                    if throwUp:
                        thisProjectile = throwManager.newProjectile(self.projectile, (diagSpeedFrac, -1*diagSpeedFrac), self.rect, 'topRight')
                    elif throwDown:
                        thisProjectile = throwManager.newProjectile(self.projectile, (diagSpeedFrac, diagSpeedFrac), self.rect, 'bottomRight')
                    else:
                        thisProjectile = throwManager.newProjectile(self.projectile, (self.throwSpeed, 0), self.rect, 'midRight')
                        
                elif throwUp:
                    thisProjectile = throwManager.newProjectile(self.projectile, (0, -1*self.throwSpeed), self.rect, 'midTop')
                
                elif throwDown:
                    thisProjectile = throwManager.newProjectile(self.projectile, (0, self.throwSpeed), self.rect, 'midBottom')
                self.projectiles.append(thisProjectile)
                self.lastThrowTime = time.time()
    
    ############################
    #process analog throw input#
    ############################
    def analogThrow(self, xMag, yMag, screen):
        if self.projectile != None:
            if time.time() - self.lastThrowTime > self.throwTimeDelay and len(self.projectiles) < self.maxSimulThrows:
                self.throwing = True
                vector = (int(round(xMag * self.throwSpeed, 0)), int(round(yMag * self.throwSpeed, 0)))
                thisProjectile = throwManager.newProjectile(self.projectile, vector, self.rect, 'center')
                thisProjectile.update(screen)#to get projectile to move one animation frame out of center of player
                self.projectiles.append(thisProjectile)
                self.lastThrowTime = time.time()

    ##########################################################################################################################################
    ##########################################################################################################################################
    ##########################################################################################################################################    
    ##############################################---------------utility methods----------------##############################################
    ##########################################################################################################################################
    ##########################################################################################################################################
    ##########################################################################################################################################    
    
    #draw character facing downward
    def drawDown(self, screen):
        #check if hidden
        if self.hidden:
            #draw transparent head
            screen.blit(self.images['downHeadHidden'], self.headRect.topleft)
        #otherwise, draw out full body
        else:         
            #draw head
            screen.blit(self.images['downHead'], self.headRect.topleft)
            #draw body
            if self.meleeing != None:
                True #pass responsibility to meleeupdate
            elif self.throwing:
                bigBodyRect = pygame.Rect(0, 0, 50, 37)
                bigBodyRect.bottomleft = self.bodyRect.bottomleft
                if self.throwingCounter == 0:
                    screen.blit(self.images['downThrow1'], bigBodyRect)
                elif self.throwingCounter == 1:
                    screen.blit(self.images['downThrow2'], bigBodyRect)
                if self.throwingCounter == 2:
                    screen.blit(self.images['downThrow1'], bigBodyRect)
                self.throwingCounter += 1
                if self.throwingCounter == 3:
                    self.throwingCounter = 0
                    self.throwing = False    
            else:
                screen.blit(self.images['downBody'], self.bodyRect.topleft)
            #draw legs
            if self.moving:
                self.movingCounter = (self.movingCounter + 1) % 8
                if self.movingCounter == 0:
                    screen.blit(self.images['downLegs'], self.legRect.topleft)
                elif self.movingCounter == 1:
                    screen.blit(self.images['downWalk1'], self.legRect.topleft)
                elif self.movingCounter == 2:
                    screen.blit(self.images['downWalk2'], self.legRect.topleft)
                elif self.movingCounter == 3:
                    screen.blit(self.images['downWalk1'], self.legRect.topleft)
                elif self.movingCounter == 4:
                    screen.blit(self.images['downLegs'], self.legRect.topleft)
                elif self.movingCounter == 5:
                    screen.blit(self.images['downWalk3'], self.legRect.topleft)
                elif self.movingCounter == 6:
                    screen.blit(self.images['downWalk4'], self.legRect.topleft)
                elif self.movingCounter == 7:
                    screen.blit(self.images['downWalk3'], self.legRect.topleft)
            else:
                self.movingCounter = 0
                screen.blit(self.images['downLegs'], self.legRect.topleft)
    
    #draw character facing upward
    def drawUp(self, screen):
        #check if hidden
        if self.hidden:
            #draw transparent head
            screen.blit(self.images['upHeadHidden'], self.headRect.topleft)
        #otherwise, draw out full body
        else:
            #draw head
            screen.blit(self.images['upHead'], self.headRect.topleft)
            #draw body
            if self.meleeing != None:
                True #pass responsibility to meleeupdate
            elif self.throwing:
                bigBodyRect = pygame.Rect(0, 0, 50, 37)
                bigBodyRect.bottomleft = self.bodyRect.bottomleft
                if self.throwingCounter == 0:
                    screen.blit(self.images['upThrow1'], bigBodyRect)
                elif self.throwingCounter == 1:
                    screen.blit(self.images['upThrow2'], bigBodyRect)
                if self.throwingCounter == 2:
                    screen.blit(self.images['upThrow1'], bigBodyRect)
                self.throwingCounter += 1
                if self.throwingCounter == 3:
                    self.throwingCounter = 0
                    self.throwing = False
            else:
                screen.blit(self.images['upBody'], self.bodyRect.topleft)
            #draw legs
            if self.moving:
                self.movingCounter = (self.movingCounter + 1) % 8
                if self.movingCounter == 0:
                    screen.blit(self.images['upLegs'], self.legRect.topleft)
                elif self.movingCounter == 1:
                    screen.blit(self.images['upWalk1'], self.legRect.topleft)
                elif self.movingCounter == 2:
                    screen.blit(self.images['upWalk2'], self.legRect.topleft)
                elif self.movingCounter == 3:
                    screen.blit(self.images['upWalk1'], self.legRect.topleft)
                elif self.movingCounter == 4:
                    screen.blit(self.images['upLegs'], self.legRect.topleft)
                elif self.movingCounter == 5:
                    screen.blit(self.images['upWalk3'], self.legRect.topleft)
                elif self.movingCounter == 6:
                    screen.blit(self.images['upWalk4'], self.legRect.topleft)
                elif self.movingCounter == 7:
                    screen.blit(self.images['upWalk3'], self.legRect.topleft)
            else:
                self.movingCounter = 0
                screen.blit(self.images['upLegs'], self.legRect.topleft)
                
    #draw character facing rightward
    def drawRight(self, screen):
        #check if hidden
        if self.hidden:
            #draw transparent head
            screen.blit(self.images['rightHeadHidden'], self.headRect.topleft)
        #otherwise, draw out full body
        else:
            #draw head
            screen.blit(self.images['rightHead'], self.headRect.topleft)
            #draw body
            if self.meleeing != None:
                True #pass responsibility to meleeupdate
            elif self.throwing:
                bigBodyRect = pygame.Rect(0, 0, 50, 37)
                bigBodyRect.bottomleft = self.bodyRect.bottomleft
                if self.throwingCounter == 0:
                    screen.blit(self.images['rightThrow1'], bigBodyRect)
                elif self.throwingCounter == 1:
                    screen.blit(self.images['rightBody'], self.bodyRect)
                if self.throwingCounter == 2:
                    screen.blit(self.images['rightThrow3'], bigBodyRect)
                self.throwingCounter += 1
                if self.throwingCounter == 3:
                    self.throwingCounter = 0
                    self.throwing = False
            else:
                screen.blit(self.images['rightBody'], self.bodyRect.topleft)
            #draw legs
            if self.moving:
                self.movingCounter = (self.movingCounter + 1) % 8
                if self.movingCounter == 0:
                    screen.blit(self.images['rightLegs'], self.legRect.topleft)
                elif self.movingCounter == 1:
                    screen.blit(self.images['rightWalk1'], self.legRect.topleft)
                elif self.movingCounter == 2:
                    screen.blit(self.images['rightWalk2'], self.legRect.topleft)
                elif self.movingCounter == 3:
                    screen.blit(self.images['rightWalk1'], self.legRect.topleft)
                elif self.movingCounter == 4:
                    screen.blit(self.images['rightLegs'], self.legRect.topleft)
                elif self.movingCounter == 5:
                    screen.blit(self.images['rightWalk3'], self.legRect.topleft)
                elif self.movingCounter == 6:
                    screen.blit(self.images['rightWalk4'], self.legRect.topleft)
                elif self.movingCounter == 7:
                    screen.blit(self.images['rightWalk3'], self.legRect.topleft)
            else:
                self.movingCounter = 0
                screen.blit(self.images['rightLegs'], self.legRect.topleft)
                
    #draw character facing leftward
    def drawLeft(self, screen):
        #check if hidden
        if self.hidden:
            #draw transparent head
            screen.blit(self.images['leftHeadHidden'], self.headRect.topleft)
        #otherwise, draw out full body
        else:
            #draw head
            screen.blit(self.images['leftHead'], self.headRect.topleft)
            #draw body
            if self.meleeing != None:
                True #pass responsibility to meleeupdate
            elif self.throwing:
                bigBodyRect = pygame.Rect(0, 0, 50, 37)
                bigBodyRect.bottomleft = self.bodyRect.bottomleft
                if self.throwingCounter == 0:
                    screen.blit(self.images['leftThrow1'], bigBodyRect)
                elif self.throwingCounter == 1:
                    screen.blit(self.images['leftThrow2'], bigBodyRect)
                if self.throwingCounter == 2:
                    screen.blit(self.images['leftThrow3'], bigBodyRect)
                self.throwingCounter += 1
                if self.throwingCounter == 3:
                    self.throwingCounter = 0
                    self.throwing = False
            else:
                screen.blit(self.images['leftBody'], self.bodyRect.topleft)
            #draw legs
            if self.moving:
                self.movingCounter = (self.movingCounter + 1) % 8
                if self.movingCounter == 0:
                    screen.blit(self.images['leftLegs'], self.legRect.topleft)
                elif self.movingCounter == 1:
                    screen.blit(self.images['leftWalk1'], self.legRect.topleft)
                elif self.movingCounter == 2:
                    screen.blit(self.images['leftWalk2'], self.legRect.topleft)
                elif self.movingCounter == 3:
                    screen.blit(self.images['leftWalk1'], self.legRect.topleft)
                elif self.movingCounter == 4:
                    screen.blit(self.images['leftLegs'], self.legRect.topleft)
                elif self.movingCounter == 5:
                    screen.blit(self.images['leftWalk3'], self.legRect.topleft)
                elif self.movingCounter == 6:
                    screen.blit(self.images['leftWalk4'], self.legRect.topleft)
                elif self.movingCounter == 7:
                    screen.blit(self.images['leftWalk3'], self.legRect.topleft)
            else:
                self.movingCounter = 0
                screen.blit(self.images['leftLegs'], self.legRect.topleft)
    
    
    
    
    #move player left, scanning left edge for collisions
    def moveLeft(self, room, magicColor, distance):
        if self.rect.left - distance > 0:
            maxMove = distance
            x = self.rect.left
            y = self.rect.top
            #scan edge
            while maxMove > 0 and y < self.rect.bottom:
                move = 1
                while move <= maxMove:
                    if room.get_at((x-move,y)) == magicColor:
                        if maxMove > move - 1:
                            maxMove = move - 1
                            break
                    else:
                        move += 1
                y += 1
            
            self.rect.move_ip(-1*maxMove, 0)
            if not self.hidden:
                self.lastRect.topleft = self.rect.topleft
        else: self.rect.left = 0
    
    #move player right, scanning right edge for collisions    
    def moveRight(self, room, magicColor, distance):
        if self.rect.right + distance < AREAWIDTH:
            maxMove = distance
            x = self.rect.right
            y = self.rect.top
            #scan edge
            while maxMove > 0 and y < self.rect.bottom:
                move = 1
                while move <= maxMove:
                    if room.get_at((x-1+move,y)) == magicColor:
                        if maxMove > move - 1:
                            maxMove = move - 1
                        break
                    else:
                        move += 1
                y += 1

            self.rect.move_ip(maxMove, 0)
            if not self.hidden:
                self.lastRect.topleft = self.rect.topleft
        else: self.rect.right = AREAWIDTH
        
    #move player up, scanning top edge for collisions
    def moveUp(self, room, magicColor, distance):
        if self.rect.top - distance > 0:
            maxMove = distance
            y = self.rect.top
            x = self.rect.left
            #scan edge
            while maxMove > 0 and x < self.rect.right:
                move = 1
                while move <= maxMove:
                    if room.get_at((x,y-move)) == magicColor:
                        if maxMove > move - 1:
                            maxMove = move - 1
                        break
                    else:
                        move += 1
                x += 1
                
            self.rect.move_ip(0, -1*maxMove)
            if not self.hidden:
                self.lastRect.topleft = self.rect.topleft
        else: self.rect.top = 0
        
    #move player down, scanning bottom edge for collisions
    def moveDown(self, room, magicColor, distance):
        if self.rect.bottom + distance < AREAHEIGHT:
            maxMove = distance
            y = self.rect.bottom
            x = self.rect.left
            #scan edge
            while maxMove > 0 and x < self.rect.right:
                move = 1
                while move <= maxMove:
                    if room.get_at((x,y-1+move)) == magicColor:
                        if maxMove > move - 1:
                            maxMove = move - 1
                        break
                    else:
                        move += 1
                x += 1
                    
            self.rect.move_ip(0, maxMove)
            if not self.hidden:
                self.lastRect.topleft = self.rect.topleft
        else: self.rect.bottom = AREAHEIGHT
    
    #animate upward heavy melee attack
    def heavyMeleeUp(self, screen):
        weapon = self.weapon.pokeUp()
        weaponRect = weapon.get_rect()
        bigBodyRect = pygame.Rect(0, 0, 50, 37)
        bigBodyRect.bottomleft = self.bodyRect.bottomleft
        if self.meleeCounter == 0:
            weaponRect.midbottom = (bigBodyRect.right - 11, bigBodyRect.top + 12)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['upPoke1'], bigBodyRect.topleft)
        elif self.meleeCounter == 1:
            weaponRect.midbottom = (bigBodyRect.right - 11, bigBodyRect.top + 8)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['upPoke2'], bigBodyRect.topleft)
        elif self.meleeCounter == 2:
            weaponRect.midbottom = (bigBodyRect.right - 11, bigBodyRect.top + 4)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['upPoke3'], bigBodyRect.topleft)
        elif self.meleeCounter > 2 and self.meleeCounter < HEAVY_ANIM_FRAMES - 3:
            weaponRect.midbottom = (bigBodyRect.right - 11, bigBodyRect.top)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['upPoke4'], bigBodyRect.topleft)
        elif self.meleeCounter == HEAVY_ANIM_FRAMES - 3:
            weaponRect.midbottom = (bigBodyRect.right - 11, bigBodyRect.top + 4)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['upPoke3'], bigBodyRect.topleft)
        elif self.meleeCounter == HEAVY_ANIM_FRAMES - 2:
            weaponRect.midbottom = (bigBodyRect.right - 11, bigBodyRect.top + 8)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['upPoke2'], bigBodyRect.topleft)
        elif self.meleeCounter == HEAVY_ANIM_FRAMES - 1:
            weaponRect.midbottom = (bigBodyRect.right - 11, bigBodyRect.top + 12)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['upPoke1'], bigBodyRect.topleft)
        screen.blit(self.images['upHead'], self.headRect.topleft)#to cover stick in upward animations
        return weaponRect, (0, -1*(self.weaponHeavyKnockback + self.heavyKnockbackBonus))
    
    #animate downward heavy melee attack    
    def heavyMeleeDown(self, screen):
        weapon = self.weapon.pokeDown()
        weaponRect = weapon.get_rect()
        if self.meleeCounter == 0:
            weaponRect.midtop = (self.bodyRect.left + 10, self.bodyRect.bottom - 14)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['downPoke1'], self.bodyRect.topleft)
        elif self.meleeCounter == 1:
            weaponRect.midtop = (self.bodyRect.left + 10, self.bodyRect.bottom - 10)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['downPoke2'], self.bodyRect.topleft)
        elif self.meleeCounter == 2:
            weaponRect.midtop = (self.bodyRect.left + 10, self.bodyRect.bottom - 6)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['downPoke3'], self.bodyRect.topleft)
        elif self.meleeCounter > 2 and self.meleeCounter < HEAVY_ANIM_FRAMES - 3:
            weaponRect.midtop = (self.bodyRect.left + 10, self.bodyRect.bottom - 2)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['downPoke4'], self.bodyRect.topleft)
        elif self.meleeCounter == HEAVY_ANIM_FRAMES - 3:
            weaponRect.midtop = (self.bodyRect.left + 10, self.bodyRect.bottom - 6)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['downPoke3'], self.bodyRect.topleft)
        elif self.meleeCounter == HEAVY_ANIM_FRAMES - 2:
            weaponRect.midtop = (self.bodyRect.left + 10, self.bodyRect.bottom - 10)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['downPoke2'], self.bodyRect.topleft)
        elif self.meleeCounter == HEAVY_ANIM_FRAMES - 1:
            weaponRect.midtop = (self.bodyRect.left + 10, self.bodyRect.bottom - 14)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['downPoke1'], self.bodyRect.topleft)
        return weaponRect, (0, self.weaponHeavyKnockback + self.heavyKnockbackBonus)
            
    #animate rightward heavy melee attack
    def heavyMeleeRight(self, screen):
        weapon = self.weapon.pokeRight()
        weaponRect = weapon.get_rect()
        if self.meleeCounter == 0:
            weaponRect.midleft = (self.bodyRect.right - 24, self.bodyRect.top + 12)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['rightPoke1'], self.bodyRect.topleft)
        elif self.meleeCounter == 1:
            weaponRect.midleft = (self.bodyRect.right - 20, self.bodyRect.top + 12)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['rightPoke2'], self.bodyRect.topleft)
        elif self.meleeCounter == 2:
            weaponRect.midleft = (self.bodyRect.right - 16, self.bodyRect.top + 12)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['rightPoke3'], self.bodyRect.topleft)
        elif self.meleeCounter > 2 and self.meleeCounter < HEAVY_ANIM_FRAMES - 3:
            weaponRect.midleft = (self.bodyRect.right - 12, self.bodyRect.top + 12)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['rightPoke4'], self.bodyRect.topleft)
        elif self.meleeCounter == HEAVY_ANIM_FRAMES - 3:
            weaponRect.midleft = (self.bodyRect.right - 16, self.bodyRect.top + 12)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['rightPoke3'], self.bodyRect.topleft)
        elif self.meleeCounter == HEAVY_ANIM_FRAMES - 2:
            weaponRect.midleft = (self.bodyRect.right - 20, self.bodyRect.top + 12)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['rightPoke2'], self.bodyRect.topleft)
        elif self.meleeCounter == HEAVY_ANIM_FRAMES - 1:
            weaponRect.midleft = (self.bodyRect.right - 24, self.bodyRect.top + 12)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['rightPoke1'], self.bodyRect.topleft)
        return weaponRect, (self.weaponHeavyKnockback + self.heavyKnockbackBonus, 0)
            
    #animate leftward heavy melee attack
    def heavyMeleeLeft(self, screen):
        weapon = self.weapon.pokeLeft()
        weaponRect = weapon.get_rect()
        if self.meleeCounter == 0:
            weaponRect.midright = (self.bodyRect.left + 23, self.bodyRect.top + 12)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['leftBody'], self.bodyRect.topleft)
        elif self.meleeCounter == 1:
            weaponRect.midright = (self.bodyRect.left + 19, self.bodyRect.top + 12)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['leftBody'], self.bodyRect.topleft)
        elif self.meleeCounter == 2:
            weaponRect.midright = (self.bodyRect.left + 15, self.bodyRect.top + 12)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['leftBody'], self.bodyRect.topleft)
        elif self.meleeCounter > 2 and self.meleeCounter < HEAVY_ANIM_FRAMES - 3:
            weaponRect.midright = (self.bodyRect.left + 11, self.bodyRect.top + 12)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['leftPoke'], self.bodyRect.topleft)
        elif self.meleeCounter == HEAVY_ANIM_FRAMES - 3:
            weaponRect.midright = (self.bodyRect.left + 15, self.bodyRect.top + 12)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['leftBody'], self.bodyRect.topleft)
        elif self.meleeCounter == HEAVY_ANIM_FRAMES - 2:
            weaponRect.midright = (self.bodyRect.left + 19, self.bodyRect.top + 12)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['leftBody'], self.bodyRect.topleft)
        elif self.meleeCounter == HEAVY_ANIM_FRAMES - 1:
            weaponRect.midright = (self.bodyRect.left + 24, self.bodyRect.top + 12)
            screen.blit(weapon, weaponRect)
            screen.blit(self.images['leftBody'], self.bodyRect.topleft)
        return weaponRect, (-1*(self.weaponHeavyKnockback + self.heavyKnockbackBonus), 0)
            
    #animate upward light melee attack
    def lightMeleeUp(self, screen):
        weapon = self.weapon.sweepUp()
        weaponRect = weapon[self.meleeCounter].get_rect()
        bigBodyRect = pygame.Rect(0, 0, 50, 37)
        bigBodyRect.bottomleft = self.bodyRect.bottomleft
        if self.meleeCounter == 0:
            weaponRect.bottomleft = (bigBodyRect.right - 3, bigBodyRect.top + 4)
            screen.blit(weapon[self.meleeCounter], weaponRect)
            screen.blit(self.images['upSwing1'], bigBodyRect.topleft)
        elif self.meleeCounter == 1:
            weaponRect.bottomleft = (bigBodyRect.right - 7, bigBodyRect.top + 2)
            screen.blit(weapon[self.meleeCounter], weaponRect)
            screen.blit(self.images['upSwing2'], bigBodyRect.topleft)  
        elif self.meleeCounter == 2:
            weaponRect.midbottom = (bigBodyRect.right - 11, bigBodyRect.top)
            screen.blit(weapon[self.meleeCounter], weaponRect)
            screen.blit(self.images['upSwing3'], bigBodyRect.topleft)
        elif self.meleeCounter == 3:
            weaponRect.bottomright = (bigBodyRect.right - 15, bigBodyRect.top + 2)
            screen.blit(weapon[self.meleeCounter], weaponRect)
            screen.blit(self.images['upSwing4'], bigBodyRect.topleft)
        elif self.meleeCounter == 4:
            weaponRect.bottomright = (bigBodyRect.right - 19, bigBodyRect.top + 4)
            screen.blit(weapon[self.meleeCounter], weaponRect)
            screen.blit(self.images['upSwing5'], bigBodyRect.topleft)
        screen.blit(self.images['upHead'], self.headRect.topleft)#to cover stock in upward animations
        return weaponRect, (0, -1*(self.weaponLightKnockback + self.lightKnockbackBonus))

    #animate downward light melee attack
    def lightMeleeDown(self, screen):
        weapon = self.weapon.sweepDown()
        weaponRect = weapon[self.meleeCounter].get_rect()
        if self.meleeCounter == 0:
            weaponRect.topright = (self.bodyRect.left + 2, self.bodyRect.bottom - 6)
            screen.blit(weapon[self.meleeCounter], weaponRect)
            screen.blit(self.images['downSwing1'], self.bodyRect.topleft)
        elif self.meleeCounter == 1:
            weaponRect.topright = (self.bodyRect.left + 6, self.bodyRect.bottom - 4)
            screen.blit(weapon[self.meleeCounter], weaponRect)
            screen.blit(self.images['downSwing2'], self.bodyRect.topleft)
        elif self.meleeCounter == 2:
            weaponRect.midtop = (self.bodyRect.left + 10, self.bodyRect.bottom - 2)
            screen.blit(weapon[self.meleeCounter], weaponRect)
            screen.blit(self.images['downSwing3'], self.bodyRect.topleft)
        elif self.meleeCounter == 3:
            weaponRect.topleft = (self.bodyRect.left + 14, self.bodyRect.bottom - 4)
            screen.blit(weapon[self.meleeCounter], weaponRect)
            screen.blit(self.images['downSwing4'], self.bodyRect.topleft)
        elif self.meleeCounter == 4:
            weaponRect.topleft = (self.bodyRect.left + 18, self.bodyRect.bottom - 6)
            screen.blit(weapon[self.meleeCounter], weaponRect)
            screen.blit(self.images['downSwing5'], self.bodyRect.topleft)
        return weaponRect, (0, self.weaponLightKnockback + self.lightKnockbackBonus)
    
    #animate rightward light melee attack
    def lightMeleeRight(self, screen):
        weapon = self.weapon.sweepRight()
        weaponRect = weapon[self.meleeCounter].get_rect()
        if self.meleeCounter == 0:
            weaponRect.bottomleft = (self.bodyRect.right - 14, self.bodyRect.top + 6)
            screen.blit(weapon[self.meleeCounter], weaponRect)
            screen.blit(self.images['rightSwing1'], self.bodyRect.topleft)
        elif self.meleeCounter == 1:
            weaponRect.bottomleft = (self.bodyRect.right - 13, self.bodyRect.top + 9)
            screen.blit(weapon[self.meleeCounter], weaponRect)
            screen.blit(self.images['rightSwing2'], self.bodyRect.topleft)
        elif self.meleeCounter == 2:
            weaponRect.midleft = (self.bodyRect.right - 12, self.bodyRect.top + 12)
            screen.blit(weapon[self.meleeCounter], weaponRect)
            screen.blit(self.images['rightSwing3'], self.bodyRect.topleft)
        elif self.meleeCounter == 3:
            weaponRect.topleft = (self.bodyRect.right - 13, self.bodyRect.top + 15)
            screen.blit(weapon[self.meleeCounter], weaponRect)
            screen.blit(self.images['rightSwing4'], self.bodyRect.topleft)
        elif self.meleeCounter == 4:
            weaponRect.topleft = (self.bodyRect.right - 14, self.bodyRect.top + 18)
            screen.blit(weapon[self.meleeCounter], weaponRect)
            screen.blit(self.images['rightSwing5'], self.bodyRect.topleft)
        return weaponRect, (self.weaponLightKnockback + self.lightKnockbackBonus, 0)
    
    #animate leftward light melee attack
    def lightMeleeLeft(self, screen):
        weapon = self.weapon.sweepLeft()
        weaponRect = weapon[self.meleeCounter].get_rect()
        if self.meleeCounter == 0:
            weaponRect.bottomright = (self.bodyRect.left + 13, self.bodyRect.top + 6)
            screen.blit(weapon[self.meleeCounter], weaponRect)
            screen.blit(self.images['leftSwing1'], self.bodyRect.topleft)
        elif self.meleeCounter == 1:
            weaponRect.bottomright = (self.bodyRect.left + 12, self.bodyRect.top + 9)
            screen.blit(weapon[self.meleeCounter], weaponRect)
            screen.blit(self.images['leftSwing2'], self.bodyRect.topleft)
        elif self.meleeCounter == 2:
            weaponRect.midright = (self.bodyRect.left + 11, self.bodyRect.top + 12)
            screen.blit(weapon[self.meleeCounter], weaponRect)
            screen.blit(self.images['leftSwing3'], self.bodyRect.topleft)
        elif self.meleeCounter == 3:
            weaponRect.topright = (self.bodyRect.left + 12, self.bodyRect.top + 15)
            screen.blit(weapon[self.meleeCounter], weaponRect)
            screen.blit(self.images['leftSwing4'], self.bodyRect.topleft)
        elif self.meleeCounter == 4:
            weaponRect.topright = (self.bodyRect.left + 13, self.bodyRect.top + 18)
            screen.blit(weapon[self.meleeCounter], weaponRect)
            screen.blit(self.images['leftSwing5'], self.bodyRect.topleft)
        return weaponRect, (-1*(self.weaponLightKnockback + self.lightKnockbackBonus), 0)

    #create ring of fireballs
    def flameOn(self):
        #cardinal directions
        self.fireballs.append(projectile.MagicFireBall((8,0), self.rect, self.fireDamage, self.assets))
        self.fireballs.append(projectile.MagicFireBall((-8,0), self.rect, self.fireDamage, self.assets))
        self.fireballs.append(projectile.MagicFireBall((0,8), self.rect, self.fireDamage, self.assets))
        self.fireballs.append(projectile.MagicFireBall((0,-8), self.rect, self.fireDamage, self.assets))
        #diagonals
        self.fireballs.append(projectile.MagicFireBall((6,6), self.rect, self.fireDamage, self.assets))
        self.fireballs.append(projectile.MagicFireBall((6,-6), self.rect, self.fireDamage, self.assets))
        self.fireballs.append(projectile.MagicFireBall((-6,6), self.rect, self.fireDamage, self.assets))
        self.fireballs.append(projectile.MagicFireBall((-6,-6), self.rect, self.fireDamage, self.assets))
        #mid-diagonals
        self.fireballs.append(projectile.MagicFireBall((3,7), self.rect, self.fireDamage, self.assets))
        self.fireballs.append(projectile.MagicFireBall((7,3), self.rect, self.fireDamage, self.assets))
        self.fireballs.append(projectile.MagicFireBall((3,-7), self.rect, self.fireDamage, self.assets))
        self.fireballs.append(projectile.MagicFireBall((7,-3), self.rect, self.fireDamage, self.assets))
        self.fireballs.append(projectile.MagicFireBall((-3,7), self.rect, self.fireDamage, self.assets))
        self.fireballs.append(projectile.MagicFireBall((-7,3), self.rect, self.fireDamage, self.assets))
        self.fireballs.append(projectile.MagicFireBall((-3,-7), self.rect, self.fireDamage, self.assets))
        self.fireballs.append(projectile.MagicFireBall((-7,-3), self.rect, self.fireDamage, self.assets))

    #load image files into dictionary
    def loadImages(self):
        self.images = self.assets.imageList("player")
        
    ######################################
    #draw character and weapons to screen#
    #-----processes damage to enemies----#
    ######################################
    def updateIntro(self, screen, enemies):
        
        if self.climbing:
            if self.climbCounter < 4:
                screen.blit(self.climb1, self.rect)
            else:
                screen.blit(self.climb2, self.rect)
            self.climbCounter = (self.climbCounter + 1) % 8
            
        
        else:
            ########################################
            #update locations of body section rects#
            ########################################
            self.headRect.centerx = self.rect.centerx
            self.bodyRect.centerx = self.rect.centerx
            self.legRect.centerx = self.rect.centerx
            self.headRect.top = self.rect.top
            self.bodyRect.top = self.headRect.bottom
            self.legRect.top = self.bodyRect.bottom
            
            #############################
            #process direction animation#
            #############################
            if self.direction == 'down':
                self.drawDown(screen)
            elif self.direction == 'up':
                self.drawUp(screen)
            elif self.direction == 'right':
                self.drawRight(screen)
            elif self.direction == 'left':
                self.drawLeft(screen)
                
            #####################
            #process projectiles#
            #####################
            if len(self.projectiles) != 0:
                for enemy in enemies:
                    for projectile in self.projectiles:
                        if projectile.rect.colliderect(enemy.rect):
                            return True
                for projectile in self.projectiles:
                    projectile.update(screen)
                for projectile in self.projectiles:
                    if projectile.rect.right < 0 or projectile.rect.left > AREAWIDTH or projectile.rect.bottom < 0 or projectile.rect.top > AREAHEIGHT:
                        self.projectiles.remove(projectile)
            return False

