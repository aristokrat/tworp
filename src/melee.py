import pygame, utils
from pygame.locals import *

HAMMERDAMAGE = 18
HAMMERDAMAGEPLUS = 4
HAMMERKNOCKBACK = 16
HAMMERKNOCKBACKPLUS = 6

SWORDDAMAGE = 22
SWORDDAMAGEPLUS = 8
SWORDKNOCKBACK = 10
SWORDKNOCKBACKPLUS = 3

BLADEDAMAGE = 24
BLADEDAMAGEPLUS = 12
BLADEKNOCKBACK = 5
BLADEKNOCKBACKPLUS = 1

class Stick():

    def __init__(self):
        self.name = 'stick'
        self.heavyDamage = 25
        self.lightDamage = 10
        self.heavyKnockback = 50
        self.lightKnockback = 10
        self.images = {}
        self.loadImages()
        
    def loadImages(self):
        self.mapImage = utils.load_trans_image(self.name + 'NW.png')
        
        self.images['N'] = utils.load_trans_image(self.name + 'N.png')
        self.images['NNE'] = utils.load_trans_image(self.name + 'NNE.png')
        self.images['NE'] = utils.load_trans_image(self.name + 'NE.png')
        self.images['ENE'] = utils.load_trans_image(self.name + 'ENE.png')
        
        self.images['E'] = utils.load_trans_image(self.name + 'E.png')
        self.images['ESE'] = utils.load_trans_image(self.name + 'ESE.png')
        self.images['SE'] = utils.load_trans_image(self.name + 'SE.png')
        self.images['SSE'] = utils.load_trans_image(self.name + 'SSE.png')
        
        self.images['S'] = utils.load_trans_image(self.name + 'S.png')
        self.images['SSW'] = utils.load_trans_image(self.name + 'SSW.png')
        self.images['SW'] = utils.load_trans_image(self.name + 'SW.png')
        self.images['WSW'] = utils.load_trans_image(self.name + 'WSW.png')
        
        self.images['W'] = utils.load_trans_image(self.name + 'W.png')
        self.images['WNW'] = utils.load_trans_image(self.name + 'WNW.png')
        self.images['NW'] = utils.load_trans_image(self.name + 'NW.png')
        self.images['NNW'] = utils.load_trans_image(self.name + 'NNW.png')
        
    def pokeUp(self):
        return self.images['N']
    
    def pokeDown(self):
        return self.images['S']
    
    def pokeRight(self):
        return self.images['E']
    
    def pokeLeft(self):
        return self.images['W']
    
    def sweepUp(self):
        return [self.images['NE'], self.images['NNE'], self.images['N'], self.images['NNW'], self.images['NW']]
    
    def sweepRight(self):
        return [self.images['NE'], self.images['ENE'], self.images['E'], self.images['ESE'], self.images['SE']]
    
    def sweepDown(self):
        return [self.images['SW'], self.images['SSW'], self.images['S'], self.images['SSE'], self.images['SE']]
    
    def sweepLeft(self):
        return [self.images['NW'], self.images['WNW'], self.images['W'], self.images['WSW'], self.images['SW']]
    
class Hammer1(Stick):
    
    def __init__(self):
        self.name = 'hammer1'
        self.lightDamage = HAMMERDAMAGE
        self.heavyDamage = int(self.lightDamage * 2.5)
        self.lightKnockback = HAMMERKNOCKBACK
        self.heavyKnockback = self.lightKnockback * 5
        self.images = {}
        self.loadImages()

class Hammer2(Stick):
    
    def __init__(self):
        self.name = 'hammer2'
        self.lightDamage = HAMMERDAMAGE + HAMMERDAMAGEPLUS
        self.heavyDamage = int(self.lightDamage * 2.5)
        self.lightKnockback = HAMMERKNOCKBACK + HAMMERKNOCKBACKPLUS
        self.heavyKnockback = self.lightKnockback * 5
        self.images = {}
        self.loadImages()

class Hammer3(Stick):
    
    def __init__(self):
        self.name = 'hammer3'
        self.lightDamage = HAMMERDAMAGE + HAMMERDAMAGEPLUS * 2
        self.heavyDamage = int(self.lightDamage * 2.5)
        self.lightKnockback = HAMMERKNOCKBACK + HAMMERKNOCKBACKPLUS * 2
        self.heavyKnockback = self.lightKnockback * 5
        self.images = {}
        self.loadImages()

class Hammer4(Stick):
    
    def __init__(self):
        self.name = 'hammer4'
        self.lightDamage = HAMMERDAMAGE + HAMMERDAMAGEPLUS * 3
        self.heavyDamage = int(self.lightDamage * 2.5)
        self.lightKnockback = HAMMERKNOCKBACK + HAMMERKNOCKBACKPLUS * 3
        self.heavyKnockback = self.lightKnockback * 5
        self.images = {}
        self.loadImages()

class Hammer5(Stick):
    
    def __init__(self):
        self.name = 'hammer5'
        self.lightDamage = HAMMERDAMAGE + HAMMERDAMAGEPLUS * 4
        self.heavyDamage = int(self.lightDamage * 2.5)
        self.lightKnockback = HAMMERKNOCKBACK + HAMMERKNOCKBACKPLUS * 4
        self.heavyKnockback = self.lightKnockback * 5
        self.images = {}
        self.loadImages()

class Hammer6(Stick):
    
    def __init__(self):
        self.name = 'hammer6'
        self.lightDamage = HAMMERDAMAGE + HAMMERDAMAGEPLUS * 5
        self.heavyDamage = int(self.lightDamage * 2.5)
        self.lightKnockback = HAMMERKNOCKBACK + HAMMERKNOCKBACKPLUS * 5
        self.heavyKnockback = self.lightKnockback * 5
        self.images = {}
        self.loadImages()

class Sword1(Stick):
    
    def __init__(self):
        self.name = 'sword1'
        self.lightDamage = SWORDDAMAGE
        self.heavyDamage = int(self.lightDamage * 2.5)
        self.lightKnockback = SWORDKNOCKBACK
        self.heavyKnockback = self.lightKnockback * 5
        self.images = {}
        self.loadImages()
        
class Sword2(Stick):
    
    def __init__(self):
        self.name = 'sword2'
        self.lightDamage = SWORDDAMAGE + SWORDDAMAGEPLUS
        self.heavyDamage = int(self.lightDamage * 2.5)
        self.lightKnockback = SWORDKNOCKBACK + SWORDKNOCKBACKPLUS
        self.heavyKnockback = self.lightKnockback * 5
        self.images = {}
        self.loadImages()
        
class Sword3(Stick):
    
    def __init__(self):
        self.lightDamage = SWORDDAMAGE + SWORDDAMAGEPLUS * 2
        self.heavyDamage = int(self.lightDamage * 2.5)
        self.lightKnockback = SWORDKNOCKBACK + SWORDKNOCKBACKPLUS * 2
        self.heavyKnockback = self.lightKnockback * 5
        self.images = {}
        self.loadImages()
        
class Sword4(Stick):
    
    def __init__(self):
        self.name = 'sword4'
        self.lightDamage = SWORDDAMAGE + SWORDDAMAGEPLUS * 3
        self.heavyDamage = int(self.lightDamage * 2.5)
        self.lightKnockback = SWORDKNOCKBACK + SWORDKNOCKBACKPLUS * 3
        self.heavyKnockback = self.lightKnockback * 5
        self.images = {}
        self.loadImages()
        
class Sword5(Stick):
    
    def __init__(self):
        self.name = 'sword5'
        self.lightDamage = SWORDDAMAGE + SWORDDAMAGEPLUS * 4
        self.heavyDamage = int(self.lightDamage * 2.5)
        self.lightKnockback = SWORDKNOCKBACK + SWORDKNOCKBACKPLUS * 4
        self.heavyKnockback = self.lightKnockback * 5
        self.images = {}
        self.loadImages()

class Sword6(Stick):
    
    def __init__(self):
        self.name = 'sword6'
        self.lightDamage = SWORDDAMAGE + SWORDDAMAGEPLUS * 5
        self.heavyDamage = int(self.lightDamage * 2.5)
        self.lightKnockback = SWORDKNOCKBACK + SWORDKNOCKBACKPLUS * 5
        self.heavyKnockback = self.lightKnockback * 5
        self.images = {}
        self.loadImages()


class Blade1(Stick):
    
    def __init__(self):
        self.name = 'blade1'
        self.lightDamage = BLADEDAMAGE
        self.heavyDamage = int(self.lightDamage * 2.5)
        self.lightKnockback = BLADEKNOCKBACK
        self.heavyKnockback = self.lightKnockback * 5
        self.images = {}
        self.loadImages()
        self.loadAsymImages()
        
    def loadAsymImages(self):
        self.images['NEr'] = utils.load_trans_image(self.name + 'NEr.png')
        self.images['SEr'] = utils.load_trans_image(self.name + 'SEr.png')
        
    def sweepRight(self):
        return [self.images['NEr'], self.images['ENE'], self.images['E'], self.images['ESE'], self.images['SEr']]
    
class Blade2(Blade1):
    
    def __init__(self):
        self.name = 'blade2'
        self.lightDamage = BLADEDAMAGE + BLADEDAMAGEPLUS
        self.heavyDamage = int(self.lightDamage * 2.5)
        self.lightKnockback = BLADEKNOCKBACK + BLADEKNOCKBACKPLUS
        self.heavyKnockback = self.lightKnockback * 5
        self.images = {}
        self.loadImages()
        self.loadAsymImages()
        
class Blade3(Blade1):
    
    def __init__(self):
        self.name = 'blade3'
        self.lightDamage = BLADEDAMAGE + BLADEDAMAGEPLUS * 2
        self.heavyDamage = int(self.lightDamage * 2.5)
        self.lightKnockback = BLADEKNOCKBACK + BLADEKNOCKBACKPLUS * 2
        self.heavyKnockback = self.lightKnockback * 5
        self.images = {}
        self.loadImages()
        self.loadAsymImages()

class Blade4(Blade1):
    
    def __init__(self):
        self.name = 'blade4'
        self.lightDamage = BLADEDAMAGE + BLADEDAMAGEPLUS * 3
        self.heavyDamage = int(self.lightDamage * 2.5)
        self.lightKnockback = BLADEKNOCKBACK + BLADEKNOCKBACKPLUS * 3
        self.heavyKnockback = self.lightKnockback * 5
        self.images = {}
        self.loadImages()
        self.loadAsymImages()
        
class Blade5(Blade1):
    
    def __init__(self):
        self.name = 'blade5'
        self.lightDamage = BLADEDAMAGE + BLADEDAMAGEPLUS * 4
        self.heavyDamage = int(self.lightDamage * 2.5)
        self.lightKnockback = BLADEKNOCKBACK + BLADEKNOCKBACKPLUS * 4
        self.heavyKnockback = self.lightKnockback * 5
        self.images = {}
        self.loadImages()
        self.loadAsymImages()
        
class Blade6(Blade1):
    
    def __init__(self):
        self.name = 'blade6'
        self.lightDamage = BLADEDAMAGE + BLADEDAMAGEPLUS * 5
        self.heavyDamage = int(self.lightDamage * 2.5)
        self.lightKnockback = BLADEKNOCKBACK + BLADEKNOCKBACKPLUS * 5
        self.heavyKnockback = self.lightKnockback * 5
        self.images = {}
        self.loadImages()
        self.loadAsymImages()

        