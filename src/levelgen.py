import random

class Node():
    def __init__(self, x, y, previous=None):
        self.mainPathPrevious = previous
        self.mainPathNext = None
        self.doors = []
        self.x = x
        self.y = y
        self.roomName = 'normal'
        self.right = self.left = self.up = self.down = None
        self.visited = False
        self.item = None
        self.environs = []
        
    def updateDoors(self):
        self.doors = []
        if self.right is not None:
            self.doors.append('r')
        if self.left is not None:
            self.doors.append('l')
        if self.up is not None:
            self.doors.append('u')
        if self.down is not None:
            self.doors.append('d')


class Map():
    
    def __init__(self, level):
        
        self.side = 6 + 3 * level
        
        #create blank grid
        self.grid = []
    
        self.startx = self.side/2
        self.starty = self.side/2
        self.lastx = None
        self.lasty = None
        self.firstNode = None
        self.lastNode = None
        self.penultimateNode = None
        self.branchEndings = []
        self.madePath = False
        
        while not self.madePath:
            #create blank grid
            self.grid = []
            for i in range(self.side):
                column = []
                for j in range(self.side):
                    column.append('X')
                self.grid.append(column)
            self.initialPath()
        self.addNames()
        self.addBranches()
        self.addSecretRoom()
        self.printMap()#for debugging
        
        
        
        
    def initialPath(self):
        x = self.startx
        y = self.starty
        
        
        self.firstNode = previousNode = self.grid[x][y] = Node(x, y)
        
        for i in range(self.side-3):
            dirOut = None
            choices = 'udlr'
            while True:
                if choices == '':
                    return
                newX = x
                newY = y
                dirOut = random.choice(choices)
                if dirOut == 'u':#up
                    newY -= 1
                elif dirOut == 'd':#down
                    newY += 1
                elif dirOut == 'l':#left
                    newX -= 1
                elif dirOut == 'r':#right
                    newX += 1
                if newX >= 0 and newX < self.side and newY >=0 and newY < self.side and self.grid[newX][newY] == 'X':
                    x = newX
                    y = newY
                    break
                else:
                    choices = choices.replace(dirOut, '')
            thisNode = self.grid[x][y] = Node(x, y, previousNode)
            previousNode.mainPathNext = thisNode
            thisNode.mainPathPrevious = previousNode
            if dirOut == 'u':#up
                previousNode.up = thisNode
                thisNode.down = previousNode
            elif dirOut == 'd':#down
                previousNode.down = thisNode
                thisNode.up = previousNode
            elif dirOut == 'l':#left
                previousNode.left = thisNode
                thisNode.right = previousNode
            elif dirOut == 'r':#right
                previousNode.right = thisNode
                thisNode.left = previousNode
            if i == self.side-4:
                self.lastx = x
                self.lasty = y
                self.lastNode = thisNode
                self.penultimateNode = self.lastNode.mainPathPrevious
            previousNode.updateDoors()
            thisNode.updateDoors()
            previousNode = thisNode
        self.madePath = True
            
    
    def addNames(self):
        self.firstNode.roomName = 'first'
        self.lastNode.roomName = 'last'
        self.penultimateNode.roomName = 'penultimate'
        currentNode = self.firstNode.mainPathNext
        while currentNode.roomName != 'penultimate':
            currentNode.roomName = 'mainPath'
            currentNode = currentNode.mainPathNext
        
    def addBranches(self):
        '''if (self.side-3)/3 > 3:
            randTop = 3
        else:
            randTop = 1'''
        randTop = 1
        mainPathNode = self.firstNode
        randCounterMax = 0
        randCounter = 0
        while mainPathNode.roomName != "last":
            if randCounter == randCounterMax:
                self.makeNewBranch(mainPathNode)
                randCounterMax = random.randint(0,randTop)
                randCounter = 0
            else:
                randCounter += 1
            mainPathNode = mainPathNode.mainPathNext
    
    def makeNewBranch(self, node):
        x = node.x
        y = node.y
        choices = 'udlr'
        
        previousNode = node
        
        #for i in range((self.side-3)/2):
        
        
        for i in range(random.randint(2,3)):
            branchLength = 0
            choices = 'udlr'
            dirOut = None
            while True:
                if choices == '':
                    if previousNode.roomName == 'normal':
                        self.branchEndings.append(previousNode)
                        previousNode.roomName = 'branchEnd'
                    return
                newX = x
                newY = y
                dirOut = random.choice(choices)
                if dirOut == 'u':#up
                    newY -= 1
                elif dirOut == 'd':#down
                    newY += 1
                elif dirOut == 'l':#left
                    newX -= 1
                elif dirOut == 'r':#right
                    newX += 1
                if newX >= 0 and newX < self.side and newY >=0 and newY < self.side and self.grid[newX][newY] == 'X':
                    x = newX
                    y = newY
                    break
                else:
                    choices = choices.replace(dirOut, '')
            thisNode = self.grid[x][y] = Node(x, y)
            branchLength += 1
            if dirOut == 'u':#up
                previousNode.up = thisNode
                thisNode.down = previousNode
            elif dirOut == 'd':#down
                previousNode.down = thisNode
                thisNode.up = previousNode
            elif dirOut == 'l':#left
                previousNode.left = thisNode
                thisNode.right = previousNode
            elif dirOut == 'r':#right
                previousNode.right = thisNode
                thisNode.left = previousNode
            previousNode.updateDoors()
            thisNode.updateDoors()
            previousNode = thisNode
        if previousNode.roomName != 'mainPath':
            self.branchEndings.append(previousNode)
            previousNode.roomName = 'branchEnd'
            
            
    def addSecretRoom(self):
        secretRoom = False
        mainPathChoices = []
        for i in range(self.side-4):
            mainPathChoices.append(i+1)
        while not secretRoom:
            if len(mainPathChoices) == 0:
                break
            seed = random.choice(mainPathChoices)
            currentNode = self.firstNode
            for i in range(seed):
                currentNode = currentNode.mainPathNext
            choices = 'udlr'
            x = currentNode.x
            y = currentNode.y
            while True:
                if choices == '':
                    break
                newX = x
                newY = y
                dirOut = random.choice(choices)
                if dirOut == 'u':#up
                    newY -= 1
                elif dirOut == 'd':#down
                    newY += 1
                elif dirOut == 'l':#left
                    newX -= 1
                elif dirOut == 'r':#right
                    newX += 1
                if newX >= 0 and newX < self.side and newY >=0 and newY < self.side and self.grid[newX][newY] == 'X':
                    x = newX
                    y = newY
                    secretRoom = True
                    break
                else:
                    choices = choices.replace(dirOut, '')
            if secretRoom:
                secretNode = self.grid[x][y] = Node(x, y)
                if dirOut == 'u':#up
                    currentNode.up = secretNode
                    secretNode.down = currentNode
                elif dirOut == 'd':#down
                    currentNode.down = secretNode
                    secretNode.up = currentNode
                elif dirOut == 'l':#left
                    currentNode.left = secretNode
                    secretNode.right = currentNode
                elif dirOut == 'r':#right
                    currentNode.right = secretNode
                    secretNode.left = currentNode 
                currentNode.updateDoors()
                secretNode.updateDoors()
                secretNode.roomName = 'secret'
                return
            else:
                mainPathChoices.remove(seed)
                
        while not secretRoom:
            branchEndings = self.branchEndings
            while len(branchEndings) > 0:
                print 'trying a branchend'
                currentNode = random.choice(branchEndings)
                choices = 'udlr'
                x = currentNode.x
                y = currentNode.y
                while True:
                    if choices == '':
                        branchEndings.remove(currentNode)
                        break
                    newX = x
                    newY = y
                    dirOut = random.choice(choices)
                    if dirOut == 'u':#up
                        newY -= 1
                    elif dirOut == 'd':#down
                        newY += 1
                    elif dirOut == 'l':#left
                        newX -= 1
                    elif dirOut == 'r':#right
                        newX += 1
                    if newX >= 0 and newX < self.side and newY >=0 and newY < self.side and self.grid[newX][newY] == 'X':
                        x = newX
                        y = newY
                        secretRoom = True
                        break
                    else:
                        choices = choices.replace(dirOut, '')
                if secretRoom:
                    secretNode = self.grid[x][y] = Node(x, y)
                    if dirOut == 'u':#up
                        currentNode.up = secretNode
                        secretNode.down = currentNode
                    elif dirOut == 'd':#down
                        currentNode.down = secretNode
                        secretNode.up = currentNode
                    elif dirOut == 'l':#left
                        currentNode.left = secretNode
                        secretNode.right = currentNode
                    elif dirOut == 'r':#right
                        currentNode.right = secretNode
                        secretNode.left = currentNode 
                    currentNode.updateDoors()
                    secretNode.updateDoors()
                    secretNode.roomName = 'secret'
                    return

        
    
    
    
    def printMap(self):
        nodeCounter = 0
        print " ",
        for j in range(self.side):
            if j < 10:
                print j,
            else:
                print j-10,
        print ""
        for j in range(self.side):
            if j < 10:
                print j,
            else:
                print j-10,
            for i in range(self.side):
                if self.grid[i][j].__class__.__name__ == Node.__name__:
                    if self.grid[i][j].roomName == 'first':
                        print '@',
                        nodeCounter += 1
                    elif self.grid[i][j].roomName == 'mainPath': 
                        print '0',
                        nodeCounter += 1
                    elif self.grid[i][j].roomName == 'last': 
                        print '?',
                        nodeCounter += 1
                    elif self.grid[i][j].roomName == 'penultimate':
                        print '!',
                        nodeCounter += 1
                    elif self.grid[i][j].roomName == 'secret':
                        print 'S',
                        nodeCounter += 1
                    elif self.grid[i][j].roomName == 'branchEnd':
                        print 'E',
                        nodeCounter += 1
                    else:
                        print '.',
                        nodeCounter += 1
                else:
                    print ' ',#thisMap.grid[i][j],
            print ""
        print "Total Rooms = ", nodeCounter
        print "BranchEnds = ", len(self.branchEndings)
                
             
            
            
def main():
    thisMap = Map(1)#6 appears to be the reasonable maximum value here, making usable range 0-6
    print ""
    thisMap = Map(2)
    print ""
    thisMap = Map(3)
    print""
    thisMap = Map(4)


if __name__ == '__main__':main()