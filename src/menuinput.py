import pygame
from pygame.locals import *


def manage(event, controller):
            
    if event.type == JOYBUTTONDOWN:
        
        #PS3 DualShock3 controls
        if controller.get_name() == 'PLAYSTATION(R)3 Controller':
                 
            #select
            if event.button == 14: #x button
                return 'select'
            #movement (rarer on dpad)
            elif event.button == 5: #dpad right
                return 'moveRight'
            elif event.button == 7: #dpad left
                return 'moveLeft'
            elif event.button == 4: #dpad up
                return 'moveUp'
            elif event.button == 6: #dpad down
                return 'moveDown'
            
        #Xbox 360 controller controls 
        elif controller.get_name() == 'Controller':
             
            #select    
            if event.button == 11: #A button
                return 'select'
            #movement (rarer from dpad)
            elif event.button == 3: #dpad right
                return 'moveRight'
            elif event.button == 2: #dpad left
                return 'moveLeft'
            elif event.button == 0: #dpad up
                return 'moveUp'
            elif event.button == 1: #dpad down
                return 'moveDown' 
            
    elif event.type == JOYBUTTONUP:
        
        #PS3 DualShock3 controls
        if controller.get_name() == 'PLAYSTATION(R)3 Controller':
            
            if event.button == 16: #PS button
                return 'terminate'
        
        #Xbox 360 controller controls
        elif controller.get_name() == 'Controller':
            
            if event.button == 10: #Guide button
                return 'terminate'
        
    elif event.type == KEYDOWN:
        
        #keyboard movement
        if event.key == ord('d'):
            return 'moveRight'
        elif event.key == ord('a'):
            return 'moveLeft'
        elif event.key == ord('w'):
            return 'moveUp'
        elif event.key == ord('s'):
            return 'moveDown'
        
        #select
        elif event.key == K_SPACE or event.key == K_KP0 or event.key == K_RETURN or event.key == K_KP_ENTER:
            return 'select'
    
    elif event.type == KEYUP:
        
        if event.key == K_ESCAPE:
            return 'terminate'
            
    elif event.type == QUIT:
        return 'terminate'