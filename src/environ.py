import pygame, utils, random
from pygame.locals import *

class Rock():
    
    def __init__(self, assets):
        self.images = assets.imageList("rock")
        self.image = self.pickImage()
        self.rect = self.image.get_rect()
        self.type = 'environ'
        
    def update(self, screen):
        screen.blit(self.image, self.rect)
        
    def pickImage(self):
        return self.images[random.randint(1,self.images["count"])]