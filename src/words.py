import pygame, utils
from pygame.locals import *

wait = 500

def drawText(screen, level, set):
    center = (500, 550)
    if level == 1:
        if set == 1:
            image = utils.load_trans_image('lv1words1.png')
            rect = image.get_rect()
            rect.center = center
            screen.blit(image, rect)
            pygame.display.update()
            pygame.time.delay(wait)
            utils.waitForPlayerToPressKey()
            return
        elif set == 2:
            image = utils.load_trans_image('lv1words2.png')
            rect = image.get_rect()
            rect.center = center
            screen.blit(image, rect)
            pygame.display.update()
            pygame.time.delay(wait)
            utils.waitForPlayerToPressKey()
            return
        elif set == 3:
            image = utils.load_trans_image('lv1words3.png')
            rect = image.get_rect()
            rect.center = center
            screen.blit(image, rect)
            pygame.display.update()
            pygame.time.delay(wait)
            utils.waitForPlayerToPressKey()
            return
    elif level == 2:
        if set == 1:
            image = utils.load_trans_image('lv2words1.png')
            rect = image.get_rect()
            rect.center = center
            screen.blit(image, rect)
            pygame.display.update()
            pygame.time.delay(wait)
            utils.waitForPlayerToPressKey()
            return
    elif level == 3:
        if set == 1:
            image = utils.load_trans_image('lv3words1.png')
            rect = image.get_rect()
            rect.center = center
            screen.blit(image, rect)
            pygame.display.update()
            pygame.time.delay(wait)
            utils.waitForPlayerToPressKey()
            return
    elif level == 4:
        if set == 1:
            image = utils.load_trans_image('lv4words1.png')
            rect = image.get_rect()
            rect.center = center
            screen.blit(image, rect)
            pygame.display.update()
            pygame.time.delay(wait)
            utils.waitForPlayerToPressKey()
            return
    elif level == 'gen':
        if set == 'item':
            image = utils.load_trans_image('wordsItem.png')
            rect = image.get_rect()
            rect.center = center
            screen.blit(image, rect)
            pygame.display.update()
            pygame.time.delay(wait)
            utils.waitForPlayerToPressKey()
            return
        if set == 'boss':
            image = utils.load_trans_image('wordsBoss.png')
            rect = image.get_rect()
            rect.center = center
            screen.blit(image, rect)
            pygame.display.update()
            pygame.time.delay(wait)
            utils.waitForPlayerToPressKey()
            return
        if set == 'ladder':
            image = utils.load_trans_image('wordsLadder.png')
            rect = image.get_rect()
            rect.center = center
            screen.blit(image, rect)
            pygame.display.update()
            pygame.time.delay(wait)
            utils.waitForPlayerToPressKey()
            return
    elif level == 0:
        if set == 'dad1':
            image = utils.load_trans_image('wordsDad1.png')
            rect = image.get_rect()
            rect.center = center
            screen.blit(image, rect)
            pygame.display.update()
            pygame.time.delay(wait)
            utils.waitForPlayerToPressKey()
            return
        elif set == 'dad2':
            image = utils.load_trans_image('wordsDad2.png')
            rect = image.get_rect()
            rect.center = center
            screen.blit(image, rect)
            pygame.display.update()
            pygame.time.delay(wait)
            utils.waitForPlayerToPressKey()
            return
        elif set == 'dad3':
            image = utils.load_trans_image('wordsDad3.png')
            rect = image.get_rect()
            rect.center = center
            screen.blit(image, rect)
            pygame.display.update()
            pygame.time.delay(wait)
            utils.waitForPlayerToPressKey()
            return
        elif set == 'judge1':
            image = utils.load_trans_image('wordsJudge1.png')
            rect = image.get_rect()
            rect.center = center
            screen.blit(image, rect)
            pygame.display.update()
            pygame.time.delay(wait)
            utils.waitForPlayerToPressKey()
            return
        elif set == 'judge2':
            image = utils.load_trans_image('wordsJudge2.png')
            rect = image.get_rect()
            rect.center = center
            screen.blit(image, rect)
            pygame.display.update()
            pygame.time.delay(wait)
            utils.waitForPlayerToPressKey()
            return
        elif set == 'judge3':
            image = utils.load_trans_image('wordsJudge3.png')
            rect = image.get_rect()
            rect.center = center
            screen.blit(image, rect)
            pygame.display.update()
            pygame.time.delay(wait)
            utils.waitForPlayerToPressKey()
            return
        elif set == 'judge4':
            image = utils.load_trans_image('wordsJudge4.png')
            rect = image.get_rect()
            rect.center = center
            screen.blit(image, rect)
            pygame.display.update()
            pygame.time.delay(wait)
            utils.waitForPlayerToPressKey()
            return
        elif set == 'tworp1':
            image = utils.load_trans_image('wordsTworp1.png')
            rect = image.get_rect()
            rect.center = center
            screen.blit(image, rect)
            pygame.display.update()
            pygame.time.delay(wait)
            utils.waitForPlayerToPressKey()
            return
        elif set == 'tworp2':
            image = utils.load_trans_image('wordsTworp2.png')
            rect = image.get_rect()
            rect.center = center
            screen.blit(image, rect)
            pygame.display.update()
            pygame.time.delay(wait)
            utils.waitForPlayerToPressKey()
            return
        