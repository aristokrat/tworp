import pygame, utils, os, npc
from pygame.locals import *
import caveinput, random, interface, environ

FRAMERATE = 8

SCREENWIDTH = 1024
SCREENHEIGHT = 768
AREAWIDTH = 1000
AREAHEIGHT = 760


def fade(screen, player, controller, globVars, imageAssets):

    mainClock = pygame.time.Clock()
    pygame.key.set_repeat(50)
    
    HUD = interface.UI()
    area = pygame.Surface((AREAWIDTH, AREAHEIGHT))
    areaRect = area.get_rect()
    areaRect.topleft = (12, 0)

    player.projectiles = []
    
    fade1 = utils.load_image('intro2fade1.png')
    fade2 = utils.load_image('intro2fade2.png')
    fade3 = utils.load_image('intro2fade3.png')
    fade4 = utils.load_image('intro2fade4.png')
    fade5 = utils.load_image('intro2fade5.png')
    fade6 = utils.load_image('intro2fade6.png')
    fade7 = utils.load_image('intro2fade7.png')
    fade8 = utils.load_image('intro2fade8.png')
    fade9 = utils.load_image('intro2fade9.png')
    fade10 = utils.load_image('intro2fade10.png')
    fade11 = utils.load_image('intro2fade11.png')
    fade12 = utils.load_image('intro2fade12.png')
    
    blank = utils.load_image('blank.png')
    
    dad = utils.load_trans_image('fatherStandLeft.png')

    judge = utils.load_trans_image('judgeStand.png')


    player.rect.topleft = (300, 650)
    
    player.climbing = True
    
    fade = None
    fadeCounter = 0

    
    ##################
    #start event loop#
    ##################
    while True:
        
        for event in pygame.event.get():
            action = caveinput.manage(event, controller)
            
            ###########################
            #process quick termination#
            ###########################
            if action == 'skip':
                return
            elif action == 'terminate':
                utils.terminate()
        
        if fadeCounter == 0:
            fade = fade1
        elif fadeCounter == 1:
            fade = fade2
        elif fadeCounter == 2:
            fade = fade3
        elif fadeCounter == 3:
            fade = fade4
        elif fadeCounter == 4:
            fade = fade5
        elif fadeCounter == 5:
            fade = fade6
        elif fadeCounter == 6:
            fade = fade5
        elif fadeCounter == 7:
            fade = fade4
        elif fadeCounter == 8:
            fade = fade5
        elif fadeCounter == 9:
            fade = fade6
        elif fadeCounter == 10:
            fade = fade7
        elif fadeCounter == 11:
            fade = fade8
        elif fadeCounter == 12:
            fade = fade9
        elif fadeCounter == 13:
            fade = fade8
        elif fadeCounter == 14:
            fade = fade7
        elif fadeCounter == 15:
            fade = fade8
        elif fadeCounter == 16:
            fade = fade9
        elif fadeCounter == 17:
            fade = fade10
        elif fadeCounter == 18:
            fade = fade11
        elif fadeCounter == 19:
            fade = fade12
        elif fadeCounter == 20:
            return
        
        
        
        fadeCounter += 1
        
        area.blit(fade, (0, -1*fadeCounter*38))
        
        area.blit(dad, (650, 650 - fadeCounter*38))
        area.blit(judge, (590, 390 - fadeCounter*38))
         
        
        
        

        player.update(area, None)
        
        screen.blit(area, (12,0))

        HUD.update(screen, player)
        
        pygame.display.update()
        area.fill((0,0,0))
        mainClock.tick(FRAMERATE)
        #print mainClock.get_fps()