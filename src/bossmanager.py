import pygame, levelgen, roomgen, utils, background, enemy, teleporter, item, os, copy
from pygame.locals import *
import caveinput, random, interface, environ, leveler, math, bossmaker, ladder, words

FRAMERATE = 30

SCREENWIDTH = 1024
SCREENHEIGHT = 768
AREAWIDTH = 1000
AREAHEIGHT = 760

#for preview, change desired effect value to 1
DARKNESSLEVEL = 1
LIGHT_RADIUS = 300

FLASHLEVEL = 2
WATERLEVEL = 3
PINWHEELLEVEL = 4


def startRoom(screen, player, controller, globVars, imageAssets, room, currentMapNode):
    
    if currentMapNode == None:
        currentMapNode = levelgen.Node(0,0)
        currentMapNode.roomName = 'last'
    
    if room == None:
        room = roomgen.Room(None, None, None, currentMapNode, None, None, player)
        player.rect.left
        
    
    
    mainClock = pygame.time.Clock()
    pygame.key.set_repeat(50)
    if globVars.sound:
        pygame.mixer.music.load(os.path.join('data', 'tworpBoss.wav'))
        pygame.mixer.music.play(-1)
    
    HUD = interface.UI()
    area = pygame.Surface((AREAWIDTH, AREAHEIGHT))
    areaRect = area.get_rect()
    areaRect.topleft = (12, 0)

    bg = background.Background()
    moveRight = moveLeft = moveUp = moveDown = False
    throwRight = throwLeft = throwUp = throwDown = False
    teleport = None
    enemies = []
    environs = []
    
    #level effect variables
    effect = False
    effectCounter = 0
    effectCounterMax = 40
    pinwheeltracker = 0
    effectChoice = 0
    
    #load level effects
    if player.gameLevel == DARKNESSLEVEL:
        darkness = utils.load_trans_image('darkness.png')
        darknessRect = darkness.get_rect()
        enemies.append(bossmaker.First(imageAssets))
    elif player.gameLevel == FLASHLEVEL:
        whiteOut = utils.load_trans_image('whiteOut.png')
        enemies.append(bossmaker.Second(imageAssets))
    elif player.gameLevel == WATERLEVEL:
        water = background.BossWater()
        boss = bossmaker.Third(imageAssets)
        boss.getWater(water)
        enemies.append(boss)
    elif player.gameLevel == PINWHEELLEVEL:
        redE = utils.load_trans_image('redE.png')
        redNE = utils.load_trans_image('redNE.png')
        redN = utils.load_trans_image('redN.png')
        redNW = utils.load_trans_image('redNW.png')
        redW = utils.load_trans_image('redW.png')
        redSW = utils.load_trans_image('redSW.png')
        redS = utils.load_trans_image('redS.png')
        redSE = utils.load_trans_image('redSW.png')
        redRect = redE.get_rect()
        reds = [redE, redNE, redN, redNW, redW, redSW, redS, redSE]
        enemies.append(bossmaker.Fourth(imageAssets))
        
    ladderPiece = ladder.Ladder()
    ladderSet = False
    climb = False
    climbing = False
    currentMapNode.item = ladderPiece
    
    

    
    
    ##################
    #start event loop#
    ##################
    while True:
        
        if ladderSet and not player.usedLadder:
            words.drawText(screen, 'gen', 'ladder')
            player.usedLadder = True
            moveRight = moveLeft = moveUp = moveDown = False
            throwRight = throwLeft = throwUp = throwDown = False
        
        if not climb:
        
            ################
            #process inputs#
            ################   
            for event in pygame.event.get():
                action = caveinput.manage(event, controller)
                
                #########################
                #digital movement inputs#
                #########################
                if action == 'moveRight':
                    moveRight = True
                    moveLeft = False
                elif action == 'moveLeft':
                    moveLeft = True
                    moveRight = False
                elif action == 'moveUp':
                    moveUp = True
                    moveDown = False
                elif action == 'moveDown':
                    moveDown = True
                    moveUp = False
                    
                #############################
                #melee attacks and abilities#
                #############################
                elif action == 'heavyMelee':
                    player.heavyMelee()
                elif action == 'lightMelee':
                    player.lightMelee()
                elif action == 'use':
                    if ladderSet:
                        climb = player.use(currentMapNode)
                elif action == 'ability1':
                    player.ability('freeze', enemies)
                elif action == 'ability2':
                    player.ability('fire', enemies)
                elif action == 'ability3':
                    player.ability('hide', enemies)
                
                #######################    
                #stop digital movement#
                #######################
                elif action == 'XmoveRight':
                    moveRight = False
                elif action == 'XmoveLeft':
                    moveLeft = False
                elif action == 'XmoveUp':
                    moveUp = False
                elif action == 'XmoveDown':
                    moveDown = False
                    
                ##########################
                #digital throwing attacks#
                ##########################    
                elif action == 'throwRight':
                    throwRight = True
                    throwLeft = False
                elif action == 'throwLeft':
                    throwLeft = True
                    throwRight = False
                elif action == 'throwUp':
                    throwUp = True
                    throwDown = False
                elif action == 'throwDown':
                    throwDown = True
                    throwUp = False
                
                ######################    
                #stop diital throwing#
                ######################
                elif action == 'XthrowRight':
                    throwRight = False
                elif action == 'XthrowLeft':
                    throwLeft = False
                elif action == 'XthrowUp':
                    throwUp = False
                elif action == 'XthrowDown':
                    throwDown = False 
                    
                ###########################
                #process quick termination#
                ###########################
                elif action == 'skip':
                    player.gameLevel += 1
                    return
                elif action == 'god':
                    player.godmode()
                elif action == 'terminate':
                    utils.terminate()
                        
            ############################
            #update background and room#
            ############################
            
            bg.update(area)
            room.updateBoss(area)
            for environ in environs:
                environ.update(area)
            
            if len(enemies) == 0 and player.rect.bottom >= 495:
                ladderSet = ladderPiece.update(area)
            
            ################
            #update enemies#
            ################
            for enemy in enemies:
                if enemy.type == 'boss':
                    enemy.update(area, player, enemies, environs, area, room.magicColor)
                elif enemy.type == 'enemy':
                    enemy.update(area, player, environs, area, room.magicColor)
            
            #if player dies
            #should call a game over screen?
            if player.health <= 0:
                if globVars.sound:
                    pygame.mixer.music.load(os.path.join('data', 'tworpMain.wav'))
                    pygame.mixer.music.play(-1)
                return
            
            
            ##########################
            #process projectile input#
            ##########################
            if throwRight or throwLeft or throwUp or throwDown:
                player.digitalThrow(throwLeft, throwRight, throwUp, throwDown)
            elif controller != None and (abs(controller.get_axis(2)) > 0.3 or abs(controller.get_axis(3)) > 0.3):
                player.analogThrow(controller.get_axis(2), controller.get_axis(3), area)
           
           
            #########################
            #process player movement#
            #########################
            if moveRight or moveLeft or moveUp or moveDown:
                player.digitalMove(moveLeft, moveRight, moveUp, moveDown, area, room.magicColor)
            elif controller != None and (abs(controller.get_axis(0)) > 0.3 or abs(controller.get_axis(1)) > 0.3):
                player.analogMove(controller.get_axis(0), controller.get_axis(1), area, room.magicColor)
            else:
                player.moving = False
                
        elif climb:
            bg.update(area)
            room.updateBoss(area)
            for environ in environs:
                environ.update(area)
            
            ladderPiece.update(area)
            
            #move player to bottom center of ladder
            if not climbing:
                xP = player.rect.centerx #changed line
                yP = player.rect.bottom #changed line
                xE = ladderPiece.rect.centerx
                yE = ladderPiece.rect.bottom
                vectorLength = math.sqrt((xP-xE)**2 + (yP-yE)**2)
                if vectorLength < 10:
                    player.rect.midbottom = ladderPiece.rect.midbottom
                    climbing = True
                else:    
                    deltaX = (-10*(xP-xE))/vectorLength #changed line
                    deltaY = (-10*(yP-yE))/vectorLength
                    player.rect.move_ip(deltaX, deltaY)
            elif climbing:
                player.climbing = True
                player.rect.move_ip(0, -5)
                if player.rect.centery < 0:
                    player.fireballs = []
                    player.climbing = False
                    player.gameLevel += 1
                    return

                
            
            
            
        
            
            
        ######################
        #draw pinwheel effect#
        ######################
        if player.gameLevel == PINWHEELLEVEL:
            redRect.center = player.rect.center
            area.blit(reds[pinwheeltracker], redRect)
            pinwheeltracker = (pinwheeltracker + 1) % 8 #randomize spinning pattern on each glitch?'''
        
        ###############################
        #process water effect movement#
        ###############################
        if player.gameLevel == WATERLEVEL and not climbing:
            x, y = water.getMove()
            player.waterMove(x, y, area, room.magicColor)
            for enemy in enemies:
                enemy.waterMove(x, y, area, room.magicColor)
            for bomb in boss.bombs:
                bomb.waterMove(x, y, area, room.magicColor)

        #######################
        #draw player to screen#
        #######################
        player.update(area, enemies)
        
        if len(enemies) == 0 and player.rect.bottom < 495 and not climbing:
            ladderSet = ladderPiece.update(area)
        
        ######################
        #draw darkness effect#
        ######################
        if player.gameLevel == DARKNESSLEVEL:
            darknessRect.center = player.rect.center
            area.blit(darkness, darknessRect)
            for enemy in enemies:
                xDistance = enemy.rect.centerx - player.rect.centerx
                yDistance = enemy.rect.centery - player.rect.centery
                distance = int(round(math.sqrt(xDistance**2 + yDistance**2),0))
                if distance < LIGHT_RADIUS:
                    enemy.inDark = False
                else:
                    enemy.inDark = True
            screen.blit(area, areaRect)
                    
        
        ###################
        #draw water effect#
        ###################
        if player.gameLevel == WATERLEVEL:
            water.update(area)
            screen.blit(area, areaRect)
        
        
        ###################
        #make level twitch#
        ###################
        if player.gameLevel == PINWHEELLEVEL:
            if effect:
                if effectChoice >= 0 and effectChoice <= 4:
                    effectedArea = pygame.transform.flip(area, False, True)
                    screen.blit(effectedArea, areaRect)
                elif effectChoice > 4 and effectChoice <= 9:
                    effectedArea = pygame.transform.flip(area, True, False)
                    screen.blit(effectedArea, areaRect)
                elif effectChoice > 9 and effectChoice <= 13:
                    effectedArea = pygame.transform.flip(area, True, True)
                    screen.blit(effectedArea, areaRect) 
                elif effectChoice == 14:
                    effectedArea = pygame.transform.rotozoom(area, 90, 0.76)
                    screen.blit(effectedArea, (223,0))
                elif effectChoice == 15:
                    effectedArea = pygame.transform.rotozoom(area, -90, 0.76)
                    screen.blit(effectedArea, (223,0))
                elif effectChoice == 16 or effectChoice == 17:
                    shrink = (500,380)
                    effectedArea = pygame.Surface(shrink)
                    pygame.transform.smoothscale(area, shrink, effectedArea)
                    screen.blit(effectedArea, (262, 190))
                elif effectChoice == 18 or effectChoice == 19:
                    zoom = (2000,1520)
                    effectedArea = pygame.Surface(zoom)
                    pygame.transform.smoothscale(area, zoom, effectedArea)
                    screen.blit(effectedArea, (-488, -380))
                effectCounter += 1
                if effectCounter == effectCounterMax:
                    effect = False
                    effectCounter = 0
                    effectCounterMax = random.randint(40,80)
            else:
                screen.blit(area, areaRect)                
                effectCounter += 1
                if effectCounter == effectCounterMax:
                    effect = True
                    effectCounter = 0
                    effectCounterMax = 4
                    effectChoice = random.randint(0,19)
                    tempEnemies = copy.copy(enemies)
                    for enemy in tempEnemies:
                        enemy.glitch(enemies)
            


        ######################
        #process flash effect#
        ######################
        if player.gameLevel == FLASHLEVEL:
            if effect:
                area.blit(whiteOut, (0,0))
                effectCounter += 1
                if effectCounter == effectCounterMax:
                    for enemy in enemies:
                        enemy.phase()
                    effect = False
                    effectCounter = 0
                    effectCounterMax = random.randint(60,90)
            else:
                effectCounter += 1
                if effectCounter == effectCounterMax:
                    effect = True
                    effectCounter = 0
                    effectCounterMax = random.randint(3,10)
            screen.blit(area, areaRect)
        
            
        HUD.update(screen, player)
        
        pygame.display.update()
        mainClock.tick(FRAMERATE)
        #print mainClock.get_fps()
        
        if player.levelUp == True:
            leveler.Up(screen, area, areaRect, player, controller)
            moveRight = moveLeft = moveUp = moveDown = False
            throwRight = throwLeft = throwUp = throwDown = False
