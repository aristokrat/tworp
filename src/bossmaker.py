import pygame, utils, math, random, time, projectile, enemy
from pygame.locals import *

AREAWIDTH = 1000
AREAHEIGHT = 760
    

class Base:
    def __init__(self,imageAssets):
        self.life = 0
        self.image = utils.load_trans_image('bossTemp.png')
        self.rect = self.image.get_rect()
        self.rect.center = (500,360)
        self.baseMoveRate = 0
        self.baseDamage = 0
        self.expPayload = 0
        self.manaPayload = 0
        self.moveRate = 0
        self.imageAssets = imageAssets
        self.effects = self.loadEffects()
        self.type = 'boss'
        #new level effect variables
        self.inDark = False
        self.phased = False
        #ability variables
        self.frozen = False
        self.iced = False
        self.freezeCounter = 0
        self.aflame = False
        self.onFire = 0
        self.bleeding = 0
        self.freezeAble = False
        self.burnAble = True
        self.burnDamageRate = 0
        
    def loadEffects(self):
        return self.imageAssets.imageList("fireEffect")
    
    def drawEffects(self, screen):
        if self.frozen:
            image = self.effects['ice']
            rect = image.get_rect()
            rect.center = self.rect.center
            screen.blit(image,rect.topleft)
        if self.iced:
            image = self.effects['lightIce']
            rect = image.get_rect()
            rect.center = self.rect.center
            screen.blit(image,rect.topleft)
        if self.onFire >= 1:
            if self.burnAble:
                burnType = "fire"
            else:
                burnType = "smoke"
            image = self.effects[burnType][self.onFire]
            rect = image.get_rect()
            rect.center = self.rect.center
            screen.blit(image,rect.topleft)
            self.onFire -= 1
        if self.bleeding > 0:
            image = self.effects["blood"][self.bleeding]
            rect = image.get_rect()
            rect.center = self.rect.center
            screen.blit(image,rect.topleft)
            self.bleeding -= 1
            
    def draw(self, screen):
        screen.blit(self.image, self.rect.topleft)
        self.drawEffects(screen)
    
    def update(self, screen, player, enemies, environs, room, magicColor):
        if self.aflame:
            self.life -= self.burnDamageRate
            if self.life <= 0:
                self.life = 1
                self.aflame = False
        if self.frozen:
            self.freezeCounter -= 1
            if self.freezeCounter == 0:
                self.frozen = False
                self.moveRate = self.baseMoveRate
        if self.iced:
            self.moveRate = self.baseMoveRate/2
        else:
            self.moveRate = self.baseMoveRate
        if not self.phased:
            self.draw(screen)
            
                
    def takeThrowDamage(self, damage):
        if self.frozen:
            damage = damage/2
        self.life -= damage
        if self.life <= 0:
            return 'dead'
        
    #####################
    #new exp/mana method#
    #####################
    def giveUpTheGoods(self):
        return (self.expPayload, self.manaPayload)
    
    ###################
    #new freeze method#
    ###################
    def freeze(self, time, icy = False):
        if self.freezeAble:
            self.frozen = True
            self.freezeCounter = time
            if icy:
                self.iced = True
                self.aflame = False
        
    ###########################
    #new takeFireDamage method#
    ###########################
    def takeFireDamage(self, damage, burn = False):
        if self.onFire == 0:
                self.onFire = 6
        if self.burnAble:
            if not self.iced or not self.frozen:
                self.life -= damage
            if burn:
                self.aflame = True
                self.iced = False
                self.frozen = False
            if self.life <= 0:
                return 'dead'
        else:
            self.iced = False
            self.frozen = False
        
        
    def takeMeleeDamage(self, damage, knockback):
        if self.frozen:
            damage = damage/2
        else:
            self.bleeding = self.effects["blood"]["count"]
            #print self.effects["blood"]
        self.life -= damage
        self.rect.move_ip(knockback)
        if self.life <= 0:
            return 'dead'
        
    def pickPoint(self):#picks a random point on the screen
        return (random.randint(200,AREAWIDTH -200),random.randint(100,AREAHEIGHT -100) )
    
    
    
    
    ###############
    ####  NEW  ####
    ###############
    
    
    
    def waterMove(self, x, y, room, magicColor):
        if x > 0:
            self.moveRight(room, magicColor, x)
        elif x < 0:
            self.moveLeft(room, magicColor, 0-x)
        if y > 0:
            self.moveDown(room, magicColor, y)
        elif y < 0:
            self.moveUp(room, magicColor, 0-y)
    
    #move left, scanning left edge for collisions
    def moveLeft(self, room, magicColor, distance):
        if self.rect.left - distance > 0:
            maxMove = distance
            x = self.rect.left
            y = self.rect.top
            #scan edge
            while maxMove > 0 and y < self.rect.bottom:
                move = 1
                while move <= maxMove:
                    if room.get_at((x-move,y)) == magicColor:
                        if maxMove > move - 1:
                            maxMove = move - 1
                            break
                    else:
                        move += 1
                y += 1
            
            self.rect.move_ip(-1*maxMove, 0)
        else: self.rect.left = 0
    
    #move right, scanning right edge for collisions    
    def moveRight(self, room, magicColor, distance):
        if self.rect.right + distance < AREAWIDTH:
            maxMove = distance
            x = self.rect.right
            y = self.rect.top
            #scan edge
            while maxMove > 0 and y < self.rect.bottom:
                move = 1
                while move <= maxMove:
                    if room.get_at((x-1+move,y)) == magicColor:
                        if maxMove > move - 1:
                            maxMove = move - 1
                        break
                    else:
                        move += 1
                y += 1

            self.rect.move_ip(maxMove, 0)
        else: self.rect.right = AREAWIDTH
        
    #move up, scanning top edge for collisions
    def moveUp(self, room, magicColor, distance):
        if self.rect.top - distance > 0:
            maxMove = distance
            y = self.rect.top
            x = self.rect.left
            #scan edge
            while maxMove > 0 and x < self.rect.right:
                move = 1
                while move <= maxMove:
                    if room.get_at((x,y-move)) == magicColor:
                        if maxMove > move - 1:
                            maxMove = move - 1
                        break
                    else:
                        move += 1
                x += 1
                
            self.rect.move_ip(0, -1*maxMove)
        else: self.rect.top = 0
        
    #move down, scanning bottom edge for collisions
    def moveDown(self, room, magicColor, distance):
        if self.rect.bottom + distance < AREAHEIGHT:
            maxMove = distance
            y = self.rect.bottom
            x = self.rect.left
            #scan edge
            while maxMove > 0 and x < self.rect.right:
                move = 1
                while move <= maxMove:
                    if room.get_at((x,y-1+move)) == magicColor:
                        if maxMove > move - 1:
                            maxMove = move - 1
                        break
                    else:
                        move += 1
                x += 1
                    
            self.rect.move_ip(0, maxMove)
        else: self.rect.bottom = AREAHEIGHT
        
class First(Base):
    
    def __init__(self,imageAssets):
        self.life = 1000
        self.images = self.load_images()
        self.image = self.images["flip"]
        self.rect = self.image.get_rect()
        self.rect.center = (500, 360)
        self.baseMoveRate = 0
        self.rockDamage = 0
        self.expPayload = 100
        self.manaPayload = 500
        self.moveRate = 0
        self.imageAssets = imageAssets
        self.effects = self.loadEffects()
        self.type = 'boss'
        #new level effect variables
        self.inDark = False
        self.phased = False
        #ability variables
        self.frozen = False
        self.iced = False
        self.freezeCounter = 0
        self.aflame = False
        self.onFire = 0
        self.bleeding = 0
        self.freezeAble = False
        self.burnAble = True
        self.burnDamageRate = 0
        
        
        self.rocks = []
        self.lastRockTime = 0
        self.rockTimeDelay = 0.5
        self.rockMoveRate = 10
        
        self.bats = []
        self.batTimeDelay = 1
        self.lastBatTime = 0
        
        self.flap = "flip"
        
        self.mode = 0
        self.modeCounter = 0
        self.modeCounterMax = random.randint(120, 240)
        
        self.hidingSpots = [(73,98), (368,98), (632,98), (927,98), (73,317), (927,317), (73,538), (927,538), (198,662), (500,622), (802,662)]
        
    def load_images(self):
        images = {}
        images['flap'] = utils.load_trans_image('BossOneFlap.png')
        images["flip"] = utils.load_trans_image('BossOneFlip.png')
        images["open"] = utils.load_trans_image('BossOneOpen.png')
        return images
        
    def update(self, screen, player, enemies, environs, room, magicColor):
        if self.mode == 0:
            if self.flap == "flip":
                self.flap = "flap"
            else:
                self.flap = "flip"
            self.image = self.images[self.flap]
            if not self.frozen:
                if self.getPlayerDistance(player) < 300:
                    self.findNewHidingSpot(player)
                if time.time() - self.lastRockTime > self.rockTimeDelay:
                    xP = player.lastRect.centerx #changed line
                    yP = player.lastRect.centery #changed line
                    xE = self.rect.centerx
                    yE = self.rect.centery
                    vectorLength = math.sqrt((xP-xE)**2 + (yP-yE)**2)
                    if vectorLength == 0.0:
                        vectorLength = 0.0001
                    #move basically moveRate number of units toward player, based on ratio calculation
                    deltaX = (self.rockMoveRate*(xP-xE))/vectorLength #changed line
                    deltaY = (self.rockMoveRate*(yP-yE))/vectorLength
                    self.rocks.append(projectile.BossRock((deltaX, deltaY), self.rect))
                    self.lastRockTime = time.time()
            self.modeCounter += 1
            if self.modeCounter == self.modeCounterMax:
                self.mode = 1
                self.modeCounter = 0
                self.modeCounterMax = random.randint(120, 240)
            self.draw(screen)
        elif self.mode == 1:
            self.image = self.images["open"]
            if not self.frozen:
                if time.time() - self.lastBatTime > self.batTimeDelay:
                    bat = enemy.BossBat(self.imageAssets)
                    bat.rect.center = self.rect.center
                    enemies.append(bat)
                    self.lastBatTime = time.time()
            self.modeCounter += 1
            if self.modeCounter == self.modeCounterMax:
                self.mode = 2
                self.modeCounter = 0
                self.modeCounterMax = random.randint(90, 180)
            self.draw(screen)
        elif self.mode == 2:
            self.modeCounter += 1
            if self.modeCounter == self.modeCounterMax:
                self.mode = 0
                self.modeCounter = 0
                self.modeCounterMax = random.randint(120, 240)
            self.draw(screen)
        
        
        
        for rock in self.rocks:
            rock.update(screen)
        tempRocks = self.rocks
        for rock in tempRocks:
            if rock.rect.left < 0 or rock.rect.right > AREAWIDTH or rock.rect.top < 0 or rock.rect.bottom > AREAHEIGHT:
                self.rocks.remove(rock)
            if rock.rect.colliderect(player.rect):
                player.takeDamage(self.rockDamage)
                self.rocks.remove(rock)
            
                
    def getPlayerDistance(self, player):
        xDistance = self.rect.centerx - player.rect.centerx
        yDistance = self.rect.centery - player.rect.centery
        distance = int(round(math.sqrt(xDistance**2 + yDistance**2),0))
        return distance
    
    def findNewHidingSpot(self, player):
        counter = 0
        while counter < 20:
            self.rect.center = random.choice(self.hidingSpots)
            if self.getPlayerDistance(player) > 400:
                break
            
class Second(Base): #needs to take a lot of knockback damage to encourage melee damage
    
    def __init__(self,imageAssets):
        self.life = 1000
        self.images = self.load_images()
        self.image = self.images["leftClosed"]
        self.rect = self.image.get_rect()
        self.rect.center = (500, 360)
        self.baseMoveRate = 0
        self.baseDamage = 40
        self.expPayload = 100
        self.manaPayload = 500
        self.moveRate = 10
        self.imageAssets = imageAssets
        self.effects = self.loadEffects()
        self.type = 'boss'
        #new level effect variables
        self.inDark = False
        self.phased = False
        #ability variables
        self.frozen = False
        self.iced = False
        self.freezeCounter = 0
        self.aflame = False
        self.onFire = 0
        self.bleeding = 0
        self.freezeAble = False
        self.burnAble = True
        self.burnDamageRate = 0
        
        self.deployed = False
        
        self.startingSpots = [(73,98), (368,98), (632,98), (927,98), (73,317), (927,317), (73,538), (927,538), (198,662), (500,622), (802,662)]
        
    def update(self, screen, player, enemies, environs, room, magicColor):
        if self.phased:
            if not self.deployed:
                shooter = enemy.BossShooter(self.imageAssets)
                shooter.rect.center = random.choice(self.startingSpots)
                enemies.append(shooter)
                self.deployed = True
        elif not self.phased:
            xP = player.rect.centerx
            yP = player.rect.centery
            xE = self.rect.centerx
            yE = self.rect.centery
            vectorLength = math.sqrt((xP-xE)**2 + (yP-yE)**2)
            if vectorLength == 0.0:
                vectorLength = 0.0001
            #move basically moveRate number of units toward player, based on ratio calculation
            deltaX = (self.moveRate*(xP-xE))/vectorLength
            deltaY = (self.moveRate*(yP-yE))/vectorLength
                        
            if deltaX < 0:
                if vectorLength > 200:
                    self.image = self.images["leftClosed"]
                else:
                    self.image = self.images["leftOpen"]
            else:
                if vectorLength > 200:
                    self.image = self.images["rightClosed"]
                else:
                    self.image = self.images["rightOpen"]
            
            self.rect.move_ip(deltaX, deltaY)
            if self.rect.colliderect(player.rect):
                player.takeDamage(self.baseDamage)
                self.rect.center = random.choice(self.startingSpots)
            
            self.draw(screen)
    
    def phase(self):
        if self.phased:
            self.phased = False
            self.rect.center = random.choice(self.startingSpots)
        elif not self.phased:
            self.phased = True
            self.rect.topleft = (1000,760)
            self.deployed = False
            
    def load_images(self):
        images = {}
        images['leftOpen'] = utils.load_trans_image('b2LeftOpen.png')
        images["rightOpen"] = utils.load_trans_image('b2RightOpen.png')
        images["leftClosed"] = utils.load_trans_image('b2LeftClose.png')
        images["rightClosed"] = utils.load_trans_image('b2RightClose.png')
        return images
    
class Third(Base):
    
    def __init__(self,imageAssets):
        self.life = 1000
        self.images = self.load_images()
        self.image = self.images["leftClosed"]
        self.rect = self.image.get_rect()
        self.rect.center = (500, 360)
        self.baseMoveRate = 0
        self.baseDamage = 20
        self.expPayload = 100
        self.manaPayload = 500
        self.moveRate = 20
        self.imageAssets = imageAssets
        self.effects = self.loadEffects()
        self.type = 'boss'
        #new level effect variables
        self.inDark = False
        self.phased = False
        #ability variables
        self.frozen = False
        self.iced = False
        self.freezeCounter = 0
        self.aflame = False
        self.onFire = 0
        self.bleeding = 0
        self.freezeAble = False
        self.burnAble = True
        self.burnDamageRate = 0
        
        self.water = None
        
        self.directionx = 0
        self.directiony = 0
        self.directed = False
        
        self.mode = 0
        self.modeCounter = 0
        self.modeCounterMax = random.randint(120, 240)
        
        self.target = None
        self.bombs = []
        
        self.bombingSpots = [(500,98),(73,317), (927,317), (500,622)]
        
    def getWater(self, water):
        self.water = water
        
    
    def update(self, screen, player, enemies, environs, room, magicColor):
        if self.mode == 0:
            self.modeCounter += 1
            if self.modeCounter == self.modeCounterMax:
                self.mode = 1
                self.modeCounter = 0
                self.modeCounterMax = random.randint(120, 240)
            if not self.directed:
                self.chooseDirection()
            hitWall = False
            hitSideWall = False
            if self.directionx > 0:
                hitSideWall = self.moveRight(room, magicColor, self.directionx)
            elif self.directionx < 0:
                hitSideWall = self.moveLeft(room, magicColor, 0-self.directionx)
            if self.directiony > 0:
                hitWall = self.moveDown(room, magicColor, self.directiony)
            elif self.directiony < 0:
                hitWall = self.moveUp(room, magicColor, 0-self.directiony)
            if hitWall or hitSideWall:
                self.directed = False
        elif self.mode == 1:
            if self.target == None:
                self.target = random.choice(self.bombingSpots)
            xT = self.target[0]
            yT = self.target[1]
            xE = self.rect.centerx
            yE = self.rect.centery
            vectorLength = math.sqrt((xT-xE)**2 + (yT-yE)**2)
            if vectorLength < 15:
                if self.target[0] == 500: #top or bottom placement
                    self.mode = 2
                else: self.mode = 3
                self.target = None
                
            else:
                deltaX = (self.moveRate*(xT-xE))/vectorLength
                deltaY = (self.moveRate*(yT-yE))/vectorLength
                self.rect.move_ip(deltaX, deltaY)
        elif self.mode == 2:
            y = 0
            if self.rect.centery > 380:
                y = self.rect.bottom+10
            else:
                y = self.rect.top+20
            for i in range(3):
                x = self.rect.centerx + 70 + i*125
                self.bombs.append(enemy.Bomb(self.imageAssets, x, y, self))
                x = self.rect.centerx - 70 - i*125
                self.bombs.append(enemy.Bomb(self.imageAssets, x, y, self))
            self.mode = 4
        elif self.mode == 3:
            x = 0
            if self.rect.centerx > 500:
                x = self.rect.right-20
            else:
                x = self.rect.left+20
            for i in range(2):
                y = self.rect.centery + 120 + i*140
                self.bombs.append(enemy.Bomb(self.imageAssets, x, y, self))
                y = self.rect.centery - 120 - i*140
                self.bombs.append(enemy.Bomb(self.imageAssets, x, y, self))
            self.mode = 4
        elif self.mode == 4:
            if self.modeCounter == 0:
                self.water.controlled = True
                current = 29
                var = random.randint(1,4)
                varDir = random.choice((-1, 1))
                if self.rect.centerx > 700:
                    self.water.setCurrent(current, -1, var, varDir)
                elif self.rect.centerx < 300:
                    self.water.setCurrent(current, 1, var, varDir)
                elif self.rect.centery > 380:
                    self.water.setCurrent(var, varDir, current, -1)
                else:
                    self.water.setCurrent(var, varDir, current, 1)
            self.modeCounter += 1
            
            if self.modeCounter == 30:
                self.modeCounter = 0
                self.mode = 0
                self.water.controlled = False
        for bomb in self.bombs:
            bomb.update(screen, player, environs, room, magicColor)
        if self.rect.colliderect(player.rect):
                player.takeDamage(self.baseDamage)
        
        if self.directionx < 0:
            if self.directiony > 0:
                self.image = self.images["leftClosed"]
            else:
                self.image = self.images["leftOpen"]
        else:
            if self.directiony > 0:
                self.image = self.images["rightClosed"]
            else:
                self.image = self.images["rightOpen"]
        
        self.draw(screen)
 
    def chooseDirection(self):
        a = random.uniform(0.0, 1.0)
        b = math.sqrt(1 - a**2)
        s1 = random.choice ((-1,1))
        s2 = random.choice ((-1,1))
        self.directionx = int(s1 * a * self.moveRate)
        self.directiony = int(s2 * b * self.moveRate)
        self.directed = True
        
    def load_images(self):
        images = {}
        images['leftOpen'] = utils.load_trans_image('waterBossLeftOpen.png')
        images["rightOpen"] = utils.load_trans_image('waterBossRightOpen.png')
        images["leftClosed"] = utils.load_trans_image('waterBossLeft.png')
        images["rightClosed"] = utils.load_trans_image('waterBossRight.png')
        return images 
    
    def moveRight(self, room, magicColor, distance):
        right = self.rect.right
        Base.moveRight(self, room, magicColor, distance)
        if distance != 0 and self.rect.right == right:
            return True
        else:
            return False
        
    def moveLeft(self, room, magicColor, distance):
        left = self.rect.left
        Base.moveLeft(self, room, magicColor, distance)
        if distance != 0 and self.rect.left == left:
            return True
        else:
            return False
            
    def moveDown(self, room, magicColor, distance):
        bottom = self.rect.bottom
        Base.moveDown(self, room, magicColor, distance)
        if distance != 0 and self.rect.bottom == bottom:
            return True
        else:
            return False        
            
    def moveUp(self, room, magicColor, distance):
        top = self.rect.top
        Base.moveUp(self, room, magicColor, distance)
        if distance != 0 and self.rect.top == top:
            return True
        else:
            return False
        
    def removeBomb(self, bomb):
        self.bombs.remove(bomb)
        
    def waterMove(self, x, y, room, magicColor):
        True
        

class Fourth(Base):
    
    def __init__(self,imageAssets, scale = 100, shootCounter = 0):
        self.images = self.load_images()
        factor = scale/100.0
        self.images[1] = pygame.transform.scale(self.images[1], (int(75*factor), int(125*factor)))
        self.images[2] = pygame.transform.scale(self.images[2], (int(75*factor), int(125*factor)))
        self.images[3] = pygame.transform.scale(self.images[3], (int(75*factor), int(125*factor)))
        self.images[4] = pygame.transform.scale(self.images[4], (int(75*factor), int(125*factor)))
        
        self.image = self.images[1]
        
        self.rect = self.image.get_rect()
        self.rect.center = (500, 360)
        self.baseMoveRate = 0
        self.expPayload = 100
        self.manaPayload = 500
        self.moveRate = 20
        self.imageAssets = imageAssets
        self.effects = self.loadEffects()
        self.type = 'boss'
        #new level effect variables
        self.inDark = False
        self.phased = False
        #ability variables
        self.frozen = False
        self.iced = False
        self.freezeCounter = 0
        self.aflame = False
        self.onFire = 0
        self.bleeding = 0
        self.freezeAble = False
        self.burnAble = True
        self.burnDamageRate = 0
        
        self.imageChange = 0
        
        self.directionx = 0
        self.directiony = 0
        self.directed = False
        
        self.life = int(400 * factor)
        self.baseDamage = int(30*factor)
        self.gooDamage = int(10*factor)
        self.scale = scale
        self.shootCounter = shootCounter
        self.shots = []
        self.gooRate = 40
        
    def update(self, screen, player, enemies, environs, room, magicColor):
            if self.imageChange == 1:
                self.image = self.images[random.randint(1,4)]
                self.imageChange = 0
            else:
                self.imageChange +=1

            if self.shootCounter == 60:
                self.shootCounter = 0
                vector = self.getGooVector(player)
                self.shots.append(projectile.BossGoo(vector, self.rect))
 
            self.shootCounter += 1
            
            if not self.directed:
                self.chooseDirection()
            hitWall = False
            hitSideWall = False
            if self.directionx > 0:
                hitSideWall = self.moveRight(room, magicColor, self.directionx)
            elif self.directionx < 0:
                hitSideWall = self.moveLeft(room, magicColor, 0-self.directionx)
            if self.directiony > 0:
                hitWall = self.moveDown(room, magicColor, self.directiony)
            elif self.directiony < 0:
                hitWall = self.moveUp(room, magicColor, 0-self.directiony)
            if hitWall or hitSideWall:
                self.directed = False
            
            for shot in self.shots:
                shot.update(screen)
            tempShots = self.shots
            for shot in tempShots:
                if shot.rect.left < 0 or shot.rect.right > AREAWIDTH or shot.rect.top < 0 or shot.rect.bottom > AREAHEIGHT:
                    self.shots.remove(shot)
                if shot.rect.colliderect(player.rect):
                    player.takeDamage(self.gooDamage)
                    self.shots.remove(shot)
            
            if self.rect.colliderect(player.rect):
                player.takeDamage(self.baseDamage)        
                
            self.draw(screen)
            
    def load_images(self):
        images = {}
        images[1] = utils.load_trans_image('finalBoss0001.png')
        images[2] = utils.load_trans_image('finalBoss0002.png')
        images[3] = utils.load_trans_image('finalBoss0003.png')
        images[4] = utils.load_trans_image('finalBoss0004.png')
        return images 
            
    def glitch(self, enemies):
        if self.scale > 50:
            scale = int(self.scale*0.75)
            enemies.append(Fourth(self.imageAssets, scale, self.shootCounter))
        
        
    def getGooVector(self, player):
        xP = player.rect.centerx
        yP = player.rect.centery
        xE = self.rect.centerx
        yE = self.rect.centery
        vectorLength = math.sqrt((xP-xE)**2 + (yP-yE)**2)
        if vectorLength == 0.0:
            vectorLength = 0.0001
        #move basically moveRate number of units toward player, based on ratio calculation
        deltaX = (self.gooRate*(xP-xE))/vectorLength
        deltaY = (self.gooRate*(yP-yE))/vectorLength
        return (deltaX, deltaY)

    def chooseDirection(self):
        a = random.uniform(0.0, 1.0)
        b = math.sqrt(1 - a**2)
        s1 = random.choice ((-1,1))
        s2 = random.choice ((-1,1))
        self.directionx = int(s1 * a * self.moveRate)
        self.directiony = int(s2 * b * self.moveRate)
        self.directed = True
        
    def moveRight(self, room, magicColor, distance):
        right = self.rect.right
        Base.moveRight(self, room, magicColor, distance)
        if distance != 0 and self.rect.right == right:
            return True
        else:
            return False
        
    def moveLeft(self, room, magicColor, distance):
        left = self.rect.left
        Base.moveLeft(self, room, magicColor, distance)
        if distance != 0 and self.rect.left == left:
            return True
        else:
            return False
            
    def moveDown(self, room, magicColor, distance):
        bottom = self.rect.bottom
        Base.moveDown(self, room, magicColor, distance)
        if distance != 0 and self.rect.bottom == bottom:
            return True
        else:
            return False        
            
    def moveUp(self, room, magicColor, distance):
        top = self.rect.top
        Base.moveUp(self, room, magicColor, distance)
        if distance != 0 and self.rect.top == top:
            return True
        else:
            return False
        
        