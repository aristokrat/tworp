import enemy, pygame, random, enemy, environ
from pygame.locals import *

class Slots():
    
    def __init__(self):
        self.slots = []
    
    def fill(self, things):
        for thing in things:
            thing[2].rect.center = (thing[0]*51+512, thing[1]*81+384)
            self.slots.append(thing[2])
        
#SLOTS MAP - each slot is 50x80 with 1 pixel cushion
#corners ([-4,-3], [4,-3], etc.) are iffy for being within room boundaries, but all else should be good
#63 total spots!
#
# [-4,-3] [-3,-3] [-2,-3] [-1,-3] [ 0,-3] [ 1,-3] [ 2,-3] [3,-3] [4,-3]
# [-4,-2] [-2,-2] [-2,-2] [-1,-2] [ 0,-2] [ 1,-2] [ 2,-2] [2,-2] [4,-2]
# [-4,-1] [-1,-1] [-2,-1] [-1,-1] [ 0,-1] [ 1,-1] [ 2,-1] [1,-1] [4,-1]
# [-4,-0] [-0,-0] [-2,-0] [-1,-0] [ 0,-0] [ 1,-0] [ 2,-0] [0,-0] [4,-0]
# [-4, 1] [ 1, 1] [-2, 1] [-1, 1] [ 0, 1] [ 1, 1] [ 2, 1] [3, 1] [4, 1]
# [-4, 2] [ 2, 2] [-2, 2] [-1, 2] [ 0, 2] [ 1, 2] [ 2, 2] [3, 2] [4, 2]
# [-4, 3] [ 3, 3] [-3, 3] [-1, 3] [ 0, 3] [ 1, 3] [ 3, 3] [3, 3] [4, 3]

def first(assets):
    ivy = enemy.Ivy(assets)
    ivy.rect.center = (630, 380)
    return [ivy]


def fill(assets, level):
    grid = Slots()
    
    if level == 1:
        choice = random.randint(0,12)
        #choice = 0
        if choice == 0:
            grid.fill([ [-2,-1,environ.Rock(assets)], [-2,1,environ.Rock(assets)], [0,0,enemy.Dummy(assets)], [2,-1,environ.Rock(assets)], [2,1,environ.Rock(assets)] ])
        elif choice == 1:
            grid.fill([ [-2,-1,enemy.Bat(assets)], [3,-3,enemy.Bat(assets)], [2,0,enemy.Bat(assets)],[2,-1,enemy.Dummy(assets)]])
        elif choice == 2:
            grid.fill([ [3,-3,enemy.Bat(assets)], [2,-1,enemy.Snail(assets)], [-1,1,enemy.Bat(assets)], [4,0,enemy.Bat(assets)]])
        elif choice == 3:
            grid.fill([[-2,-1,enemy.Bat(assets)], [3,-3,enemy.Bat(assets)], [2,-1,enemy.RunAway(assets)], [-1,1,enemy.Bat(assets)], [4,0,enemy.Bat(assets)]])
        elif choice == 4:
            grid.fill([[-2,-1,environ.Rock(assets)], [-2,1,environ.Rock(assets)], [0,0,enemy.RunAway(assets)], [2,-1,environ.Rock(assets)], [2,1,environ.Rock(assets)]])
        elif choice == 5:
            grid.fill([[2,2,enemy.Dummy(assets)], [2,-1,enemy.RunAway(assets)], [2,1,environ.Rock(assets)], [0,2,environ.Rock(assets)], [3,3,environ.Rock(assets)]])
        elif choice == 6:
            grid.fill([[-2,-1,enemy.Bat(assets)], [3,-3,enemy.Bat(assets)], [2,-1,enemy.RunAway(assets)],[1,-3,enemy.Bat(assets)],[2,-4,enemy.Bat(assets)],[4,0,enemy.Bat(assets)],[1,2,enemy.Bat(assets)]])
        elif choice == 7:
            grid.fill([[-2,-1,environ.Rock(assets)], [-2,1,environ.Rock(assets)], [0,0,enemy.Snail(assets)], [2,-1,environ.Rock(assets)], [2,1,environ.Rock(assets)]])
        elif choice == 8:
            grid.fill([[-2,-1,environ.Rock(assets)], [-2,1,environ.Rock(assets)], [0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)], [2,-1,environ.Rock(assets)], [2,1,environ.Rock(assets)]])
        elif choice == 9:
            grid.fill([[-2,-1,environ.Rock(assets)], [-3,-1,environ.Rock(assets)],[-4,1,environ.Rock(assets)],[2,1,environ.Rock(assets)],[3,1,environ.Rock(assets)], [1,-3,environ.Rock(assets)],[0,0,enemy.RunAway(assets)], [2,-1,environ.Rock(assets)], [2,1,environ.Rock(assets)]])
        elif choice == 10:
            grid.fill([ [-2,-1,enemy.Ivy(assets)], [-2,1,enemy.Ivy(assets)], [0,0,enemy.Dummy(assets)], [2,-1,enemy.Ivy(assets)], [2,1,enemy.Ivy(assets)] ])
        elif choice == 11:
            grid.fill([ [0,0,enemy.Ivy(assets)] ])
        elif choice == 12:
            grid.fill([[-2,-1,enemy.Ivy(assets)], [-2,1,enemy.Ivy(assets)], [0,0,enemy.Ivy(assets)], [2,-1,enemy.Ivy(assets)], [2,1,enemy.Ivy(assets)]])
            
    elif level == 2:
        choice = random.randint(0,9)
        if choice == 0:
            grid.fill([ [-2,-1,environ.Rock(assets)], [-2,1,environ.Rock(assets)], [0,0,enemy.Shooter(assets)], [2,-1,environ.Rock(assets)], [2,1,environ.Rock(assets)] ])
        elif choice == 1:
            grid.fill([ [-4,-2,enemy.Dummy(assets)], [-4,2,enemy.Dummy(assets)], [2,2,enemy.Dummy(assets)], [2,4,enemy.Dummy(assets)], [-2,2,enemy.Dummy(assets)],[-2,-4,enemy.Dummy(assets)], [4,3,enemy.Dummy(assets)], [4,1,enemy.Dummy(assets)], [4,-1,enemy.Dummy(assets)], ])
        elif choice == 2:
            grid.fill([ [-2,-1,environ.Rock(assets)], [-2,1,environ.Rock(assets)], [0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)], [2,-1,environ.Rock(assets)], [2,1,environ.Rock(assets)] ])
        elif choice == 3:
            grid.fill([ [0,0,enemy.Shooter(assets)] ,[1,0,enemy.Shooter(assets)],[0,1,enemy.Shooter(assets)],[-1,0,enemy.Shooter(assets)],[0,-1,enemy.Shooter(assets)],[1,1,enemy.Ivy(assets)],[1,-1,enemy.Ivy(assets)],[-1,-1,enemy.Ivy(assets)],[1,-1,enemy.Ivy(assets)] ])
        elif choice == 4:
            grid.fill([ [-3,-1,enemy.Ivy(assets)], [-3,1,environ.Rock(assets)], [0,0,enemy.Snail(assets)], [3,-1,enemy.Ivy(assets)], [3,1,environ.Rock(assets)] ])
        elif choice == 5:
            grid.fill([ [4,2,enemy.Snail(assets)], [-4,-2,enemy.Snail(assets)], [4,1,enemy.Snail(assets)], [-4,-1,enemy.Snail(assets)], [3,2,enemy.Snail(assets)], [-3,-2,enemy.Snail(assets)], [-1,-1,enemy.Snail(assets)], ])
        elif choice == 6:
            grid.fill([ [-2,-1,environ.Rock(assets)], [-3,1,enemy.Ivy(assets)], [4,1,enemy.Dummy(assets)], [4,3,enemy.Dummy(assets)], [-4,1,enemy.Dummy(assets)], [4,3,enemy.Dummy(assets)], [-4,-3,enemy.Dummy(assets)], [-3,2,enemy.Dummy(assets)],[2,2,enemy.Dummy(assets)], [-2,2,enemy.Dummy(assets)], [2,-1,environ.Rock(assets)], [2,1,environ.Rock(assets)] ])
        elif choice == 7:
            grid.fill([ [4,2,enemy.Snail(assets)], [-4,-2,enemy.Snail(assets)], [4,1,enemy.Snail(assets)], [4,3,enemy.Dummy(assets)], [-4,1,enemy.Dummy(assets)], [4,3,enemy.Dummy(assets)]])
        elif choice == 8:
            grid.fill([ [4,2,enemy.Snail(assets)], [-4,-2,enemy.Snail(assets)], [4,1,enemy.Snail(assets)], [0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)] ])
        elif choice == 9:
            grid.fill([ [4,1,enemy.Bat(assets)],[4,2,enemy.Bat(assets)], [4,3,enemy.Bat(assets)], [4,-1,enemy.Bat(assets)],  [4,-2,enemy.Bat(assets)], [4,3,enemy.Bat(assets)],[2,1,enemy.Bat(assets)],[2,2,enemy.Bat(assets)], [2,3,enemy.Bat(assets)], [2,4,enemy.Bat(assets)], [-2,-4,enemy.Bat(assets)], [-3,-4,enemy.Bat(assets)],[-2,-2,enemy.Bat(assets)],[-3,-2,enemy.Bat(assets)],    ])
    
    
    elif level == 3:
        choice = random.randint(0,14)
        if choice == 0:
            grid.fill([ [-2,-1,environ.Rock(assets)], [-2,1,environ.Rock(assets)], [0,0,enemy.Teleport(assets)], [2,-1,environ.Rock(assets)], [2,1,environ.Rock(assets)] ])
        elif choice == 1:
            grid.fill([ [-2,-1,environ.Rock(assets)], [-2,1,environ.Rock(assets)], [0,0,enemy.Shieldy(assets)], [2,-1,environ.Rock(assets)], [2,1,environ.Rock(assets)] ])
        elif choice == 2:
            grid.fill([ [-4,-2,enemy.Dummy(assets)], [-4,2,enemy.Dummy(assets)], [2,2,enemy.Dummy(assets)], [2,4,enemy.Dummy(assets)], [-2,2,enemy.Dummy(assets)],[-2,-4,enemy.Dummy(assets)], [4,3,enemy.Dummy(assets)], [4,1,enemy.Dummy(assets)], [4,-1,enemy.Dummy(assets)], [-4,-2,enemy.RunAway(assets)], [-4,2,enemy.RunAway(assets)], [2,2,enemy.RunAway(assets)], [2,4,enemy.RunAway(assets)], [-2,2,enemy.RunAway(assets)],[-2,-4,enemy.RunAway(assets)], [4,3,enemy.RunAway(assets)], [4,1,enemy.RunAway(assets)], [4,-1,enemy.RunAway(assets)],])
        elif choice == 3:
            grid.fill([ [-2,-1,environ.Rock(assets)], [-2,1,environ.Rock(assets)], [0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)], [2,-1,environ.Rock(assets)], [2,1,environ.Rock(assets)] ,[-4,-1,enemy.Snail(assets)], [3,2,enemy.Snail(assets)], [-3,-2,enemy.Snail(assets)], [-1,-1,enemy.Snail(assets)],])
        elif choice == 4:
            grid.fill([ [0,0,enemy.Shooter(assets)] ,[1,0,enemy.Shooter(assets)],[0,1,enemy.Shooter(assets)],[-1,0,enemy.Shooter(assets)],[0,-1,enemy.Shooter(assets)],[1,1,enemy.Snail(assets)],[1,-1,enemy.Snail(assets)],[-1,-1,enemy.Snail(assets)],[1,-1,enemy.Snail(assets)],[2,1,enemy.Snail(assets)],[2,-1,enemy.Snail(assets)] ,[1,-2,enemy.Snail(assets)],[1,2,enemy.Snail(assets)]])
        elif choice == 5:
            grid.fill([ [-3,-1,enemy.Ivy(assets)], [-3,1,environ.Rock(assets)], [0,0,enemy.Teleport(assets)], [3,-1,enemy.Ivy(assets)], [3,1,environ.Rock(assets)] ])
        elif choice == 6:
            grid.fill([ [4,2,enemy.Snail(assets)], [-4,-2,enemy.Snail(assets)], [4,1,enemy.Snail(assets)], [-4,-1,enemy.Snail(assets)], [3,2,enemy.Snail(assets)], [-3,-2,enemy.Snail(assets)], [-1,-1,enemy.Snail(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)] ])
        elif choice == 7:
            grid.fill([[0,0,enemy.Boss(assets)] ])
        elif choice == 8:
            grid.fill([ [4,2,enemy.Teleport(assets)], [4,2,enemy.Teleport(assets)],  [4,3,enemy.Dummy(assets)], [-4,1,enemy.Dummy(assets)], [4,3,enemy.Dummy(assets)]])
        elif choice == 9:
            grid.fill([ [4,2,enemy.Snail(assets)], [-4,-2,enemy.Snail(assets)], [4,1,enemy.Snail(assets)], [0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)] ])
        elif choice == 10:
            grid.fill([ [4,1,enemy.Bat(assets)],[4,2,enemy.Bat(assets)], [4,3,enemy.Bat(assets)], [4,-1,enemy.Bat(assets)],  [4,-2,enemy.Bat(assets)], [4,3,enemy.Bat(assets)],[2,1,enemy.Bat(assets)],[2,2,enemy.Bat(assets)], [2,3,enemy.Bat(assets)], [2,4,enemy.Bat(assets)], [-2,-4,enemy.Bat(assets)], [-3,-4,enemy.Bat(assets)],[-2,-2,enemy.Bat(assets)],[-3,-2,enemy.Bat(assets)],   [4,1,enemy.Bat(assets)],[4,2,enemy.Bat(assets)], [4,3,enemy.Bat(assets)], [4,-1,enemy.Bat(assets)],  [4,-2,enemy.Bat(assets)], [4,3,enemy.Bat(assets)],[4,1,enemy.Bat(assets)],[4,2,enemy.Bat(assets)], [4,3,enemy.Bat(assets)], [4,-1,enemy.Bat(assets)],  [4,-2,enemy.Bat(assets)], [4,3,enemy.Bat(assets)], ])
        elif choice == 11:
            grid.fill([ [4,2,enemy.Shieldy(assets)], [4,2,enemy.Shieldy(assets)],  [4,3,enemy.Shooter(assets)], [-4,1,enemy.Shooter(assets)], [4,3,enemy.Shooter(assets)]])
        elif choice == 12:
            grid.fill([ [4,2,enemy.Teleport(assets)],[-4,-2, enemy.Shieldy(assets)]])
        elif choice == 13:
            grid.fill([ [4,2,enemy.Shieldy(assets)],[-4,-2, enemy.Shieldy(assets)]])
        elif choice == 14:
            grid.fill([ [2,2,enemy.Teleport(assets)],[-4,-2, enemy.Teleport(assets)]])
        

    
    
    elif level == 4:
        choice = random.randint(0,4)
        if choice == 0:
            grid.fill([  [0,0,enemy.Teleport(assets)],[0,0,enemy.Teleport(assets)],[0,0,enemy.Teleport(assets)],[0,0,enemy.Teleport(assets)],[0,0,enemy.Teleport(assets)],[0,0,enemy.Teleport(assets)],[0,0,enemy.Teleport(assets)],[0,0,enemy.Teleport(assets)], ])
        elif choice == 1:
            grid.fill([  [-4,-3,enemy.Snail(assets)],[-4,-2,enemy.Snail(assets)],[-4,-1,enemy.Snail(assets)],[-4,0,enemy.Snail(assets)],[-4,1,enemy.Snail(assets)],[-4,2,enemy.Snail(assets)],[-4,3,enemy.Snail(assets)],[4,-3,enemy.Snail(assets)], [4,-2,enemy.Snail(assets)],[4,-1,enemy.Snail(assets)],[4,0,enemy.Snail(assets)],[4,1,enemy.Snail(assets)],[4,2,enemy.Snail(assets)],[4,3,enemy.Snail(assets)],[0,0,enemy.Snail(assets)],[2,2,enemy.Snail(assets)], ])
        elif choice == 2:
            grid.fill([[-4,-3,enemy.Shieldy(assets)],[4,3,enemy.Shieldy(assets)],[2,2,enemy.Shieldy(assets)],[-2,-3,enemy.Shieldy(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)]])
        elif choice == 3:
            grid.fill([[-4,-3,enemy.Shieldy(assets)],[4,3,enemy.Shieldy(assets)],[2,2,enemy.Shieldy(assets)],[-2,-3,enemy.Shieldy(assets)],[0,0,enemy.Teleport(assets)],[0,0,enemy.Teleport(assets)],[0,0,enemy.Teleport(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)],[0,0,enemy.RunAway(assets)]])
        elif choice == 4:
            grid.fill([ [2,2,enemy.GooBlob(assets)],[-4,-2, enemy.GooBlob(assets)], [0,2,enemy.GooBlob(assets)],[4,-2, enemy.GooBlob(assets)]])
        elif choice == 5:
            grid.fill([[0,0,enemy.Boss(assets)],[2,2,enemy.GooBlob(assets)],[2,2,enemy.RunAway(assets)], ])
        elif choice == 5:
            grid.fill([[0,0,enemy.Boss(assets)],[2,2,enemy.Snail(assets)], [-2,2,enemy.Snail(assets)]])
    return grid.slots


    
    
    
    
    
    
    
    #c = []
    """
    c.append([enemy.Bat(assets), enemy.Bat(assets, (427, 309)), enemy.Bat(assets, (547, 309)), enemy.Bat(assets, (427, 415)), enemy.Bat(assets, (547, 415)), enemy.Bat(assets, (487, 256)), enemy.Bat(assets, (367, 362)), enemy.Bat(assets, (487, 468)), enemy.Bat(assets, (607, 362))])
    c.append([enemy.Dummy(assets), enemy.Bat(assets, (487, 291)), enemy.Bat(assets, (427, 362)), enemy.Bat(assets, (487, 434)), enemy.Bat(assets, (547, 362))])
    c.append([enemy.Dummy(assets, (452, 344)), enemy.Dummy(assets, (522, 344))])
    c.append([enemy.Shooter(assets, (452, 323)), enemy.Shooter(assets, (522, 323)), enemy.Shooter(assets, (452, 394)), enemy.Shooter(assets, (522, 394))])
    c.append([enemy.Shooter(assets), enemy.Bat(assets, (487, 291)), enemy.Bat(assets, (427, 362)), enemy.Bat(assets, (487, 434)), enemy.Bat(assets, (547, 362))])
    c.append([enemy.Dummy(assets), enemy.Shooter(assets, (427, 358)), enemy.Shooter(assets, (547, 358))])
    c.append([enemy.Shooter(assets, (462, 358)), enemy.Shooter(assets, (512, 358)), enemy.Ivy(assets, (412, 301)), enemy.Ivy(assets, (412, 356)), enemy.Ivy(assets, (412, 411)), enemy.Ivy(assets, (462, 303)), enemy.Ivy(assets, (462, 409)), enemy.Ivy(assets, (512, 303)), enemy.Ivy(assets, (512, 409)), enemy.Ivy(assets, (562, 301)), enemy.Ivy(assets, (562, 356)), enemy.Ivy(assets, (562, 411))])
    c.append([enemy.Bat(assets), enemy.Bat(assets, (427, 309)), enemy.Bat(assets, (547, 309)), enemy.Bat(assets, (427, 415)), enemy.Bat(assets, (547, 415)), enemy.Ivy(assets, (487, 246)), enemy.Ivy(assets, (487, 301)), enemy.Ivy(assets, (387, 356)), enemy.Ivy(assets, (437, 356)), enemy.Ivy(assets, (537, 356)), enemy.Ivy(assets, (537, 356)), enemy.Ivy(assets, (587, 356)), enemy.Ivy(assets, (487, 411)), enemy.Ivy(assets, (487, 466))])
    c.append([enemy.Sieldy(assets, (427, 309)),enemy.Shieldy(assets, (347, 415)), enemy.Shieldy(assets, (727, 415)) ])
    c.append([enemy.Snail(assets, (300,300)),enemy.Snail(assets, (300,200)),enemy.Snail(assets, (200,300)),enemy.Snail(assets, (450,450)),enemy.Snail(assets, (500,450)),enemy.Snail(assets, (450,500)),])
    c.append([enemy.Dummy(assets), enemy.Shooter(assets, (427, 358)), enemy.Shooter(assets, (547, 358)), enemy.Snail(assets, (500,450)),enemy.Snail(assets, (450,500))])
    c.append([enemy.Shooter(assets, (462, 358)), enemy.Shooter(assets, (512, 358)), enemy.Snail(assets, (412, 301)), enemy.Snail(assets, (412, 356)), enemy.Snail(assets, (412, 411)), enemy.Snail(assets, (462, 303)), enemy.Snail(assets, (462, 409)), enemy.Snail(assets, (512, 303)), enemy.Snail(assets, (512, 409)), enemy.Snail(assets, (562, 301)), enemy.Snail(assets, (562, 356)), enemy.Snail(assets, (562, 411))])
    c.append([enemy.Shieldy(assets), enemy.Bat(assets, (487, 291)), enemy.Bat(assets, (427, 362)), enemy.Bat(assets, (487, 434)), enemy.Bat(assets, (547, 362))])
    c.append([enemy.Shieldy(assets, (452, 344)), enemy.Dummy(assets, (522, 344))])
    c.append([enemy.Snail(assets), enemy.Snail(assets, (427, 309)), enemy.Snail(assets, (547, 309)), enemy.Snail(assets, (427, 415)), enemy.Snail(assets, (547, 415)), enemy.Ivy(assets, (487, 246)), enemy.Ivy(assets, (487, 301)), enemy.Ivy(assets, (387, 356)), enemy.Ivy(assets, (437, 356)), enemy.Ivy(assets, (537, 356)), enemy.Ivy(assets, (537, 356)), enemy.Ivy(assets, (587, 356)), enemy.Ivy(assets, (487, 411)), enemy.Ivy(assets, (487, 466))])
    c.append([enemy.Shooter(assets, (452, 344)), enemy.Shieldy(assets, (522, 344))])
    c.append([enemy.GooBlob(assets, (300,200)),enemy.GooBlob(assets, (200,300)),enemy.GooBlob(assets, (450,450)),enemy.GooBlob(assets, (500,450)),enemy.GooBlob(assets, (450,500)),])
    c.append([enemy.Shooter(assets, (462, 358)), enemy.Shooter(assets, (512, 358)), enemy.GooBlob(assets, (412, 301)), enemy.GooBlob(assets, (412, 356)), enemy.GooBlob(assets, (412, 411)), enemy.GooBlob(assets, (462, 303)), enemy.GooBlob(assets, (462, 409)), enemy.GooBlob(assets, (512, 303)), enemy.GooBlob(assets, (512, 409)), enemy.GooBlob(assets, (562, 301)), enemy.GooBlob(assets, (562, 356)), enemy.GooBlob(assets, (562, 411))])
    """
    
    #return random.choice(c)'''
    #return [enemy.GooBlob()]
    #return [enemy.Snail((300,300)),enemy.GooBlob((300,200)),enemy.GooBlob((200,300)),enemy.GooBlob((450,450)),enemy.GooBlob((500,450)),enemy.GooBlob((450,500)),]
    #return [enemy.Shieldy((427, 309)),enemy.Shieldy((347, 415)), enemy.Shieldy((727, 415)) ]#tests sheildy
    #return [enemy.Snail((300,300)),enemy.Snail((300,200)),enemy.Snail((200,300)),enemy.Snail((450,450)),enemy.Snail((500,450)),enemy.Snail((450,500)),] #test snails
    #return [enemy.Dummy()]
    #return [enemy.Bat(),enemy.Dummy(),enemy.Shieldy(),enemy.Shooter(),enemy.Snail(),enemy.Ivy(), enemy.Boss()]

    #return random.choice(c)
    #return [enemy.GooBlob(assets)]
    #return [enemy.Snail(assets, (300,300)),enemy.GooBlob(assets, (300,200)),enemy.GooBlob(assets, (200,300)),enemy.GooBlob(assets, (450,450)),enemy.GooBlob(assets, (500,450)),enemy.GooBlob(assets, (450,500)),]
    #return [enemy.Shieldy(assets, (427, 309)),enemy.Shieldy(assets, (347, 415)), enemy.Shieldy(assets, (727, 415)) ]#tests sheildy
    #return [enemy.Snail(assets, (300,300)),enemy.Snail(assets, (300,200)),enemy.Snail(assets, (200,300)),enemy.Snail(assets, (450,450)),enemy.Snail(assets, (500,450)),enemy.Snail(assets, (450,500)),] #test snails
    #return [enemy.Dummy(assets)]
    #return [enemy.Bat(assets),enemy.Dummy(assets),enemy.Shieldy(assets),enemy.Shooter(assets),enemy.Snail(assets),enemy.Ivy(assets), enemy.Boss(assets), enemy.GooBlob(assets)]


    
    
    
    
