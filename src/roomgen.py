import random, pygame, utils
from pygame.locals import *

ROUNDBOTTOMPROB = 0.3
ROUNDTOPPROB = 0.7
AREAWIDTH = 1000
AREAHEIGHT = 760
HORITILES = AREAWIDTH/40
VERTTILES = AREAHEIGHT/40

class Room():

    def __init__(self, edge, start, width, thisNode, secret, lastRoomName, player):
        self.grid = []
        self.level = str(player.gameLevel)
        self.flooring = None
        if thisNode.roomName == 'first' or thisNode.roomName == "mainPath" or thisNode.roomName == 'penultimate' or thisNode.roomName == 'last':
            self.flooring = 'land'
        else:
            self.flooring = 'side'
        self.openings = thisNode.doors
        
        self.topWidth = random.randint(3,7)
        self.topStart = random.randint(8, (HORITILES-8)-self.topWidth)
        
        self.bottomWidth = random.randint(3,7)
        self.bottomStart = random.randint(8, (HORITILES-8)-self.bottomWidth)
        
        self.leftWidth = random.randint(3,7)
        self.leftStart = random.randint(4, (VERTTILES-5)-self.leftWidth)
        
        self.rightWidth = random.randint(3,7)
        self.rightStart = random.randint(4, (VERTTILES-5)-self.rightWidth)
        
        
        if edge == 'top':
            self.topStart = start
            self.topWidth = width
        elif edge == 'bottom':
            self.bottomStart = start
            self.bottomWidth = width
        elif edge == 'right':
            self.rightStart = start
            self.rightWidth = width
        elif edge == 'left':
            self.leftStart = start
            self.leftWidth = width
        self.closeHoles(secret, lastRoomName)
        self.generateGrid()
        if thisNode.roomName == 'last':
            self.bossCorners()
        else:
            self.roundCorners()
        self.placeEdges()
        self.background, self.magicColor = self.flatImage(thisNode)
        self.walls = self.walledImage() 
        
        
        
    def generateGrid(self):
        wall = '0'
        floor = '5'
        for i in range(HORITILES):
            if i == 0:
                column = []
                for j in range(VERTTILES):
                    if j < self.leftStart:
                        column.append(wall)
                    elif j >= self.leftStart and j < self.leftStart + self.leftWidth:
                        column.append(floor)
                    else:
                        column.append(wall)
                self.grid.append(column)
            elif i > 0 and i < (HORITILES-1):
                column = []
                for j in range(VERTTILES):
                    if j == 0:
                        if i < self.topStart:
                            column.append(wall)
                        elif i >= self.topStart and i < self.topStart + self.topWidth:
                            column.append(floor)
                        else:
                            column.append(wall)
                    elif j > 0 and j < (VERTTILES-1):
                        column.append(floor)
                        
                    elif j == (VERTTILES-1):
                        if i < self.bottomStart:
                            column.append(wall)
                        elif i >= self.bottomStart and i < self.bottomStart + self.bottomWidth:
                            column.append(floor)
                        else:
                            column.append(wall)
                self.grid.append(column)
            else:
                column = []
                for j in range(VERTTILES):
                    if j < self.rightStart:
                        column.append(wall)
                    elif j >= self.rightStart and j < self.rightStart + self.rightWidth:
                        column.append(floor)
                    else:
                        column.append(wall)
                self.grid.append(column)
                
    def bossCorners(self):
        wall = '0' 
        for j in range(4):
            for i in range(4):
                self.grid[i][VERTTILES-j-1] = wall
                self.grid[HORITILES-i-1][VERTTILES-j-1] = wall
    
    def roundCorners(self):
        #round top left corner
        wall = '0'        
        for i in range(1,self.topStart):
            for j in range(1, self.leftStart):
                if self.grid[i-1][j] == wall and self.grid[i][j-1] == wall:
                    leftFraction = 1.0 * (self.leftStart - j)/self.leftStart
                    topFraction = 1.0 * (self.topStart - i)/self.topStart
                    avgFraction = (leftFraction + topFraction)/2
                    if avgFraction > random.uniform(ROUNDBOTTOMPROB, ROUNDTOPPROB):
                        self.grid[i][j] = wall
        #round out bottom left corner
        leftLength = VERTTILES-(self.leftStart+self.leftWidth)
        for i in range(1, self.bottomStart):
            for j in range(1, leftLength):
                if self.grid[i-1][(VERTTILES-1)-j] == wall and self.grid[i][VERTTILES-j] == wall:
                    bottomFraction = 1.0 * (self.bottomStart - i)/self.bottomStart
                    leftFraction = 1.0 * (leftLength - j) / leftLength
                    avgFraction = (bottomFraction + leftFraction)/2
                    if avgFraction > random.uniform(ROUNDBOTTOMPROB, ROUNDTOPPROB):
                        self.grid[i][(VERTTILES-1)-j] = wall
        #round out top right corner
        topLength = HORITILES-(self.topStart+self.topWidth)
        for i in range(1, topLength):
            for j in range(1, self.rightStart):
                if self.grid[HORITILES-i][j] == wall and self.grid[(HORITILES-1)-i][j-1] == wall:
                    rightFraction = 1.0 * (self.rightStart - j)/self.rightStart
                    topFraction = 1.0 * (topLength - i) / topLength
                    avgFraction = (rightFraction + topFraction)/2
                    if avgFraction > random.uniform(ROUNDBOTTOMPROB, ROUNDTOPPROB):
                        self.grid[(HORITILES-1)-i][j] = wall
        #round out bottom right corner
        rightLength = VERTTILES-(self.rightStart+self.rightWidth)
        bottomLength = HORITILES-(self.bottomStart+self.bottomWidth)
        for i in range(1, bottomLength):
            for j in range (1, rightLength):
                if self.grid[HORITILES-i][(VERTTILES-1)-j] == wall and self.grid[(HORITILES-1)-i][VERTTILES-j] == wall:
                    rightFraction = 1.0 * (rightLength - j)/rightLength
                    bottomFraction = 1.0 * (bottomLength - i) / bottomLength
                    avgFraction = (rightFraction + bottomFraction)/2
                    if avgFraction > random.uniform(ROUNDBOTTOMPROB, ROUNDTOPPROB):
                        self.grid[(HORITILES-1)-i][(VERTTILES-1)-j] = wall
    
    def closeHoles(self, secret, lastRoomName):
        if 'u' not in self.openings:
            self.topWidth = 0
            self.topStart = HORITILES/2
        if 'd' not in self.openings:
            self.bottomWidth = 0
            self.bottomStart = HORITILES/2
        if 'r' not in self.openings:
            self.rightWidth = 0
            self.rightStart = VERTTILES/2
        if 'l' not in self.openings:
            self.leftWidth = 0
            self.leftStart = VERTTILES/2
        
        
        #if secret node is adjacent, 33% chance of it showing up
        if secret == 'up' and lastRoomName != 'secret':
            chance = random.randint(0,2)
            if chance > 0:
                self.topWidth = 0
                self.topStart = HORITILES/2
        elif secret == 'down' and lastRoomName != 'secret':
            chance = random.randint(0,2)
            if chance > 0:
                self.bottomWidth = 0
                self.bottomStart = HORITILES/2
        elif secret == 'right' and lastRoomName != 'secret':
            chance = random.randint(0,2)
            if chance > 0:
                self.rightWidth = 0
                self.rightStart = VERTTILES/2
        elif secret == 'left' and lastRoomName != 'secret':
            chance = random.randint(0,2)
            if chance > 0:
                self.leftWidth = 0
                self.leftStart = VERTTILES/2
                
            
    def placeEdges(self):
        
        wall = '0'
        land = '5'
        landEdgeL = '4'
        landEdgeR = '6'
        landEdgeU = '8'
        landEdgeD = '2'
        landEdgeLU = '7'
        landEdgeLD = '1'
        landEdgeRU = '9'
        landEdgeRD = '3'
        landTipLU = '77'
        landTipLD = '11'
        landTipRU = '99'
        landTipRD = '33'

        
        for i in range(HORITILES):
            for j in range(VERTTILES):
                if self.grid[i][j] == wall:
                    landEdges = ''
                    if i+1 <= (HORITILES-1) and self.grid[i+1][j] == land:
                        landEdges += 'l'
                    if i-1 >= 0 and self.grid[i-1][j] == land:
                        landEdges += 'r'
                    if j+1 <= (VERTTILES-1) and self.grid[i][j+1] == land:
                        landEdges += 'u'
                    if j-1 >= 0 and self.grid[i][j-1] == land:
                        landEdges += 'd'
                    
                    if landEdges == "l":
                        self.grid[i][j] = landEdgeL
                    elif landEdges == "r":
                        self.grid[i][j] = landEdgeR
                    elif landEdges == "d":
                        self.grid[i][j] = landEdgeD
                    elif landEdges == "u":
                        self.grid[i][j] = landEdgeU
                    elif landEdges == "ld":
                        self.grid[i][j] = landEdgeLD
                    elif landEdges == "lu":
                        self.grid[i][j] = landEdgeLU
                    elif landEdges == "rd":
                        self.grid[i][j] = landEdgeRD
                    elif landEdges == "ru":
                        self.grid[i][j] = landEdgeRU
                        
        for i in range(HORITILES):
            for j in range(VERTTILES):
                if self.grid[i][j] == wall:
                        if i+1 <= (HORITILES-1) and j+1 <= (VERTTILES-1) and (self.grid[i+1][j] == landEdgeU or self.grid[i+1][j] == landEdgeLU) and (self.grid[i][j+1] == landEdgeL or self.grid[i][j+1] == landEdgeLU):
                            self.grid[i][j] = landTipLU
                            
                        elif i+1 <= (HORITILES-1) and j-1 >= 0 and (self.grid[i+1][j] == landEdgeD or self.grid[i+1][j] == landEdgeLD) and (self.grid[i][j-1] == landEdgeL or self.grid[i][j-1] == landEdgeLD):
                            self.grid[i][j] = landTipLD
                            
                        elif i-1 >= 0 and j+1 <= (VERTTILES-1) and (self.grid[i-1][j] == landEdgeU or self.grid[i-1][j] == landEdgeRU) and (self.grid[i][j+1] == landEdgeR or self.grid[i][j+1] == landEdgeRU):
                            self.grid[i][j] = landTipRU
                            
                        elif i-1 >= 0 and j-1 >= 0 and (self.grid[i-1][j] == landEdgeD or self.grid[i-1][j] == landEdgeRD) and (self.grid[i][j-1] == landEdgeR or self.grid[i][j-1] == landEdgeRD):
                            self.grid[i][j] = landTipRD
                        
                    
            
    def flatImage(self, thisNode):
        wall = '0'
        land = '5'
        landEdge4 = '4'
        landEdge6 = '6'
        landEdge8 = '8'
        landEdge2 = '2'
        landEdge7 = '7'
        landEdge1 = '1'
        landEdge9 = '9'
        landEdge3 = '3'
        landTip77 = '77'
        landTip11 = '11'
        landTip99 = '99'
        landTip33 = '33'
        
        land1 = utils.load_image('lv' + self.level + self.flooring + '1.png')
        land2 = utils.load_image('lv' + self.level + self.flooring + '2.png')
        land3 = utils.load_image('lv' + self.level + self.flooring + '3.png')
        land4 = utils.load_image('lv' + self.level + self.flooring + '4.png')
        land5 = utils.load_image('lv' + self.level + self.flooring + '5.png')
        land6 = utils.load_image('lv' + self.level + self.flooring + '6.png')
        land7 = utils.load_image('lv' + self.level + self.flooring + '7.png')
        land8 = utils.load_image('lv' + self.level + self.flooring + '8.png')
        land9 = utils.load_image('lv' + self.level + self.flooring + '9.png')
        land11 = utils.load_image('lv' + self.level + self.flooring + '11.png')
        land33 = utils.load_image('lv' + self.level + self.flooring + '33.png')
        land77 = utils.load_image('lv' + self.level + self.flooring + '77.png')
        land99 = utils.load_image('lv' + self.level + self.flooring + '99.png')
        filler = utils.load_image('land0.png')
        magicColorTile = utils.load_image('magicColor.png')
        
        #X = nonwalkablearea
        background = pygame.Surface((AREAWIDTH, AREAHEIGHT))
        for i in range(HORITILES):
            for j in range(VERTTILES):
                if self.grid[i][j] == land:
                    background.blit(land5, (40*i, 40*j))
                elif self.grid[i][j] == landEdge1:
                    background.blit(land1, (40*i, 40*j))
                elif self.grid[i][j] == landEdge2:
                    background.blit(land2, (40*i, 40*j))
                elif self.grid[i][j] == landEdge3:
                    background.blit(land3, (40*i, 40*j))
                elif self.grid[i][j] == landEdge4:
                    background.blit(land4, (40*i, 40*j))
                elif self.grid[i][j] == landEdge6:
                    background.blit(land6, (40*i, 40*j))
                elif self.grid[i][j] == landEdge7:
                    background.blit(land7, (40*i, 40*j))
                elif self.grid[i][j] == landEdge8:
                    background.blit(land8, (40*i, 40*j))
                elif self.grid[i][j] == landEdge9:
                    background.blit(land9, (40*i, 40*j))
                elif self.grid[i][j] == landTip11:
                    background.blit(land11, (40*i, 40*j))
                elif self.grid[i][j] == landTip33:
                    background.blit(land33, (40*i, 40*j))
                elif self.grid[i][j] == landTip77:
                    background.blit(land77, (40*i, 40*j))
                elif self.grid[i][j] == landTip99:
                    background.blit(land99, (40*i, 40*j))
                else:
                    background.blit(filler, (40*i, 40*j))
            
        fadeUp = utils.load_trans_image('lv' + self.level + 'fadeUp.png')
        fadeDown = utils.load_trans_image('lv' + self.level + 'fadeDown.png')
        fadeRight = utils.load_trans_image('lv' + self.level + 'fadeRight.png')
        fadeLeft = utils.load_trans_image('lv' + self.level + 'fadeLeft.png')
        
        if self.flooring == 'land':
            if 'u' in self.openings and (thisNode.up.roomName == 'normal' or thisNode.up.roomName == 'secret'):
                for i in range (HORITILES):
                    if self.grid[i][0] == land:
                        background.blit(fadeUp, (40*i, 0))
            if 'd' in self.openings and (thisNode.down.roomName == 'normal' or thisNode.down.roomName == 'secret'):
                for i in range (HORITILES):
                    if self.grid[i][VERTTILES-1] == land:
                        background.blit(fadeDown, (40*i, 40*(VERTTILES-1)))
            if 'r' in self.openings and (thisNode.right.roomName == 'normal' or thisNode.right.roomName == 'secret'):
                for j in range (VERTTILES):
                    if self.grid[HORITILES-1][j] == land:
                        background.blit(fadeRight, (40*(HORITILES-1), 40*j))
            if 'l' in self.openings and (thisNode.left.roomName == 'normal' or thisNode.left.roomName == 'secret'):
                for j in range (VERTTILES):
                    if self.grid[0][j] == land:
                        background.blit(fadeLeft, (0, 40*j))
                        
        bossfadeUp = utils.load_trans_image('bossfadeUp.png')
        bossfadeDown = utils.load_trans_image('bossfadeDown.png')
        bossfadeRight = utils.load_trans_image('bossfadeRight.png')
        bossfadeLeft = utils.load_trans_image('bossfadeLeft.png')
        
        if self.flooring == 'land':
            if 'u' in self.openings and (thisNode.up.roomName == 'last'):
                for i in range (HORITILES):
                    if self.grid[i][0] == land:
                        background.blit(bossfadeUp, (40*i, 0))
            if 'd' in self.openings and (thisNode.down.roomName == 'last'):
                for i in range (HORITILES):
                    if self.grid[i][VERTTILES-1] == land:
                        background.blit(bossfadeDown, (40*i, 40*(VERTTILES-1)))
            if 'r' in self.openings and (thisNode.right.roomName == 'last'):
                for j in range (VERTTILES):
                    if self.grid[HORITILES-1][j] == land:
                        background.blit(bossfadeRight, (40*(HORITILES-1), 40*j))
            if 'l' in self.openings and (thisNode.left.roomName == 'last'):
                for j in range (VERTTILES):
                    if self.grid[0][j] == land:
                        background.blit(bossfadeLeft, (0, 40*j))
        
        
        
        colorkey = background.get_at((0,0))
        background.set_colorkey(colorkey, RLEACCEL)
        
        return background, magicColorTile.get_at((20,20))

    
    def walledImage(self):
        land = '5'
        wallImage = pygame.Surface((AREAWIDTH, AREAHEIGHT))
        wallImage.blit(self.background, (0,0))
        fenceNS = utils.load_image('fenceNS.png', -1)
        fenceEW = utils.load_image('fenceEW.png', -1)
        
        for i in range(HORITILES):
            if self.grid[i][0] == land:
                wallImage.blit(fenceNS, (40*i, 0))
            if self.grid[i][VERTTILES-1] == land:
                wallImage.blit(fenceNS, (40*i, 40*(VERTTILES-1)))
        for j in range(VERTTILES):
            if self.grid[0][j] == land:
                wallImage.blit(fenceEW, (0, 40*j))
            if self.grid[HORITILES-1][j] == land:
                wallImage.blit(fenceEW, (40*(HORITILES-1), 40*j))
        
        colorkey = wallImage.get_at((0,0))
        wallImage.set_colorkey(colorkey, RLEACCEL)
                
        return wallImage
                
    
    def update(self, screen, numEnemies):
        if numEnemies == 0:
            screen.blit(self.background, (0,0))
        else:
            screen.blit(self.walls, (0,0))
            
    def updateBoss(self, screen):
        screen.blit(self.walls, (0,0))
        
        
            
def main():
    pygame.init()
    screen = pygame.display.set_mode((AREAWIDTH,AREAHEIGHT))
    pygame.display.set_caption("Room Test")
    
    
    thisRoom = Room('top', 14, 2, ['r', 'd'], None, None)
    thisRoom.update(screen)
    pygame.display.update()
    
    utils.waitForPlayerToPressKey()

    
if __name__ == '__main__': main()
                            