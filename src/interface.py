import pygame, utils
from pygame.locals import *

class UI():
    
    def __init__(self):
        self.frame = utils.load_trans_image('UIframe.png')
        self.exp = utils.load_image('UIexp.png')
        self.health = utils.load_image('UIhealth.png')
        self.mana = utils.load_image('UImana.png')
        self.healthMask1 = utils.load_image('UIhealthMask1.png')
        self.healthMask2 = utils.load_image('UIhealthMask2.png')
        self.healthMask3 = utils.load_image('UIhealthMask3.png')
        self.images = self.loadImages()
        self.projectile = (734, 735)
        self.weapon = (762, 735)
        self.ice = (790, 735)
        self.fire = (818, 735)
        self.hide = (846, 735)
       
        
    def update(self, screen, player):
        
        colorKey = self.healthMask1.get_at((0,0))
        healthVal = float(player.health)
        max = float(player.maxHealth)
        healthOffset = 125 - int((healthVal/max)*125.0) + 5 #where openspace is
        health = pygame.Surface((139, 139))
        health.fill(colorKey)
        health.blit(self.health, (0, healthOffset))
        health.blit(self.healthMask1, (99,0))
        health.blit(self.healthMask2, (111,0))
        health.blit(self.healthMask3, (125,0))
        health.set_colorkey(colorKey, RLEACCEL)
        screen.blit(health, (6, 625))
        
        manaVal = float(player.mana)
        max = float(player.maxMana)
        manaOffset = 125 - int((manaVal/max)*125.0) + 5 #where openspace is
        mana = pygame.Surface((139, 139))
        mana.fill(colorKey)
        mana.blit(self.mana, (0, manaOffset))
        mana.blit(self.healthMask3, (0,-3))
        mana.blit(self.healthMask2, (14,0))
        mana.blit(self.healthMask1, (28,0))
        mana.set_colorkey(colorKey, RLEACCEL)
        screen.blit(mana, (881, 625))
        
        expVal = float(player.exp)
        max = float(player.expThreshold)
        expOffset = int((expVal/max)*137.0) - 145 #where openspace is
        exp = pygame.Surface((137, 21))
        exp.fill(colorKey)
        exp.blit(self.exp, (expOffset, 0))
        exp.set_colorkey(colorKey, RLEACCEL)
        screen.blit(exp, (156, 734))
        
        screen.blit(self.frame, (0,0))
        
        if player.projectile != None:
            screen.blit(self.images[player.projectile], self.projectile)
        if player.weapon != None:
            screen.blit(self.images[player.weapon.name], self.weapon)
        
        if player.iceLevel == 1:
            screen.blit(self.images['ice1'], self.ice)
        elif player.iceLevel == 2:
            screen.blit(self.images['ice2'], self.ice)
        
        if player.fireLevel == 1:
            screen.blit(self.images['fire1'], self.fire)
        elif player.fireLevel == 2:
            screen.blit(self.images['fire2'], self.fire)
        
        if player.hideLevel == 1:
            screen.blit(self.images['hide1'], self.hide)
        elif player.hideLevel == 2:
            screen.blit(self.images['hide2'], self.hide)
        
        
    def loadImages(self):
        images = {}
        
        images['coin'] = utils.load_trans_image('UIcoin.png')
        images['acorn'] = utils.load_trans_image('UIacorn.png')
        images['egg'] = utils.load_trans_image('UIegg.png')
        images['eye'] = utils.load_trans_image('UIeye.png')
        images['cloud'] = utils.load_trans_image('UIcloud.png')
        images['puffer'] = utils.load_trans_image('UIpuffer.png')
        
        images['stick'] = utils.load_trans_image('UIstick.png')
        
        images['sword1'] = utils.load_trans_image('UIsword1.png')
        images['sword2'] = utils.load_trans_image('UIsword2.png')
        images['sword3'] = utils.load_trans_image('UIsword3.png')
        images['sword4'] = utils.load_trans_image('UIsword4.png')
        images['sword5'] = utils.load_trans_image('UIsword5.png')
        images['sword6'] = utils.load_trans_image('UIsword6.png')
        
        images['blade1'] = utils.load_trans_image('UIblade1.png')
        images['blade2'] = utils.load_trans_image('UIblade2.png')
        images['blade3'] = utils.load_trans_image('UIblade3.png')
        images['blade4'] = utils.load_trans_image('UIblade4.png')
        images['blade5'] = utils.load_trans_image('UIblade5.png')
        images['blade6'] = utils.load_trans_image('UIblade6.png')
        
        images['hammer1'] = utils.load_trans_image('UIhammer1.png')
        images['hammer2'] = utils.load_trans_image('UIhammer2.png')
        images['hammer3'] = utils.load_trans_image('UIhammer3.png')
        images['hammer4'] = utils.load_trans_image('UIhammer4.png')
        images['hammer5'] = utils.load_trans_image('UIhammer5.png')
        images['hammer6'] = utils.load_trans_image('UIhammer6.png')
        
        images['ice2'] = utils.load_trans_image('UIice2.png')
        images['ice1'] = utils.load_trans_image('UIice1.png')
        
        images['fire2'] = utils.load_trans_image('UIfire2.png')
        images['fire1'] = utils.load_trans_image('UIfire1.png')
        
        images['hide2'] = utils.load_trans_image('UIhide2.png')
        images['hide1'] = utils.load_trans_image('UIhide1.png')
        
        
        return images