import pygame, utils
from pygame.locals import *

class Rung():
    
    def __init__(self, type, level, starty = -100):
        self.image = None
        self.speed = 10 + 5 * level
        self.rect = pygame.Rect(200,starty,600,100)
        self.hitbox1 = None
        self.hitbox2 = None
        if type == 'LCR':
            self.image = utils.load_trans_image("rungLCR.png")
        elif type == 'LR':
            self.image = utils.load_trans_image("rungLR.png")
            self.hitbox1 = pygame.Rect(400,-70,200,40)
        elif type == 'CR':
            self.image = utils.load_trans_image("rungCR.png")
            self.hitbox1 = pygame.Rect(200,-70,200,40)
        elif type == 'LC':
            self.image = utils.load_trans_image("rungLC.png")
            self.hitbox1 = pygame.Rect(600,-70,200,40)
        elif type == 'L':
            self.image = utils.load_trans_image("rungL.png")
            self.hitbox1 = pygame.Rect(400,-70,400,40)
        elif type == 'C':
            self.image = utils.load_trans_image("rungC.png")
            self.hitbox1 = pygame.Rect(200,-70,200,40)
            self.hitbox2 = pygame.Rect(600,-70,200,40)
        elif type == 'R':
            self.image = utils.load_trans_image("rungR.png")
            self.hitbox1 = pygame.Rect(200,-70,400,40)
            
    def update(self, screen):
        screen.blit(self.image, self.rect)
        
        '''if self.hitbox1 != None:
            pygame.draw.rect(screen, (255,255,255), self.hitbox1)
            if self.hitbox2 != None:
                pygame.draw.rect(screen, (255,255,255), self.hitbox2)'''
        
        
        
        self.rect.move_ip(0,self.speed)
        if self.hitbox1 != None:
            self.hitbox1.move_ip(0, self.speed)
            if self.hitbox2 != None:
                self.hitbox2.move_ip(0, self.speed)