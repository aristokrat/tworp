import pygame, random, utils, ladderrung, caveinput, interface, character, globals, os
from pygame.locals import *


AREAWIDTH = 1000
AREAHEIGHT = 760


def play(player, screen, controller, globVars):
    mainClock = pygame.time.Clock()
    level = player.gameLevel-2

    
    area = pygame.Surface((1000,760))
    HUD = interface.UI()
    
    #load images
    climb1 = utils.load_trans_image("tworpClimb1.png")
    climb2 = utils.load_trans_image("tworpClimb2.png")
    climb1 = pygame.transform.scale(climb1, (250,400))
    climb2 = pygame.transform.scale(climb2, (250,400))
    
    poleL = utils.load_trans_image("poleL.png")
    poleR = utils.load_trans_image("poleR.png")
    
    background = pygame.Surface((1000,760))
    background.fill((0,0,0))
    
    animCount = 0
    animRef = 8
    
    choices = [[],[],[],[]]
    choices[0] = ['LCR', 'LR', 'CR', 'LC']
    choices[1] = ['LCR', 'LR', 'CR', 'LC','LR', 'CR', 'LC', 'LR', 'CR', 'LC', 'L', 'R', 'C']
    choices[2] = ['LR', 'CR', 'LC', 'L', 'R', 'C']
    choices[3] = ['LR', 'CR', 'LC', 'L', 'R', 'C']
    
    playerHitbox = pygame.Rect(425,690,150,30)
    playerState = 1
    
    counter = 0
    rungs = []
    rungs.append(ladderrung.Rung('LCR', level, (     (20-5*level)*(10+5*level) - 100 )))
    rungs.append(ladderrung.Rung('LCR', level, ( 2 * (20-5*level)*(10+5*level) - 100 )))
    rungs.append(ladderrung.Rung('LCR', level, ( 3 * (20-5*level)*(10+5*level) - 100 )))
    rungs.append(ladderrung.Rung('LCR', level, ( 4 * (20-5*level)*(10+5*level) - 100 )))
    rungs.append(ladderrung.Rung('LCR', level, ( 5 * (20-5*level)*(10+5*level) - 100 )))
    rungs.append(ladderrung.Rung('LCR', level, ( 6 * (20-5*level)*(10+5*level) - 100 )))
    rungs.append(ladderrung.Rung('LCR', level, ( 7 * (20-5*level)*(10+5*level) - 100 )))
    last1 = None
    numRungs = 0
    
    
    while True:
        
        for event in pygame.event.get():
            action = caveinput.manage(event, controller)
            
            if action == 'moveRight':
                playerState += 1
                if playerState > 2:
                    playerState = 2
            elif action == 'moveLeft':
                playerState -= 1
                if playerState < 0:
                    playerState = 0
            elif action == 'terminate':
                utils.terminate()
            elif action == 'skip':
                return
            elif action == 'god':
                player.godmode()
                    
        if playerState == 0:
            xposition = 150
            playerHitbox.centerx = 275
        elif playerState == 1:
            xposition = 375
            playerHitbox.centerx = 500
        elif playerState == 2:
            xposition = 600
            playerHitbox.centerx = 725
            
        for rung in rungs:
            if rung.hitbox1 != None:
                if playerHitbox.colliderect(rung.hitbox1) or (rung.hitbox2 != None and playerHitbox.colliderect(rung.hitbox2)):
                    player.takeDamage(20*(player.gameLevel-1))
                    if player.health <= 0:
                        if globVars.sound:
                            pygame.mixer.music.load(os.path.join('data', 'tworpMain.wav'))
                            pygame.mixer.music.play(-1)
                        return
        

                    
        
        area.blit(background, (0,0))
        area.blit(poleL, (260,0))
        area.blit(poleR, (660,0))
        
        if counter == 0:
            rungType = random.choice(choices[level])
            while True:
                if last1 == 'L' and rungType == 'R':
                    rungType = random.choice(choices[level])
                elif last1 == 'R' and rungType == 'L':
                    rungType = random.choice(choices[level])
                else:
                    break
            rungs.append(ladderrung.Rung(rungType, level))
            last1 = rungType
            
        counter = (counter + 1) % (20 - 5 * level)
        
        
        for rung in rungs:
            rung.update(area)
        
        for rung in rungs:
            if rung.rect.top > 760:
                rungs.remove(rung)
                numRungs += 1
                print numRungs
        
        if animCount < animRef / 2:
            area.blit(climb1, (xposition,450))
        else:
            area.blit(climb2, (xposition,450))
            
            
        if numRungs == (30 * (level + 1)):
            return
            
        #pygame.draw.rect(area, (255,255,255), playerHitbox)
            
        animCount = (animCount + 1) % animRef
        
        screen.blit(area, (12,0))
        HUD.update(screen, player)
        
        pygame.display.update()
        mainClock.tick(30)

def test():
    
    pygame.init()
    screen = pygame.display.set_mode((1024,768))
    imageAssets = utils.assetLoader()
    player = character.Character(imageAssets)
    play(player, screen, None)

if __name__ == "__main__":test()

class Ladder():
    
    def __init__(self):
        self.image = utils.load_trans_image('ladder.png')
        self.drawrect = self.image.get_rect()
        self.drawrect.midbottom = (500,0)
        self.rect = pygame.Rect(0,0,200,50)
        self.type = 'ladder'
        
    def update(self, screen):
        self.rect.midbottom = self.drawrect.midbottom
        screen.blit(self.image, self.drawrect)
        if self.drawrect.bottom < 495:
            self.drawrect.bottom += 15
            return False
        else:
            return True
        
class LadderHole():
    
    def __init__(self):
        self.image = utils.load_image('ladderHole.png', -1)
        self.rect = self.image.get_rect()
        self.rect.center = (AREAWIDTH/2, AREAHEIGHT/2)
    
    def update(self, screen):
        screen.blit(self.image, self.rect)
        
        
        
    
