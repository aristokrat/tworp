import pygame, cavemanager, ladder, bossmanager, intromanager, introfader, utils
from pygame.locals import *

def startgame(screen, player, controller, globVars, imageAssets): #needs to change to zero once prologue completed
    
    if player.gameLevel == 0:
        intromanager.startIntro(screen, player, controller, globVars, imageAssets)
        introfader.fade(screen, player, controller, globVars, imageAssets)
    
    while player.gameLevel > 0 and player.gameLevel < 5 and player.health > 0:
        room, thisNode = cavemanager.startlevel(screen, player, controller, globVars, imageAssets)
        print room, thisNode
        if player.health > 0:
            bossmanager.startRoom(screen, player, controller, globVars, imageAssets, room, thisNode)
            if player.health > 0:
                ladder.play(player, screen, controller, globVars)
                
    image = utils.load_trans_image('continued.png')
    screen.fill((0,0,0))
    screen.blit(image, (12,4))
    pygame.display.update()
    pygame.time.delay(4000)
    utils.waitForPlayerToPressKey()